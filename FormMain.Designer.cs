﻿namespace YukiNoKagami
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "7",
            "Barbarian",
            "100",
            "100",
            "48",
            "4,800",
            "60",
            "6,000"}, 0);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "7",
            "100"}, 0);
            this.lblServerStatus = new System.Windows.Forms.Label();
            this.statusIndicator = new System.Windows.Forms.Panel();
            this.btnStartServer = new System.Windows.Forms.Button();
            this.sideSeparator = new System.Windows.Forms.Panel();
            this.btnAnalyse = new System.Windows.Forms.Button();
            this.eventTree = new System.Windows.Forms.TreeView();
            this.plEventLog = new System.Windows.Forms.Panel();
            this.eventLog_Save = new System.Windows.Forms.Button();
            this.eventLog_Clear = new System.Windows.Forms.Button();
            this.plPacketLog = new System.Windows.Forms.Panel();
            this.btnPacketDetails = new System.Windows.Forms.Button();
            this.btnSavePacketLog = new System.Windows.Forms.Button();
            this.btnClearPacketLog = new System.Windows.Forms.Button();
            this.packetListView = new System.Windows.Forms.ListView();
            this.colTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnLoadDefinitions = new System.Windows.Forms.Button();
            this.lblLastEvent = new System.Windows.Forms.Label();
            this.plVillageStats = new System.Windows.Forms.Panel();
            this.plLootInfoPlus = new System.Windows.Forms.Panel();
            this.lblXCSHL = new System.Windows.Forms.Label();
            this.lblXCSHLCaption = new System.Windows.Forms.Label();
            this.lblShieldTime = new System.Windows.Forms.Label();
            this.lblShieldTimeCaption = new System.Windows.Forms.Label();
            this.lblDisplayTypeCaption = new System.Windows.Forms.Label();
            this.selDisplayType = new System.Windows.Forms.ComboBox();
            this.listInfoPlus = new System.Windows.Forms.ListView();
            this.lblExp = new System.Windows.Forms.Label();
            this.lblExpCaption = new System.Windows.Forms.Label();
            this.lblGemOrder = new System.Windows.Forms.Label();
            this.lblGemOrderCaption = new System.Windows.Forms.Label();
            this.lblBuilders = new System.Windows.Forms.Label();
            this.lblBuilderCaption = new System.Windows.Forms.Label();
            this.lblBoxSpawn = new System.Windows.Forms.Label();
            this.lblBoxSpawnCaption = new System.Windows.Forms.Label();
            this.lblLastActivity = new System.Windows.Forms.Label();
            this.lblLastActivityCaption = new System.Windows.Forms.Label();
            this.lblObjectSpawn = new System.Windows.Forms.Label();
            this.lblObjectSpawnCaption = new System.Windows.Forms.Label();
            this.plDividerLootInfo0 = new System.Windows.Forms.Panel();
            this.lblSummaryCaption = new System.Windows.Forms.Label();
            this.lblClanID = new System.Windows.Forms.Label();
            this.lblClanIDCaption = new System.Windows.Forms.Label();
            this.lblUserID = new System.Windows.Forms.Label();
            this.lblUserIDCaption = new System.Windows.Forms.Label();
            this.lblClanLevel = new System.Windows.Forms.Label();
            this.lblClanLevelCaption = new System.Windows.Forms.Label();
            this.lblClanName = new System.Windows.Forms.Label();
            this.lblClanNameCaption = new System.Windows.Forms.Label();
            this.btnLootInfo = new System.Windows.Forms.Button();
            this.lblUserLevel = new System.Windows.Forms.Label();
            this.lblLevelCaption = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblUserNameCaption = new System.Windows.Forms.Label();
            this.plDividerTop0 = new System.Windows.Forms.Panel();
            this.btnToggleCCView = new System.Windows.Forms.Button();
            this.lblTroopsCaption = new System.Windows.Forms.Label();
            this.listViewTroops = new System.Windows.Forms.ListView();
            this.colTroopLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopSpace = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopHp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopTotalHp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopDPS = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopTotalDPS = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTroopTotalCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imgListTroops = new System.Windows.Forms.ImageList(this.components);
            this.lblWallsCaption = new System.Windows.Forms.Label();
            this.listViewWalls = new System.Windows.Forms.ListView();
            this.colLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTotalHP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imgListWalls = new System.Windows.Forms.ImageList(this.components);
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.btnExperimental = new System.Windows.Forms.Button();
            this.kbEventLog = new YukiNoKagami.KagamiButton();
            this.kbPacketLog = new YukiNoKagami.KagamiButton();
            this.kbVillageStats = new YukiNoKagami.KagamiButton();
            this.kbServerStats = new YukiNoKagami.KagamiButton();
            this.serverModeSwitch = new YukiNoKagami.KagamiButton();
            this.plEventLog.SuspendLayout();
            this.plPacketLog.SuspendLayout();
            this.plVillageStats.SuspendLayout();
            this.plLootInfoPlus.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblServerStatus
            // 
            this.lblServerStatus.AutoSize = true;
            this.lblServerStatus.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerStatus.Location = new System.Drawing.Point(12, 9);
            this.lblServerStatus.Name = "lblServerStatus";
            this.lblServerStatus.Size = new System.Drawing.Size(116, 15);
            this.lblServerStatus.TabIndex = 2;
            this.lblServerStatus.Text = "Server Status: Offline";
            // 
            // statusIndicator
            // 
            this.statusIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.statusIndicator.Location = new System.Drawing.Point(10, 30);
            this.statusIndicator.Name = "statusIndicator";
            this.statusIndicator.Size = new System.Drawing.Size(975, 3);
            this.statusIndicator.TabIndex = 3;
            // 
            // btnStartServer
            // 
            this.btnStartServer.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartServer.Location = new System.Drawing.Point(15, 40);
            this.btnStartServer.Name = "btnStartServer";
            this.btnStartServer.Size = new System.Drawing.Size(80, 25);
            this.btnStartServer.TabIndex = 4;
            this.btnStartServer.Text = "Start";
            this.btnStartServer.UseVisualStyleBackColor = true;
            this.btnStartServer.Click += new System.EventHandler(this.btnStartServer_Click);
            // 
            // sideSeparator
            // 
            this.sideSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.sideSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.sideSeparator.Location = new System.Drawing.Point(104, 40);
            this.sideSeparator.Name = "sideSeparator";
            this.sideSeparator.Size = new System.Drawing.Size(2, 533);
            this.sideSeparator.TabIndex = 6;
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnalyse.Location = new System.Drawing.Point(15, 71);
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Size = new System.Drawing.Size(80, 25);
            this.btnAnalyse.TabIndex = 10;
            this.btnAnalyse.Text = "Analyse...";
            this.btnAnalyse.UseVisualStyleBackColor = true;
            // 
            // eventTree
            // 
            this.eventTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventTree.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventTree.FullRowSelect = true;
            this.eventTree.Indent = 10;
            this.eventTree.ItemHeight = 18;
            this.eventTree.Location = new System.Drawing.Point(0, 0);
            this.eventTree.Name = "eventTree";
            this.eventTree.ShowLines = false;
            this.eventTree.ShowRootLines = false;
            this.eventTree.Size = new System.Drawing.Size(867, 423);
            this.eventTree.TabIndex = 16;
            // 
            // plEventLog
            // 
            this.plEventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plEventLog.Controls.Add(this.eventLog_Save);
            this.plEventLog.Controls.Add(this.eventLog_Clear);
            this.plEventLog.Controls.Add(this.eventTree);
            this.plEventLog.Location = new System.Drawing.Point(116, 75);
            this.plEventLog.Name = "plEventLog";
            this.plEventLog.Size = new System.Drawing.Size(867, 494);
            this.plEventLog.TabIndex = 17;
            this.plEventLog.Visible = false;
            // 
            // eventLog_Save
            // 
            this.eventLog_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.eventLog_Save.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventLog_Save.Location = new System.Drawing.Point(702, 462);
            this.eventLog_Save.Name = "eventLog_Save";
            this.eventLog_Save.Size = new System.Drawing.Size(78, 25);
            this.eventLog_Save.TabIndex = 18;
            this.eventLog_Save.Text = "Save...";
            this.eventLog_Save.UseVisualStyleBackColor = true;
            // 
            // eventLog_Clear
            // 
            this.eventLog_Clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.eventLog_Clear.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventLog_Clear.Location = new System.Drawing.Point(786, 462);
            this.eventLog_Clear.Name = "eventLog_Clear";
            this.eventLog_Clear.Size = new System.Drawing.Size(78, 25);
            this.eventLog_Clear.TabIndex = 17;
            this.eventLog_Clear.Text = "Clear Log";
            this.eventLog_Clear.UseVisualStyleBackColor = true;
            this.eventLog_Clear.Click += new System.EventHandler(this.eventLog_Clear_Click);
            // 
            // plPacketLog
            // 
            this.plPacketLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plPacketLog.Controls.Add(this.btnPacketDetails);
            this.plPacketLog.Controls.Add(this.btnSavePacketLog);
            this.plPacketLog.Controls.Add(this.btnClearPacketLog);
            this.plPacketLog.Controls.Add(this.packetListView);
            this.plPacketLog.Location = new System.Drawing.Point(116, 75);
            this.plPacketLog.Name = "plPacketLog";
            this.plPacketLog.Size = new System.Drawing.Size(867, 494);
            this.plPacketLog.TabIndex = 18;
            this.plPacketLog.Visible = false;
            // 
            // btnPacketDetails
            // 
            this.btnPacketDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPacketDetails.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnPacketDetails.Location = new System.Drawing.Point(618, 462);
            this.btnPacketDetails.Name = "btnPacketDetails";
            this.btnPacketDetails.Size = new System.Drawing.Size(78, 25);
            this.btnPacketDetails.TabIndex = 3;
            this.btnPacketDetails.Text = "Details...";
            this.btnPacketDetails.UseVisualStyleBackColor = true;
            this.btnPacketDetails.Click += new System.EventHandler(this.btnPacketDetails_Click);
            // 
            // btnSavePacketLog
            // 
            this.btnSavePacketLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSavePacketLog.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePacketLog.Location = new System.Drawing.Point(702, 462);
            this.btnSavePacketLog.Name = "btnSavePacketLog";
            this.btnSavePacketLog.Size = new System.Drawing.Size(78, 25);
            this.btnSavePacketLog.TabIndex = 2;
            this.btnSavePacketLog.Text = "Save...";
            this.btnSavePacketLog.UseVisualStyleBackColor = true;
            this.btnSavePacketLog.Click += new System.EventHandler(this.btnSavePacketLog_Click);
            // 
            // btnClearPacketLog
            // 
            this.btnClearPacketLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearPacketLog.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearPacketLog.Location = new System.Drawing.Point(786, 462);
            this.btnClearPacketLog.Name = "btnClearPacketLog";
            this.btnClearPacketLog.Size = new System.Drawing.Size(78, 25);
            this.btnClearPacketLog.TabIndex = 1;
            this.btnClearPacketLog.Text = "Clear Log";
            this.btnClearPacketLog.UseVisualStyleBackColor = true;
            this.btnClearPacketLog.Click += new System.EventHandler(this.btnClearPacketLog_Click);
            // 
            // packetListView
            // 
            this.packetListView.AllowColumnReorder = true;
            this.packetListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.packetListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTime,
            this.colID,
            this.colName,
            this.colSource,
            this.colSize,
            this.colData});
            this.packetListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.packetListView.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packetListView.FullRowSelect = true;
            this.packetListView.Location = new System.Drawing.Point(0, 0);
            this.packetListView.Name = "packetListView";
            this.packetListView.ShowItemToolTips = true;
            this.packetListView.Size = new System.Drawing.Size(867, 423);
            this.packetListView.TabIndex = 0;
            this.packetListView.UseCompatibleStateImageBehavior = false;
            this.packetListView.View = System.Windows.Forms.View.Details;
            this.packetListView.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.packetListView_ColumnWidthChanging);
            this.packetListView.SizeChanged += new System.EventHandler(this.packetListView_SizeChanged);
            this.packetListView.DoubleClick += new System.EventHandler(this.packetListView_DoubleClick);
            // 
            // colTime
            // 
            this.colTime.Text = "Time";
            this.colTime.Width = 90;
            // 
            // colID
            // 
            this.colID.Text = "ID";
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 140;
            // 
            // colSource
            // 
            this.colSource.Text = "Source";
            this.colSource.Width = 70;
            // 
            // colSize
            // 
            this.colSize.Text = "Size";
            // 
            // colData
            // 
            this.colData.Text = "Data";
            this.colData.Width = 120;
            // 
            // btnLoadDefinitions
            // 
            this.btnLoadDefinitions.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadDefinitions.Location = new System.Drawing.Point(15, 102);
            this.btnLoadDefinitions.Name = "btnLoadDefinitions";
            this.btnLoadDefinitions.Size = new System.Drawing.Size(80, 25);
            this.btnLoadDefinitions.TabIndex = 19;
            this.btnLoadDefinitions.Text = "ReM (∞)";
            this.btnLoadDefinitions.UseVisualStyleBackColor = true;
            this.btnLoadDefinitions.Click += new System.EventHandler(this.btnLoadDefinitions_Click);
            // 
            // lblLastEvent
            // 
            this.lblLastEvent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastEvent.AutoEllipsis = true;
            this.lblLastEvent.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblLastEvent.ForeColor = System.Drawing.Color.Blue;
            this.lblLastEvent.Location = new System.Drawing.Point(206, 9);
            this.lblLastEvent.Name = "lblLastEvent";
            this.lblLastEvent.Size = new System.Drawing.Size(777, 15);
            this.lblLastEvent.TabIndex = 20;
            this.lblLastEvent.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // plVillageStats
            // 
            this.plVillageStats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plVillageStats.Controls.Add(this.plLootInfoPlus);
            this.plVillageStats.Controls.Add(this.lblClanID);
            this.plVillageStats.Controls.Add(this.lblClanIDCaption);
            this.plVillageStats.Controls.Add(this.lblUserID);
            this.plVillageStats.Controls.Add(this.lblUserIDCaption);
            this.plVillageStats.Controls.Add(this.lblClanLevel);
            this.plVillageStats.Controls.Add(this.lblClanLevelCaption);
            this.plVillageStats.Controls.Add(this.lblClanName);
            this.plVillageStats.Controls.Add(this.lblClanNameCaption);
            this.plVillageStats.Controls.Add(this.btnLootInfo);
            this.plVillageStats.Controls.Add(this.lblUserLevel);
            this.plVillageStats.Controls.Add(this.lblLevelCaption);
            this.plVillageStats.Controls.Add(this.lblUserName);
            this.plVillageStats.Controls.Add(this.lblUserNameCaption);
            this.plVillageStats.Controls.Add(this.plDividerTop0);
            this.plVillageStats.Controls.Add(this.btnToggleCCView);
            this.plVillageStats.Controls.Add(this.lblTroopsCaption);
            this.plVillageStats.Controls.Add(this.listViewTroops);
            this.plVillageStats.Controls.Add(this.lblWallsCaption);
            this.plVillageStats.Controls.Add(this.listViewWalls);
            this.plVillageStats.Location = new System.Drawing.Point(116, 75);
            this.plVillageStats.Name = "plVillageStats";
            this.plVillageStats.Size = new System.Drawing.Size(867, 494);
            this.plVillageStats.TabIndex = 21;
            this.plVillageStats.Visible = false;
            // 
            // plLootInfoPlus
            // 
            this.plLootInfoPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plLootInfoPlus.Controls.Add(this.lblXCSHL);
            this.plLootInfoPlus.Controls.Add(this.lblXCSHLCaption);
            this.plLootInfoPlus.Controls.Add(this.lblShieldTime);
            this.plLootInfoPlus.Controls.Add(this.lblShieldTimeCaption);
            this.plLootInfoPlus.Controls.Add(this.lblDisplayTypeCaption);
            this.plLootInfoPlus.Controls.Add(this.selDisplayType);
            this.plLootInfoPlus.Controls.Add(this.listInfoPlus);
            this.plLootInfoPlus.Controls.Add(this.lblExp);
            this.plLootInfoPlus.Controls.Add(this.lblExpCaption);
            this.plLootInfoPlus.Controls.Add(this.lblGemOrder);
            this.plLootInfoPlus.Controls.Add(this.lblGemOrderCaption);
            this.plLootInfoPlus.Controls.Add(this.lblBuilders);
            this.plLootInfoPlus.Controls.Add(this.lblBuilderCaption);
            this.plLootInfoPlus.Controls.Add(this.lblBoxSpawn);
            this.plLootInfoPlus.Controls.Add(this.lblBoxSpawnCaption);
            this.plLootInfoPlus.Controls.Add(this.lblLastActivity);
            this.plLootInfoPlus.Controls.Add(this.lblLastActivityCaption);
            this.plLootInfoPlus.Controls.Add(this.lblObjectSpawn);
            this.plLootInfoPlus.Controls.Add(this.lblObjectSpawnCaption);
            this.plLootInfoPlus.Controls.Add(this.plDividerLootInfo0);
            this.plLootInfoPlus.Controls.Add(this.lblSummaryCaption);
            this.plLootInfoPlus.Location = new System.Drawing.Point(0, 0);
            this.plLootInfoPlus.Name = "plLootInfoPlus";
            this.plLootInfoPlus.Size = new System.Drawing.Size(867, 428);
            this.plLootInfoPlus.TabIndex = 36;
            this.plLootInfoPlus.Visible = false;
            // 
            // lblXCSHL
            // 
            this.lblXCSHL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblXCSHL.AutoSize = true;
            this.lblXCSHL.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblXCSHL.Location = new System.Drawing.Point(756, 403);
            this.lblXCSHL.Name = "lblXCSHL";
            this.lblXCSHL.Size = new System.Drawing.Size(27, 15);
            this.lblXCSHL.TabIndex = 44;
            this.lblXCSHL.Text = "null";
            // 
            // lblXCSHLCaption
            // 
            this.lblXCSHLCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblXCSHLCaption.AutoSize = true;
            this.lblXCSHLCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblXCSHLCaption.Location = new System.Drawing.Point(671, 403);
            this.lblXCSHLCaption.Name = "lblXCSHLCaption";
            this.lblXCSHLCaption.Size = new System.Drawing.Size(47, 15);
            this.lblXCSHLCaption.TabIndex = 43;
            this.lblXCSHLCaption.Text = "XCSHL:";
            // 
            // lblShieldTime
            // 
            this.lblShieldTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShieldTime.AutoSize = true;
            this.lblShieldTime.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblShieldTime.Location = new System.Drawing.Point(549, 381);
            this.lblShieldTime.Name = "lblShieldTime";
            this.lblShieldTime.Size = new System.Drawing.Size(43, 15);
            this.lblShieldTime.TabIndex = 42;
            this.lblShieldTime.Text = "7:59:44";
            // 
            // lblShieldTimeCaption
            // 
            this.lblShieldTimeCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShieldTimeCaption.AutoSize = true;
            this.lblShieldTimeCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblShieldTimeCaption.Location = new System.Drawing.Point(464, 381);
            this.lblShieldTimeCaption.Name = "lblShieldTimeCaption";
            this.lblShieldTimeCaption.Size = new System.Drawing.Size(75, 15);
            this.lblShieldTimeCaption.TabIndex = 41;
            this.lblShieldTimeCaption.Text = "Shield Time:";
            // 
            // lblDisplayTypeCaption
            // 
            this.lblDisplayTypeCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDisplayTypeCaption.AutoSize = true;
            this.lblDisplayTypeCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblDisplayTypeCaption.Location = new System.Drawing.Point(616, 336);
            this.lblDisplayTypeCaption.Name = "lblDisplayTypeCaption";
            this.lblDisplayTypeCaption.Size = new System.Drawing.Size(76, 15);
            this.lblDisplayTypeCaption.TabIndex = 40;
            this.lblDisplayTypeCaption.Text = "Display Type:";
            // 
            // selDisplayType
            // 
            this.selDisplayType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.selDisplayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selDisplayType.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.selDisplayType.FormattingEnabled = true;
            this.selDisplayType.Items.AddRange(new object[] {
            "Loot Info",
            "Barracks #1",
            "Barracks #2",
            "Barracks #3",
            "Barracks #4",
            "Dark Barracks #1",
            "Dark Barracks #2",
            "Construction Info"});
            this.selDisplayType.Location = new System.Drawing.Point(702, 332);
            this.selDisplayType.Name = "selDisplayType";
            this.selDisplayType.Size = new System.Drawing.Size(157, 23);
            this.selDisplayType.TabIndex = 39;
            this.selDisplayType.SelectedIndexChanged += new System.EventHandler(this.selDisplayType_SelectedIndexChanged);
            // 
            // listInfoPlus
            // 
            this.listInfoPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listInfoPlus.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.listInfoPlus.FullRowSelect = true;
            this.listInfoPlus.Location = new System.Drawing.Point(12, 12);
            this.listInfoPlus.Name = "listInfoPlus";
            this.listInfoPlus.Size = new System.Drawing.Size(847, 309);
            this.listInfoPlus.TabIndex = 38;
            this.listInfoPlus.UseCompatibleStateImageBehavior = false;
            this.listInfoPlus.View = System.Windows.Forms.View.Details;
            // 
            // lblExp
            // 
            this.lblExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExp.AutoSize = true;
            this.lblExp.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblExp.Location = new System.Drawing.Point(549, 403);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(34, 15);
            this.lblExp.TabIndex = 37;
            this.lblExp.Text = "2,389";
            // 
            // lblExpCaption
            // 
            this.lblExpCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExpCaption.AutoSize = true;
            this.lblExpCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblExpCaption.Location = new System.Drawing.Point(464, 403);
            this.lblExpCaption.Name = "lblExpCaption";
            this.lblExpCaption.Size = new System.Drawing.Size(31, 15);
            this.lblExpCaption.TabIndex = 36;
            this.lblExpCaption.Text = "EXP:";
            // 
            // lblGemOrder
            // 
            this.lblGemOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGemOrder.AutoSize = true;
            this.lblGemOrder.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblGemOrder.Location = new System.Drawing.Point(756, 381);
            this.lblGemOrder.Name = "lblGemOrder";
            this.lblGemOrder.Size = new System.Drawing.Size(73, 15);
            this.lblGemOrder.TabIndex = 35;
            this.lblGemOrder.Text = "4, 5, 0, 3, 2, 1";
            // 
            // lblGemOrderCaption
            // 
            this.lblGemOrderCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGemOrderCaption.AutoSize = true;
            this.lblGemOrderCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblGemOrderCaption.Location = new System.Drawing.Point(671, 381);
            this.lblGemOrderCaption.Name = "lblGemOrderCaption";
            this.lblGemOrderCaption.Size = new System.Drawing.Size(73, 15);
            this.lblGemOrderCaption.TabIndex = 34;
            this.lblGemOrderCaption.Text = "Gem Order:";
            // 
            // lblBuilders
            // 
            this.lblBuilders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBuilders.AutoSize = true;
            this.lblBuilders.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblBuilders.Location = new System.Drawing.Point(352, 403);
            this.lblBuilders.Name = "lblBuilders";
            this.lblBuilders.Size = new System.Drawing.Size(24, 15);
            this.lblBuilders.TabIndex = 33;
            this.lblBuilders.Text = "4/5";
            // 
            // lblBuilderCaption
            // 
            this.lblBuilderCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBuilderCaption.AutoSize = true;
            this.lblBuilderCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblBuilderCaption.Location = new System.Drawing.Point(268, 403);
            this.lblBuilderCaption.Name = "lblBuilderCaption";
            this.lblBuilderCaption.Size = new System.Drawing.Size(55, 15);
            this.lblBuilderCaption.TabIndex = 32;
            this.lblBuilderCaption.Text = "Builders:";
            // 
            // lblBoxSpawn
            // 
            this.lblBoxSpawn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBoxSpawn.AutoSize = true;
            this.lblBoxSpawn.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblBoxSpawn.Location = new System.Drawing.Point(352, 381);
            this.lblBoxSpawn.Name = "lblBoxSpawn";
            this.lblBoxSpawn.Size = new System.Drawing.Size(58, 15);
            this.lblBoxSpawn.TabIndex = 31;
            this.lblBoxSpawn.Text = "3:11:41:06";
            // 
            // lblBoxSpawnCaption
            // 
            this.lblBoxSpawnCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBoxSpawnCaption.AutoSize = true;
            this.lblBoxSpawnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblBoxSpawnCaption.Location = new System.Drawing.Point(268, 381);
            this.lblBoxSpawnCaption.Name = "lblBoxSpawnCaption";
            this.lblBoxSpawnCaption.Size = new System.Drawing.Size(72, 15);
            this.lblBoxSpawnCaption.TabIndex = 30;
            this.lblBoxSpawnCaption.Text = "Box Spawn:";
            // 
            // lblLastActivity
            // 
            this.lblLastActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLastActivity.AutoSize = true;
            this.lblLastActivity.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblLastActivity.Location = new System.Drawing.Point(106, 403);
            this.lblLastActivity.Name = "lblLastActivity";
            this.lblLastActivity.Size = new System.Drawing.Size(43, 15);
            this.lblLastActivity.TabIndex = 29;
            this.lblLastActivity.Text = "6:23:49";
            // 
            // lblLastActivityCaption
            // 
            this.lblLastActivityCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLastActivityCaption.AutoSize = true;
            this.lblLastActivityCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblLastActivityCaption.Location = new System.Drawing.Point(9, 403);
            this.lblLastActivityCaption.Name = "lblLastActivityCaption";
            this.lblLastActivityCaption.Size = new System.Drawing.Size(78, 15);
            this.lblLastActivityCaption.TabIndex = 28;
            this.lblLastActivityCaption.Text = "Last Activity:";
            // 
            // lblObjectSpawn
            // 
            this.lblObjectSpawn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblObjectSpawn.AutoSize = true;
            this.lblObjectSpawn.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblObjectSpawn.Location = new System.Drawing.Point(106, 381);
            this.lblObjectSpawn.Name = "lblObjectSpawn";
            this.lblObjectSpawn.Size = new System.Drawing.Size(43, 15);
            this.lblObjectSpawn.TabIndex = 27;
            this.lblObjectSpawn.Text = "4:33:59";
            // 
            // lblObjectSpawnCaption
            // 
            this.lblObjectSpawnCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblObjectSpawnCaption.AutoSize = true;
            this.lblObjectSpawnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblObjectSpawnCaption.Location = new System.Drawing.Point(9, 381);
            this.lblObjectSpawnCaption.Name = "lblObjectSpawnCaption";
            this.lblObjectSpawnCaption.Size = new System.Drawing.Size(87, 15);
            this.lblObjectSpawnCaption.TabIndex = 26;
            this.lblObjectSpawnCaption.Text = "Object Spawn:";
            // 
            // plDividerLootInfo0
            // 
            this.plDividerLootInfo0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plDividerLootInfo0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.plDividerLootInfo0.Location = new System.Drawing.Point(75, 366);
            this.plDividerLootInfo0.Name = "plDividerLootInfo0";
            this.plDividerLootInfo0.Size = new System.Drawing.Size(785, 2);
            this.plDividerLootInfo0.TabIndex = 25;
            // 
            // lblSummaryCaption
            // 
            this.lblSummaryCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSummaryCaption.AutoSize = true;
            this.lblSummaryCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblSummaryCaption.Location = new System.Drawing.Point(9, 358);
            this.lblSummaryCaption.Name = "lblSummaryCaption";
            this.lblSummaryCaption.Size = new System.Drawing.Size(63, 15);
            this.lblSummaryCaption.TabIndex = 24;
            this.lblSummaryCaption.Text = "Summary:";
            // 
            // lblClanID
            // 
            this.lblClanID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClanID.AutoSize = true;
            this.lblClanID.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblClanID.Location = new System.Drawing.Point(531, 465);
            this.lblClanID.Name = "lblClanID";
            this.lblClanID.Size = new System.Drawing.Size(61, 15);
            this.lblClanID.TabIndex = 35;
            this.lblClanID.Text = "000000000";
            this.lblClanID.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblClanID_MouseClick);
            // 
            // lblClanIDCaption
            // 
            this.lblClanIDCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClanIDCaption.AutoSize = true;
            this.lblClanIDCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblClanIDCaption.Location = new System.Drawing.Point(464, 465);
            this.lblClanIDCaption.Name = "lblClanIDCaption";
            this.lblClanIDCaption.Size = new System.Drawing.Size(49, 15);
            this.lblClanIDCaption.TabIndex = 34;
            this.lblClanIDCaption.Text = "Clan ID:";
            // 
            // lblUserID
            // 
            this.lblUserID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserID.AutoSize = true;
            this.lblUserID.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblUserID.Location = new System.Drawing.Point(531, 440);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(61, 15);
            this.lblUserID.TabIndex = 33;
            this.lblUserID.Text = "000000000";
            this.lblUserID.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblUserID_MouseClick);
            // 
            // lblUserIDCaption
            // 
            this.lblUserIDCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserIDCaption.AutoSize = true;
            this.lblUserIDCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblUserIDCaption.Location = new System.Drawing.Point(464, 440);
            this.lblUserIDCaption.Name = "lblUserIDCaption";
            this.lblUserIDCaption.Size = new System.Drawing.Size(52, 15);
            this.lblUserIDCaption.TabIndex = 32;
            this.lblUserIDCaption.Text = "User ID:";
            // 
            // lblClanLevel
            // 
            this.lblClanLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClanLevel.AutoSize = true;
            this.lblClanLevel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblClanLevel.Location = new System.Drawing.Point(319, 465);
            this.lblClanLevel.Name = "lblClanLevel";
            this.lblClanLevel.Size = new System.Drawing.Size(17, 15);
            this.lblClanLevel.TabIndex = 31;
            this.lblClanLevel.Text = "VI";
            // 
            // lblClanLevelCaption
            // 
            this.lblClanLevelCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClanLevelCaption.AutoSize = true;
            this.lblClanLevelCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblClanLevelCaption.Location = new System.Drawing.Point(268, 465);
            this.lblClanLevelCaption.Name = "lblClanLevelCaption";
            this.lblClanLevelCaption.Size = new System.Drawing.Size(40, 15);
            this.lblClanLevelCaption.TabIndex = 30;
            this.lblClanLevelCaption.Text = "Level:";
            // 
            // lblClanName
            // 
            this.lblClanName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClanName.AutoSize = true;
            this.lblClanName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblClanName.Location = new System.Drawing.Point(91, 465);
            this.lblClanName.Name = "lblClanName";
            this.lblClanName.Size = new System.Drawing.Size(87, 15);
            this.lblClanName.TabIndex = 29;
            this.lblClanName.Text = "The Renegades";
            this.lblClanName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblClanName_MouseClick);
            // 
            // lblClanNameCaption
            // 
            this.lblClanNameCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClanNameCaption.AutoSize = true;
            this.lblClanNameCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblClanNameCaption.Location = new System.Drawing.Point(9, 465);
            this.lblClanNameCaption.Name = "lblClanNameCaption";
            this.lblClanNameCaption.Size = new System.Drawing.Size(69, 15);
            this.lblClanNameCaption.TabIndex = 28;
            this.lblClanNameCaption.Text = "Clan Name:";
            // 
            // btnLootInfo
            // 
            this.btnLootInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLootInfo.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLootInfo.Location = new System.Drawing.Point(784, 462);
            this.btnLootInfo.Name = "btnLootInfo";
            this.btnLootInfo.Size = new System.Drawing.Size(75, 25);
            this.btnLootInfo.TabIndex = 27;
            this.btnLootInfo.Text = "Loot Info";
            this.btnLootInfo.UseVisualStyleBackColor = true;
            this.btnLootInfo.Click += new System.EventHandler(this.btnLootInfo_Click);
            // 
            // lblUserLevel
            // 
            this.lblUserLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserLevel.AutoSize = true;
            this.lblUserLevel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblUserLevel.Location = new System.Drawing.Point(319, 440);
            this.lblUserLevel.Name = "lblUserLevel";
            this.lblUserLevel.Size = new System.Drawing.Size(19, 15);
            this.lblUserLevel.TabIndex = 26;
            this.lblUserLevel.Text = "94";
            // 
            // lblLevelCaption
            // 
            this.lblLevelCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevelCaption.AutoSize = true;
            this.lblLevelCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblLevelCaption.Location = new System.Drawing.Point(268, 440);
            this.lblLevelCaption.Name = "lblLevelCaption";
            this.lblLevelCaption.Size = new System.Drawing.Size(40, 15);
            this.lblLevelCaption.TabIndex = 25;
            this.lblLevelCaption.Text = "Level:";
            // 
            // lblUserName
            // 
            this.lblUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblUserName.Location = new System.Drawing.Point(91, 440);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(89, 15);
            this.lblUserName.TabIndex = 24;
            this.lblUserName.Text = "King of Sweden";
            this.lblUserName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblUserName_MouseClick);
            // 
            // lblUserNameCaption
            // 
            this.lblUserNameCaption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserNameCaption.AutoSize = true;
            this.lblUserNameCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblUserNameCaption.Location = new System.Drawing.Point(9, 440);
            this.lblUserNameCaption.Name = "lblUserNameCaption";
            this.lblUserNameCaption.Size = new System.Drawing.Size(72, 15);
            this.lblUserNameCaption.TabIndex = 23;
            this.lblUserNameCaption.Text = "User Name:";
            // 
            // plDividerTop0
            // 
            this.plDividerTop0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plDividerTop0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.plDividerTop0.Location = new System.Drawing.Point(8, 429);
            this.plDividerTop0.Name = "plDividerTop0";
            this.plDividerTop0.Size = new System.Drawing.Size(851, 2);
            this.plDividerTop0.TabIndex = 22;
            // 
            // btnToggleCCView
            // 
            this.btnToggleCCView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggleCCView.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnToggleCCView.Location = new System.Drawing.Point(784, 398);
            this.btnToggleCCView.Name = "btnToggleCCView";
            this.btnToggleCCView.Size = new System.Drawing.Size(75, 25);
            this.btnToggleCCView.TabIndex = 5;
            this.btnToggleCCView.Text = "Show CC";
            this.btnToggleCCView.UseVisualStyleBackColor = true;
            this.btnToggleCCView.Click += new System.EventHandler(this.btnToggleCCView_Click);
            // 
            // lblTroopsCaption
            // 
            this.lblTroopsCaption.AutoSize = true;
            this.lblTroopsCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblTroopsCaption.Location = new System.Drawing.Point(9, 173);
            this.lblTroopsCaption.Name = "lblTroopsCaption";
            this.lblTroopsCaption.Size = new System.Drawing.Size(46, 15);
            this.lblTroopsCaption.TabIndex = 4;
            this.lblTroopsCaption.Text = "Troops:";
            // 
            // listViewTroops
            // 
            this.listViewTroops.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewTroops.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTroopLevel,
            this.colTroopName,
            this.colTroopCount,
            this.colTroopSpace,
            this.colTroopHp,
            this.colTroopTotalHp,
            this.colTroopDPS,
            this.colTroopTotalDPS,
            this.colTroopCost,
            this.colTroopTotalCost});
            this.listViewTroops.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.listViewTroops.FullRowSelect = true;
            listViewItem3.StateImageIndex = 0;
            this.listViewTroops.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem3});
            this.listViewTroops.Location = new System.Drawing.Point(8, 193);
            this.listViewTroops.Name = "listViewTroops";
            this.listViewTroops.ShowItemToolTips = true;
            this.listViewTroops.Size = new System.Drawing.Size(851, 199);
            this.listViewTroops.SmallImageList = this.imgListTroops;
            this.listViewTroops.TabIndex = 3;
            this.listViewTroops.UseCompatibleStateImageBehavior = false;
            this.listViewTroops.View = System.Windows.Forms.View.Details;
            this.listViewTroops.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.listViewTroops_ColumnWidthChanging);
            this.listViewTroops.SizeChanged += new System.EventHandler(this.listViewTroops_SizeChanged);
            // 
            // colTroopLevel
            // 
            this.colTroopLevel.Text = "Level";
            this.colTroopLevel.Width = 70;
            // 
            // colTroopName
            // 
            this.colTroopName.Text = "Name";
            this.colTroopName.Width = 100;
            // 
            // colTroopCount
            // 
            this.colTroopCount.Text = "Count";
            // 
            // colTroopSpace
            // 
            this.colTroopSpace.Text = "Space";
            // 
            // colTroopHp
            // 
            this.colTroopHp.Text = "HP";
            // 
            // colTroopTotalHp
            // 
            this.colTroopTotalHp.Text = "Total HP";
            this.colTroopTotalHp.Width = 80;
            // 
            // colTroopDPS
            // 
            this.colTroopDPS.Text = "DPS";
            // 
            // colTroopTotalDPS
            // 
            this.colTroopTotalDPS.Text = "Total DPS";
            this.colTroopTotalDPS.Width = 80;
            // 
            // colTroopCost
            // 
            this.colTroopCost.Text = "Cost";
            this.colTroopCost.Width = 70;
            // 
            // colTroopTotalCost
            // 
            this.colTroopTotalCost.Text = "Total Cost";
            this.colTroopTotalCost.Width = 80;
            // 
            // imgListTroops
            // 
            this.imgListTroops.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListTroops.ImageStream")));
            this.imgListTroops.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListTroops.Images.SetKeyName(0, "1_2.png");
            this.imgListTroops.Images.SetKeyName(1, "3_4.png");
            this.imgListTroops.Images.SetKeyName(2, "5.png");
            this.imgListTroops.Images.SetKeyName(3, "6.png");
            this.imgListTroops.Images.SetKeyName(4, "7.png");
            this.imgListTroops.Images.SetKeyName(5, "1_2.png");
            this.imgListTroops.Images.SetKeyName(6, "3_4.png");
            this.imgListTroops.Images.SetKeyName(7, "5.png");
            this.imgListTroops.Images.SetKeyName(8, "6.png");
            this.imgListTroops.Images.SetKeyName(9, "7.png");
            this.imgListTroops.Images.SetKeyName(10, "1_2.png");
            this.imgListTroops.Images.SetKeyName(11, "3_4.png");
            this.imgListTroops.Images.SetKeyName(12, "5.png");
            this.imgListTroops.Images.SetKeyName(13, "6.png");
            this.imgListTroops.Images.SetKeyName(14, "1_2.png");
            this.imgListTroops.Images.SetKeyName(15, "3_4.png");
            this.imgListTroops.Images.SetKeyName(16, "5.png");
            this.imgListTroops.Images.SetKeyName(17, "6.png");
            this.imgListTroops.Images.SetKeyName(18, "7.png");
            this.imgListTroops.Images.SetKeyName(19, "1_2.png");
            this.imgListTroops.Images.SetKeyName(20, "3_4.png");
            this.imgListTroops.Images.SetKeyName(21, "5.png");
            this.imgListTroops.Images.SetKeyName(22, "6.png");
            this.imgListTroops.Images.SetKeyName(23, "1_2.png");
            this.imgListTroops.Images.SetKeyName(24, "3_4.png");
            this.imgListTroops.Images.SetKeyName(25, "5.png");
            this.imgListTroops.Images.SetKeyName(26, "6.png");
            this.imgListTroops.Images.SetKeyName(27, "1_2.png");
            this.imgListTroops.Images.SetKeyName(28, "3_4.png");
            this.imgListTroops.Images.SetKeyName(29, "5.png");
            this.imgListTroops.Images.SetKeyName(30, "6.png");
            this.imgListTroops.Images.SetKeyName(31, "1_2.png");
            this.imgListTroops.Images.SetKeyName(32, "3_4.png");
            this.imgListTroops.Images.SetKeyName(33, "1.png");
            this.imgListTroops.Images.SetKeyName(34, "2.png");
            this.imgListTroops.Images.SetKeyName(35, "3.png");
            this.imgListTroops.Images.SetKeyName(36, "4.png");
            this.imgListTroops.Images.SetKeyName(37, "5.png");
            this.imgListTroops.Images.SetKeyName(38, "1.png");
            this.imgListTroops.Images.SetKeyName(39, "2.png");
            this.imgListTroops.Images.SetKeyName(40, "3.png");
            this.imgListTroops.Images.SetKeyName(41, "4.png");
            this.imgListTroops.Images.SetKeyName(42, "5.png");
            this.imgListTroops.Images.SetKeyName(43, "1_2.png");
            this.imgListTroops.Images.SetKeyName(44, "3_4.png");
            this.imgListTroops.Images.SetKeyName(45, "5.png");
            this.imgListTroops.Images.SetKeyName(46, "6.png");
            this.imgListTroops.Images.SetKeyName(47, "1_2.png");
            this.imgListTroops.Images.SetKeyName(48, "3_4.png");
            this.imgListTroops.Images.SetKeyName(49, "5.png");
            this.imgListTroops.Images.SetKeyName(50, "1_2.png");
            this.imgListTroops.Images.SetKeyName(51, "3_4.png");
            this.imgListTroops.Images.SetKeyName(52, "1_2.png");
            this.imgListTroops.Images.SetKeyName(53, "3_4.png");
            this.imgListTroops.Images.SetKeyName(54, "5.png");
            this.imgListTroops.Images.SetKeyName(55, "1_2.png");
            this.imgListTroops.Images.SetKeyName(56, "1_2.png");
            this.imgListTroops.Images.SetKeyName(57, "3.png");
            // 
            // lblWallsCaption
            // 
            this.lblWallsCaption.AutoSize = true;
            this.lblWallsCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblWallsCaption.Location = new System.Drawing.Point(9, 12);
            this.lblWallsCaption.Name = "lblWallsCaption";
            this.lblWallsCaption.Size = new System.Drawing.Size(38, 15);
            this.lblWallsCaption.TabIndex = 2;
            this.lblWallsCaption.Text = "Walls:";
            // 
            // listViewWalls
            // 
            this.listViewWalls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewWalls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colLevel,
            this.colCount,
            this.colHP,
            this.colTotalHP});
            this.listViewWalls.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.listViewWalls.FullRowSelect = true;
            listViewItem1.StateImageIndex = 0;
            this.listViewWalls.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listViewWalls.Location = new System.Drawing.Point(8, 32);
            this.listViewWalls.Name = "listViewWalls";
            this.listViewWalls.ShowItemToolTips = true;
            this.listViewWalls.Size = new System.Drawing.Size(851, 130);
            this.listViewWalls.SmallImageList = this.imgListWalls;
            this.listViewWalls.TabIndex = 1;
            this.listViewWalls.UseCompatibleStateImageBehavior = false;
            this.listViewWalls.View = System.Windows.Forms.View.Details;
            this.listViewWalls.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.listViewWalls_ColumnWidthChanging);
            this.listViewWalls.SizeChanged += new System.EventHandler(this.listViewWalls_SizeChanged);
            // 
            // colLevel
            // 
            this.colLevel.Text = "Level";
            // 
            // colCount
            // 
            this.colCount.Text = "Count";
            // 
            // colHP
            // 
            this.colHP.Text = "HP";
            // 
            // colTotalHP
            // 
            this.colTotalHP.Text = "Total HP";
            this.colTotalHP.Width = 80;
            // 
            // imgListWalls
            // 
            this.imgListWalls.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListWalls.ImageStream")));
            this.imgListWalls.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListWalls.Images.SetKeyName(0, "1.png");
            this.imgListWalls.Images.SetKeyName(1, "2.png");
            this.imgListWalls.Images.SetKeyName(2, "3.png");
            this.imgListWalls.Images.SetKeyName(3, "4.png");
            this.imgListWalls.Images.SetKeyName(4, "5.png");
            this.imgListWalls.Images.SetKeyName(5, "6.png");
            this.imgListWalls.Images.SetKeyName(6, "7.png");
            this.imgListWalls.Images.SetKeyName(7, "8.png");
            this.imgListWalls.Images.SetKeyName(8, "9.png");
            this.imgListWalls.Images.SetKeyName(9, "10.png");
            this.imgListWalls.Images.SetKeyName(10, "11.png");
            // 
            // updateTimer
            // 
            this.updateTimer.Interval = 1000;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // btnExperimental
            // 
            this.btnExperimental.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnExperimental.Location = new System.Drawing.Point(15, 133);
            this.btnExperimental.Name = "btnExperimental";
            this.btnExperimental.Size = new System.Drawing.Size(80, 25);
            this.btnExperimental.TabIndex = 22;
            this.btnExperimental.Text = "ExpMsg";
            this.btnExperimental.UseVisualStyleBackColor = true;
            this.btnExperimental.Click += new System.EventHandler(this.btnExperimental_Click);
            // 
            // kbEventLog
            // 
            this.kbEventLog.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kbEventLog.IsDown = false;
            this.kbEventLog.Location = new System.Drawing.Point(387, 39);
            this.kbEventLog.Name = "kbEventLog";
            this.kbEventLog.Size = new System.Drawing.Size(79, 30);
            this.kbEventLog.Sticky = true;
            this.kbEventLog.TabIndex = 15;
            this.kbEventLog.XText = "Event Log";
            this.kbEventLog.MouseUp += new System.Windows.Forms.MouseEventHandler(this.kagamiBtnTab_MouseUp);
            // 
            // kbPacketLog
            // 
            this.kbPacketLog.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kbPacketLog.IsDown = false;
            this.kbPacketLog.Location = new System.Drawing.Point(299, 39);
            this.kbPacketLog.Name = "kbPacketLog";
            this.kbPacketLog.Size = new System.Drawing.Size(85, 30);
            this.kbPacketLog.Sticky = true;
            this.kbPacketLog.TabIndex = 14;
            this.kbPacketLog.XText = "Packet Log";
            this.kbPacketLog.MouseUp += new System.Windows.Forms.MouseEventHandler(this.kagamiBtnTab_MouseUp);
            // 
            // kbVillageStats
            // 
            this.kbVillageStats.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kbVillageStats.IsDown = false;
            this.kbVillageStats.Location = new System.Drawing.Point(206, 39);
            this.kbVillageStats.Name = "kbVillageStats";
            this.kbVillageStats.Size = new System.Drawing.Size(90, 30);
            this.kbVillageStats.Sticky = true;
            this.kbVillageStats.TabIndex = 13;
            this.kbVillageStats.XText = "Village Stats";
            this.kbVillageStats.MouseUp += new System.Windows.Forms.MouseEventHandler(this.kagamiBtnTab_MouseUp);
            // 
            // kbServerStats
            // 
            this.kbServerStats.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kbServerStats.IsDown = false;
            this.kbServerStats.Location = new System.Drawing.Point(116, 39);
            this.kbServerStats.Name = "kbServerStats";
            this.kbServerStats.Size = new System.Drawing.Size(87, 30);
            this.kbServerStats.Sticky = true;
            this.kbServerStats.TabIndex = 12;
            this.kbServerStats.XText = "Server Stats";
            this.kbServerStats.MouseUp += new System.Windows.Forms.MouseEventHandler(this.kagamiBtnTab_MouseUp);
            // 
            // serverModeSwitch
            // 
            this.serverModeSwitch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.serverModeSwitch.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverModeSwitch.IsDown = false;
            this.serverModeSwitch.Location = new System.Drawing.Point(15, 543);
            this.serverModeSwitch.Name = "serverModeSwitch";
            this.serverModeSwitch.Size = new System.Drawing.Size(80, 26);
            this.serverModeSwitch.Sticky = false;
            this.serverModeSwitch.TabIndex = 11;
            this.serverModeSwitch.XText = "Proxy";
            this.serverModeSwitch.MouseUp += new System.Windows.Forms.MouseEventHandler(this.serverModeSwitch_MouseUp);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 581);
            this.Controls.Add(this.btnExperimental);
            this.Controls.Add(this.lblLastEvent);
            this.Controls.Add(this.btnLoadDefinitions);
            this.Controls.Add(this.kbEventLog);
            this.Controls.Add(this.kbPacketLog);
            this.Controls.Add(this.kbVillageStats);
            this.Controls.Add(this.kbServerStats);
            this.Controls.Add(this.serverModeSwitch);
            this.Controls.Add(this.btnAnalyse);
            this.Controls.Add(this.sideSeparator);
            this.Controls.Add(this.btnStartServer);
            this.Controls.Add(this.statusIndicator);
            this.Controls.Add(this.lblServerStatus);
            this.Controls.Add(this.plVillageStats);
            this.Controls.Add(this.plPacketLog);
            this.Controls.Add(this.plEventLog);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(914, 567);
            this.Name = "FormMain";
            this.Text = "Yuki no Kagami";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMain_KeyDown);
            this.plEventLog.ResumeLayout(false);
            this.plPacketLog.ResumeLayout(false);
            this.plVillageStats.ResumeLayout(false);
            this.plVillageStats.PerformLayout();
            this.plLootInfoPlus.ResumeLayout(false);
            this.plLootInfoPlus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblServerStatus;
        private System.Windows.Forms.Panel statusIndicator;
        private System.Windows.Forms.Button btnStartServer;
        private System.Windows.Forms.Panel sideSeparator;
        private System.Windows.Forms.Button btnAnalyse;
        private KagamiButton serverModeSwitch;
        private KagamiButton kbServerStats;
        private KagamiButton kbVillageStats;
        private KagamiButton kbPacketLog;
        private KagamiButton kbEventLog;
        private System.Windows.Forms.TreeView eventTree;
        private System.Windows.Forms.Panel plEventLog;
        private System.Windows.Forms.Button eventLog_Save;
        private System.Windows.Forms.Button eventLog_Clear;
        private System.Windows.Forms.Panel plPacketLog;
        private System.Windows.Forms.ListView packetListView;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colSource;
        private System.Windows.Forms.ColumnHeader colSize;
        private System.Windows.Forms.ColumnHeader colData;
        private System.Windows.Forms.ColumnHeader colTime;
        private System.Windows.Forms.Button btnSavePacketLog;
        private System.Windows.Forms.Button btnClearPacketLog;
        private System.Windows.Forms.Button btnLoadDefinitions;
        private System.Windows.Forms.Button btnPacketDetails;
        private System.Windows.Forms.Label lblLastEvent;
        private System.Windows.Forms.Panel plVillageStats;
        private System.Windows.Forms.Label lblWallsCaption;
        public System.Windows.Forms.ListView listViewWalls;
        private System.Windows.Forms.ColumnHeader colLevel;
        private System.Windows.Forms.ColumnHeader colCount;
        private System.Windows.Forms.ColumnHeader colHP;
        private System.Windows.Forms.ColumnHeader colTotalHP;
        private System.Windows.Forms.Button btnToggleCCView;
        private System.Windows.Forms.Label lblTroopsCaption;
        public System.Windows.Forms.ListView listViewTroops;
        private System.Windows.Forms.ColumnHeader colTroopLevel;
        private System.Windows.Forms.ColumnHeader colTroopName;
        private System.Windows.Forms.ColumnHeader colTroopCount;
        private System.Windows.Forms.ColumnHeader colTroopHp;
        private System.Windows.Forms.ColumnHeader colTroopTotalHp;
        private System.Windows.Forms.ColumnHeader colTroopDPS;
        private System.Windows.Forms.ColumnHeader colTroopTotalDPS;
        private System.Windows.Forms.ColumnHeader colTroopCost;
        private System.Windows.Forms.ColumnHeader colTroopTotalCost;
        public System.Windows.Forms.Label lblClanID;
        private System.Windows.Forms.Label lblClanIDCaption;
        public System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.Label lblUserIDCaption;
        public System.Windows.Forms.Label lblClanLevel;
        private System.Windows.Forms.Label lblClanLevelCaption;
        public System.Windows.Forms.Label lblClanName;
        private System.Windows.Forms.Label lblClanNameCaption;
        private System.Windows.Forms.Button btnLootInfo;
        public System.Windows.Forms.Label lblUserLevel;
        private System.Windows.Forms.Label lblLevelCaption;
        public System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblUserNameCaption;
        private System.Windows.Forms.Panel plDividerTop0;
        private System.Windows.Forms.ColumnHeader colTroopSpace;
        private System.Windows.Forms.Panel plLootInfoPlus;
        public System.Windows.Forms.Label lblObjectSpawn;
        private System.Windows.Forms.Label lblObjectSpawnCaption;
        private System.Windows.Forms.Panel plDividerLootInfo0;
        private System.Windows.Forms.Label lblSummaryCaption;
        public System.Windows.Forms.Label lblLastActivity;
        private System.Windows.Forms.Label lblLastActivityCaption;
        public System.Windows.Forms.Label lblBuilders;
        private System.Windows.Forms.Label lblBuilderCaption;
        public System.Windows.Forms.Label lblBoxSpawn;
        private System.Windows.Forms.Label lblBoxSpawnCaption;
        private System.Windows.Forms.Label lblDisplayTypeCaption;
        public System.Windows.Forms.Label lblExp;
        private System.Windows.Forms.Label lblExpCaption;
        public System.Windows.Forms.Label lblGemOrder;
        private System.Windows.Forms.Label lblGemOrderCaption;
        public System.Windows.Forms.ComboBox selDisplayType;
        public System.Windows.Forms.ListView listInfoPlus;
        public System.Windows.Forms.Label lblXCSHL;
        private System.Windows.Forms.Label lblXCSHLCaption;
        public System.Windows.Forms.Label lblShieldTime;
        private System.Windows.Forms.Label lblShieldTimeCaption;
        public System.Windows.Forms.ImageList imgListWalls;
        public System.Windows.Forms.ImageList imgListTroops;
        private System.Windows.Forms.Button btnExperimental;
        private System.Windows.Forms.Timer updateTimer;
    }
}

