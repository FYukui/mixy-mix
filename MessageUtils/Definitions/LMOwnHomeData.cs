﻿using System;
using YukiNoKagami.MessageUtils.LFormat;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.MessageUtils.Definitions
{
    class LMOwnHomeData
    {
        private readonly MessageDataWrapper _dataWrapper;

        public LMOwnHomeData()
        {
            _dataWrapper = new MessageDataWrapper();
        }

        public void SetMessage(Message m)
        {
            _dataWrapper.SetMessage(m);
            UpdateDefinition();
        }

        public void LoadUsingKagamiDataCollectionSpecification(KagamiDataCollection structureSpecification)
        {

        }

        private void UpdateDefinition()
        {


        }

        public override String ToString()
        {
            return "";
        }
    }
}