﻿using System.Collections.Generic;
using YukiNoKagami.Logging;

namespace YukiNoKagami.MessageUtils.MessageHandling
{
    internal interface IMessageFilter
    {
        void PassMessage(Message message);
    }

    internal class MessageFilter : IMessageFilter
    {
        private readonly Dictionary<MessageInfo.MessageType, List<IMessageFilterHandler>> _filterMap;

        public MessageFilter()
        {
            _filterMap = new Dictionary<MessageInfo.MessageType, List<IMessageFilterHandler>>();
        }

        public void PassMessage(Message message)
        {
            List<IMessageFilterHandler> handlerList;

            if (_filterMap.TryGetValue(MessageInfo.GetType(message), out handlerList))
            {
                foreach (IMessageFilterHandler mHandler in handlerList)
                    mHandler.OnMessage(message);
            }

            if (LoggingProvider.PacketLog != null)
                LoggingProvider.PacketLog.LogMessage(message);
        }

        public void AddFilter(MessageInfo.MessageType message, IMessageFilterHandler handler)
        {
            List<IMessageFilterHandler> handlerList;
            if (_filterMap.TryGetValue(message, out handlerList))
            {
                handlerList.Add(handler);
            }
            else
            {
                handlerList = new List<IMessageFilterHandler> {handler};
                _filterMap.Add(message, handlerList);
            }
        }

        public void RemoveFilter(MessageInfo.MessageType message, IMessageFilterHandler handler)
        {
            List<IMessageFilterHandler> handlerList;
            if (_filterMap.TryGetValue(message, out handlerList))
            {
                if (handlerList.Contains(handler))
                    handlerList.Remove(handler);
            }
        }

        public void RemoveFilterComplete(IMessageFilterHandler handler)
        {
            List<MessageInfo.MessageType> keyRemoveList = new List<MessageInfo.MessageType>();

            foreach (KeyValuePair<MessageInfo.MessageType, List<IMessageFilterHandler>> filterPair in _filterMap)
                if (filterPair.Value.Contains(handler))
                {
                    filterPair.Value.Remove(handler);
                    if (filterPair.Value.Count == 0)
                        // All filters for this MessageType have been removed, so remove message from map
                        keyRemoveList.Add(filterPair.Key);
                }

            foreach (MessageInfo.MessageType key in keyRemoveList)
                _filterMap.Remove(key);
        }
    }
}