﻿using System.IO;
using System.IO.Compression;
using System.Text;

namespace YukiNoKagami.MessageUtils.MessageHandling
{
    /// <summary>
    ///     Utility class made to ease modification and creation of <see cref="Message"/> objects from their JSON definitions.
    ///     It is used internally by <see cref="LFormat.KagamiDataCollection"/> and its variant types to save modified packet data.
    /// </summary>
    internal class MessageWriter
    {
        private MemoryStream _msgStream;

        public MessageWriter(int msgId)
        {
            _msgStream = new MemoryStream();
            WriteShort((short) msgId); // msgID
            WriteInt24(0);  // weird 3-byte length    (undetermined during creation)
            WriteShort(0); // unknown
        }

        public MessageWriter(Message m)
        {
            _msgStream = new MemoryStream();
            // If there is no decrypted-data the message is likely just length 7, so there was nothing to encrypt; just get the normal data:
            byte[] data = m.GetDecryptedData() ?? m.GetData();
            _msgStream.Write(data, 0, data.Length);
        }

        public void SetUnknown(short unknownVal)
        {
            long p = _msgStream.Position;
            _msgStream.Position = 5;
            WriteShort(unknownVal);

            _msgStream.Position = p;
        }

        /// <summary>
        ///     Generates a <see cref="byte"/>[] from the underlying stream, containing data needed to construct a <see cref="Message"/>
        ///     object. <para/>
        ///     This method will update the length specified in the stream, accomodating any adjustments made in-between calls.
        /// </summary>
        /// <returns>An array of <see cref="byte"/>, containing the complete and unencrypted message data written to this stream.</returns>
        public byte[] FinalizeMessage()
        {
            long p = _msgStream.Position;
            _msgStream.Position = 2;
            WriteInt24((int) _msgStream.Length - 7);

            _msgStream.Position = p;

            return _msgStream.ToArray();
        }

        /// <summary>
        ///     Compresses the given string <paramref name="s"/>, using the DEFLATE algorithm, and writes it to the underlying stream.
        /// </summary>
        /// <param name="s">The string data to be compressed and written.</param>
        public void WriteZStream(string s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);

            MemoryStream compressedStream = new MemoryStream();
            DeflateStream compressor = new DeflateStream(compressedStream, CompressionMode.Compress);
            compressor.Write(bytes, 0, bytes.Length);
            compressor.Close();

            byte[] compressed = compressedStream.ToArray();

            // Total compressed length, plus:  uncompressedLength (+4b), [78 9C]* identifier, then adler32 hash (+4b)
            // * Part of gzip header, specifies compression mode. [78 9C] represents the default compression mode.
            WriteInt(compressed.Length + 10);

            // Uncompressed length, in little-endian.
            eswriteInt(bytes.Length);

            // [78 9C] sequence, along with compressed contents and an Adler32 checksum at the end.
            _msgStream.WriteByte(0x78);
            _msgStream.WriteByte(0x9C);
            _msgStream.Write(compressed, 0, compressed.Length);

            Adler32 crc = new Adler32();
            crc.Update(bytes);
            WriteInt((int) crc.Value);
        }

        public void WriteString(string s)
        {
            // Len = -1
            if (string.IsNullOrEmpty(s))
            {
                WriteInt(unchecked((int) 0xffffffff));
            }
            else
            {
                byte[] sb = Encoding.UTF8.GetBytes(s);
                WriteInt(sb.Length);
                _msgStream.Write(sb, 0, sb.Length);
            }
        }

        public void WriteLong(long longVal)
        {
            _msgStream.WriteByte((byte) ((ulong) longVal >> 56 & 0xff));
            _msgStream.WriteByte((byte) ((ulong) longVal >> 48 & 0xff));
            _msgStream.WriteByte((byte) ((ulong) longVal >> 40 & 0xff));
            _msgStream.WriteByte((byte) ((ulong) longVal >> 32 & 0xff));
            _msgStream.WriteByte((byte) ((ulong) longVal >> 24 & 0xff));
            _msgStream.WriteByte((byte) ((ulong) longVal >> 16 & 0xff));
            _msgStream.WriteByte((byte) ((ulong) longVal >> 8  & 0xff));
            _msgStream.WriteByte((byte) longVal);
        }

        // Endian-switch write (writes int little endian, rather than big)
        public void eswriteInt(int intVal)
        {
            _msgStream.WriteByte((byte) ((uint) intVal & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal >> 8 & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal >> 16 & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal >> 24));
        }

        public void WriteInt(int intVal)
        {
            _msgStream.WriteByte((byte) ((uint) intVal >> 24));
            _msgStream.WriteByte((byte) ((uint) intVal >> 16 & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal >> 8 & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal & 0xff));
        }

        /// <summary>
        ///     Writes a 24-bit int to the underlying <see cref="MessageWriter"/> stream.<para/>
        ///     Previously named WriteInt3, which totally isn't odd given INT3 breakpoints are a thing ;)
        /// </summary>
        public void WriteInt24(int intVal)
        {
            _msgStream.WriteByte((byte) ((uint) intVal >> 16 & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal >> 8 & 0xff));
            _msgStream.WriteByte((byte) ((uint) intVal & 0xff));
        }

        public void WriteShort(short shortVal)
        {
            _msgStream.WriteByte((byte) ((uint) shortVal >> 8 & 0xff));
            _msgStream.WriteByte((byte) ((uint) shortVal & 0xff));
        }

        public void WriteByte(byte byteVal)
        {
            _msgStream.WriteByte(byteVal);
        }

        public void Seek(int index)
        {
            _msgStream.Position = index;
        }

        public void Skip(int count)
        {
            _msgStream.Seek(count, SeekOrigin.Current);
        }

        public void LoadFromFile(string fileName)
        {
            FileStream f = new FileStream(fileName, FileMode.Open);

            _msgStream.Close();
            _msgStream = new MemoryStream();

            f.CopyTo(_msgStream);
            _msgStream.Seek(0, SeekOrigin.End);

            f.Close();
        }

        /// <summary>
        ///     Saves the entirety of the current <see cref="Message"/> 
        /// </summary>
        /// <param name="fileName"></param>
        public void SaveToFile(string fileName)
        {
            FileStream f = new FileStream(fileName, FileMode.Create);

            // Save position so it can be restored after copying.
            long p = _msgStream.Position;
            _msgStream.Position = 0;
            _msgStream.CopyTo(f);

            // Restore. This is why I love Rua's save/restore ...
            _msgStream.Position = p;

            f.Flush();
            f.Close();
        }
    }
}