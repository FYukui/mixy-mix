﻿using System;

namespace YukiNoKagami.MessageUtils.MessageHandling
{
    public class Message
    {
        private readonly byte[] _content;
        private readonly int _length; // Absolute length, /not/ length specifier
        private readonly int _messageID;
        private readonly DateTime _msgTime;
        private byte[] _decryptedData;
        private int _unknown;

        public Message(byte[] data)
        {
            _content = new byte[data.Length];
            Array.Copy(data, _content, data.Length);

            _length = data.Length;
            _messageID = (_content[0] << 8) | _content[1];
            _unknown = (_content[5] << 8) | _content[6];

            _msgTime = DateTime.Now;
        }

        // Alternative constructor, for use when there are possible trailing bytes after the desired message content, such as when
        // reading from a stream.
        public Message(byte[] data, int dataLen)
        {
            _content = new byte[dataLen];
            Array.Copy(data, _content, dataLen);

            _length = dataLen;
            _messageID = (_content[0] << 8) | _content[1];
            _unknown = (_content[5] << 8) | _content[6];

            _msgTime = DateTime.Now;
        }

        public int GetID()
        {
            return _messageID;
        }

        public int GetSize()
        {
            return _length;
        }

        public byte[] GetData()
        {
            return _content;
        }

        public byte[] GetDecryptedData()
        {
            return _decryptedData;
        }

        // TODO: Hide this to message-generator only.
        public void SetDecryptedData(byte[] data)
        {
            if (data.Length != _length)
                throw new InvalidOperationException("Decrypted message-length does not match the original !");

            _decryptedData = data;
        }

        public DateTime GetTime()
        {
            return _msgTime;
        }
    }
}