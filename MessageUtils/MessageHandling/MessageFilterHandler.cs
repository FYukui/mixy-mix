﻿
namespace YukiNoKagami.MessageUtils.MessageHandling
{
    internal interface IMessageFilterHandler
    {
        void OnMessage(Message m);
    }
}