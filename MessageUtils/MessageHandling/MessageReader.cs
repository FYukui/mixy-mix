﻿using System;
using System.Net.Sockets;
using YukiNoKagami.Ciphers;

namespace YukiNoKagami.MessageUtils.MessageHandling
{
    // TODO : Rename this to MessageStreamer, and MessageDataWrapper to MessageReader.
    internal class MessageReader
    {
        private readonly byte[] _intermediaryMsgBuf;

        private ICipher _cipher;
        private IMessageFilter _msgFilter;
        private Socket _outSocket;
        private int _messageBytesRemaining;
        private int _messageTotalBytes;
        private int _msgBufOffset;
        private bool _streamingMessage;

        public MessageReader()
        {
            _intermediaryMsgBuf = new byte[131072]; // 2^17, large enough buffer to store 2 max-length messages (TCPmax=2^16)
        }

        public MessageReader(Socket streamOutSocket)
        {
            _intermediaryMsgBuf = new byte[131072];
            _outSocket = streamOutSocket;
        }

        /// <summary>
        ///     Streams in Clash Message data from the provided <paramref name="streamSocket"/>, generating <see cref="Message"/>
        ///     objects as the data is received.<para/>
        ///     Created <see cref="Message"/> objects will be decrypted using the <see cref="MessageReader"/>'s selected cipher-method
        ///     (if any) and passed through any existing filters.
        /// </summary>
        /// <param name="streamSocket">Socket from which data should be received.</param>
        public void StreamMessage(Socket streamSocket)
        {
            if (!_streamingMessage)
            {
                if (streamSocket.Available < 7)
                    return;

                streamSocket.Receive(_intermediaryMsgBuf, 0, 7, SocketFlags.None);
                _outSocket?.Send(_intermediaryMsgBuf, 7, SocketFlags.None);

                _messageBytesRemaining = (_intermediaryMsgBuf[2] << 16 | _intermediaryMsgBuf[3] << 8 |
                                          _intermediaryMsgBuf[4]);
                _messageTotalBytes = _messageBytesRemaining + 7;
                _msgBufOffset = 7;
                _streamingMessage = true;
            }
            else
            {
                if (streamSocket.Available < 1)
                    return;

                int readCount = streamSocket.Available;
                if (readCount > _messageBytesRemaining)
                    readCount = _messageBytesRemaining;

                streamSocket.Receive(_intermediaryMsgBuf, _msgBufOffset, readCount, SocketFlags.None);
                _outSocket?.Send(_intermediaryMsgBuf, _msgBufOffset, readCount, SocketFlags.None);

                _messageBytesRemaining -= readCount;
                _msgBufOffset += readCount;

                if (_messageBytesRemaining == 0)
                {
                    Message nMsg = new Message(_intermediaryMsgBuf, _messageTotalBytes);
                    _streamingMessage = false;
                    _messageTotalBytes = 0;
                    _messageBytesRemaining = 0;
                    _msgBufOffset = 0;

                    if (_cipher != null)
                        CipherMessage(nMsg);

                    _msgFilter?.PassMessage(nMsg);
                }
            }
        }

        /// <summary>
        ///     Sets the <see cref="MessageReader"/>'s optional output-socket, which when set, will be sent any data received by this
        ///     reader.<para/>
        ///     This is particularly useful for implementing a proxy, where data can be intercepted from a normal client or server,
        ///     and then sent to its originally intended destination.
        /// </summary>
        /// <param name="streamOutSocket">Socket which any input</param>
        public void SetOutSocket(Socket streamOutSocket)
        {
            _outSocket = streamOutSocket;
        }

        public ICipher GetCipherMethod()
        {
            return _cipher;
        }

        public IMessageFilter GetMessageFilter()
        {
            return _msgFilter;
        }

        public void SetCipherMethod(ICipher cipherMethod)
        {
            _cipher = cipherMethod;
        }

        public void SetMessageFilter(IMessageFilter filter)
        {
            _msgFilter = filter;
        }

        private void CipherMessage(Message m)
        {
            byte[] data = m.GetData();
            if (data.Length <= 7)
                return;

            byte[] payload = new byte[data.Length - 7];
            Array.Copy(data, 7, payload, 0, data.Length - 7);
            payload = _cipher.Encrypt(payload);

            byte[] nPacket = new byte[data.Length];
            Array.Copy(data, 0, nPacket, 0, 7);
            Array.Copy(payload, 0, nPacket, 7, payload.Length);

            m.SetDecryptedData(nPacket);
        }
    }
}