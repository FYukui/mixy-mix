﻿using System;

// @Mozilla, light Adler32 implementation. Some little modifications for prettiness.
// Somewhat slower (?), but doesn't return strange results on some sets.

namespace YukiNoKagami.MessageUtils
{
    public sealed class Adler32
    {
        private const uint Base = 65521;
        private uint _checksum;

        public long Value => _checksum;

        public Adler32()
        {
            Reset();
        }

        public void Reset()
        {
            _checksum = 1;
        }

        public void Update(int value)
        {
            // We could make a length 1 byte array and call update again, but I
            // would rather not have that overhead
            uint s1 = _checksum & 0xFFFF;
            uint s2 = _checksum >> 16;

            s1 = (s1 + ((uint) value & 0xFF))%Base;
            s2 = (s1 + s2)%Base;

            _checksum = (s2 << 16) + s1;
        }


        public void Update(byte[] buffer)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            Update(buffer, 0, buffer.Length);
        }

        public void Update(byte[] buffer, int offset, int count)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset", "cannot be negative");

            if (count < 0)
                throw new ArgumentOutOfRangeException("count", "cannot be negative");

            if (offset >= buffer.Length)
                throw new ArgumentOutOfRangeException("offset", "not a valid index into buffer");

            if (offset + count > buffer.Length)
                throw new ArgumentOutOfRangeException("count", "exceeds buffer size");

            // (By Per Bothner)
            uint s1 = _checksum & 0xFFFF;
            uint s2 = _checksum >> 16;

            while (count > 0)
            {
                // We can defer the modulo operation:
                // s1 maximally grows from 65521 to 65521 + 255 * 3800
                // s2 maximally grows by 3800 * median(s1) = 2090079800 < 2^31
                int n = 3800;
                if (n > count)
                {
                    n = count;
                }
                count -= n;
                while (--n >= 0)
                {
                    s1 = s1 + (uint) (buffer[offset++] & 0xff);
                    s2 = s2 + s1;
                }
                s1 %= Base;
                s2 %= Base;
            }

            _checksum = (s2 << 16) | s1;
        }
    }
}