﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.MessageUtils.LFormat
{

    // TODO : Integrate this into the Message class itself. Remove this public class.
    class MessageDataWrapper
    {

        private byte[] _dataBuffer;
        private int _pos;

        public MessageDataWrapper()
        {

        }

        public void SetMessage(Message m)
        {
            _dataBuffer = m.GetDecryptedData();
            _pos = 7;
        }

        // TODO: Make it easier for KagamiVariant* to read conditional fields.
        public byte ReadByte()
        {
            byte res = _dataBuffer[_pos];
            _pos++;
            return res;
        }

        public int ReadInt()
        {
            int res = (_dataBuffer[_pos] << 24) | (_dataBuffer[_pos + 1] << 16) | (_dataBuffer[_pos + 2] << 8) | _dataBuffer[_pos + 3];
            _pos = _pos + 4;
            return res;
        }

        public long ReadLong()
        {
            long res =  ((long) _dataBuffer[_pos + 0]         << 56) |
                        ((long)(_dataBuffer[_pos + 1] & 0xff) << 48) |
                        ((long)(_dataBuffer[_pos + 2] & 0xff) << 40) | 
                        ((long)(_dataBuffer[_pos + 3] & 0xff) << 32) |
                        ((long)(_dataBuffer[_pos + 4] & 0xff) << 24) |
                        ((long)(_dataBuffer[_pos + 5] & 0xff) << 16) |
                        ((long)(_dataBuffer[_pos + 6] & 0xff) << 8)  |
                        ((long)(_dataBuffer[_pos + 7] & 0xff));
            _pos = _pos + 8;
            return res;
        }

        public String ReadString()
        {
            int strLen = ReadInt();
            if (strLen < 1)     // Blank string length = -1
                return null;

            byte[] sb = new byte[strLen];
            Array.Copy(_dataBuffer, _pos, sb, 0, strLen);

            String res = Encoding.UTF8.GetString(sb);

            _pos = _pos + strLen;
            return res;
        }

        public void Skip(int bytes)
        {
            _pos = _pos + bytes;
        }

        public int esreadInt()  // Endian-switch read
        {
            int res = _dataBuffer[_pos] | (_dataBuffer[_pos + 1] << 8) | (_dataBuffer[_pos + 2] << 16) | (_dataBuffer[_pos + 3] << 24);
            _pos = _pos + 4;
            return res;
        }

        public String ReadZip()
        {
            int length = ReadInt();
            if ((uint)length == 0xffffffff)
                return null;
            if(length == 0)
                return "";

            int unzippedLen = esreadInt();

            Skip(2);   // Skip 2, otherwise we'll read invalid data. This is due to gzip's prepended compression-mode info: [78 9c]. Tricky !

            byte[] zipped = new byte[length - 6];
            Array.Copy(_dataBuffer, _pos, zipped, 0, zipped.Length);

            MemoryStream memStream = new MemoryStream(zipped);
            DeflateStream zipStream = new DeflateStream(memStream, CompressionMode.Decompress);

            MemoryStream decompressed = new MemoryStream(unzippedLen);

            zipStream.CopyTo(decompressed);
            //zipStream.Close();      // Nmeh

            byte[] unzipped = decompressed.ToArray();

            _pos += length - 6;  // (i.e. zipped.Length)

            return Encoding.UTF8.GetString(unzipped);
        }

    }
}