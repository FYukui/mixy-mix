﻿using System.Collections.Generic;

// Provides a set of preconstructed KagamiDataCollections for easy use with maps.
// Data collections can easily be constructed and used without this helper class, using just FormalJsonDefinitionHost.
// This simply binds all into one group, for whatever reason. (TODO: Remove this.)

namespace YukiNoKagami.MessageUtils.LFormat
{
    internal static class KagamiDataHost
    {
        private static readonly Dictionary<string, KagamiDataCollection> CollectionSet;
        private static readonly Dictionary<string, KagamiDataCollection> NormalizedSet;

        static KagamiDataHost()
        {
            CollectionSet = new Dictionary<string, KagamiDataCollection>();
            NormalizedSet = new Dictionary<string, KagamiDataCollection>();
        }

        public static void RemoveAll()
        {
            CollectionSet.Clear();
            NormalizedSet.Clear();
        }

        public static void CreateDefinitionFromFormalJson(FormalJsonDefinition messageDefinition, string messageName)
        {
            if (messageDefinition == null)
                return;

            KagamiDataCollection dataStructure = new KagamiDataCollection();
            dataStructure.LoadListStructureFromJson(messageDefinition.Json);

            int i = messageName.IndexOf('[');
            if (i > -1)
                NormalizedSet.Add(messageName.Substring(0, i), dataStructure);
            else
                NormalizedSet.Add(messageName, dataStructure);

            CollectionSet.Add(messageName, dataStructure);
        }

        // Get the definition using a complex/fully-qualified name, ex: OwnHomeData[24101] instead of "OwnHomeData"
        public static KagamiDataCollection GetDefinition(string messageName)
        {
            KagamiDataCollection value = null;
            CollectionSet.TryGetValue(messageName, out value);
            return value;
        }

        public static KagamiDataCollection GetFromNormalizedName(string messageName)
        {
            KagamiDataCollection value = null;
            NormalizedSet.TryGetValue(messageName, out value);
            return value;
        }
    }
}