﻿using System;
using System.Collections.Generic;
using System.Linq;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.MessageUtils.LFormat
{

    // Messy JSON variant type. Not as bad as FKJson... probably x.x

    class KagamiVariantType
    {
        private readonly Object _defaultValue;
        private readonly DataType _type;
        private readonly DataType _arrayType;
        private readonly FormalJsonDefinition _arrayExternDefinition;   // Usually null, unless there's a ridiculous external-based array (which there are)
        private String _arrayExternName;                                // Used solely for informatic purposes (while inserting to a tree, querying info, etc.)
        private int _arraySize;

        public bool IsOptional;     // If set, the value will have a preceding byte set to 0 or 1, reflecting whether it is expressed or not.
        public bool IsExpressed;    // Determines whether this optional value is expressed in the active message.
        public readonly String Name;
        public readonly String Comment;

        public DataType InnerType => _type;
        public String TypeName => TypeToString(_type);
        public int ArrayLength => _arraySize;
        public Object Value { get; set; }

        public enum DataType
        {
            DtByte,
            //dtBool,
            DtInteger,
            DtLong,
            DtString,
            DtZStream,
            DtArray,
            DtCollection,
            Undefined
        }

        private static Object VariantToString(String castString, DataType castType)
        {
            switch (castType)
            {
                case DataType.DtByte:
                    return byte.Parse(castString);
                case DataType.DtInteger:
                    return int.Parse(castString);
                case DataType.DtLong:
                    return long.Parse(castString);
                case DataType.DtString:
                case DataType.DtZStream:
                    return castString;
                case DataType.DtArray:
                    throw new InvalidOperationException("DataType.Array has no valid string-conversion. Invalid variantToString target.");
                case DataType.DtCollection:
                    throw new InvalidOperationException("DataType.Collection has no valid string-conversion. Invalid variantToString target.");
            }
            throw new InvalidOperationException("Invalid type specified for variantToString: "+castType);
        }

        private static DataType MatchType(String typeStr)
        {
            typeStr = typeStr.ToLower();
            switch (typeStr)
            {
                case "byte":
                    return DataType.DtByte;
                case "int":
                    return DataType.DtInteger;
                case "long":
                    return DataType.DtLong;
                case "string":
                    return DataType.DtString;
                case "zstream":
                    return DataType.DtZStream;
                case "array":
                    return DataType.DtArray;
                case "external":
                    return DataType.DtCollection;
            }
            return DataType.Undefined;
        }

        private static String TypeToString(DataType cType)
        {
            switch (cType)
            {
                case DataType.DtByte:
                    return "byte";
                case DataType.DtInteger:
                    return "int";
                case DataType.DtLong:
                    return "long";
                case DataType.DtString:
                    return "string";
                case DataType.DtZStream:
                    return "zstream";
                case DataType.DtArray:
                    return "array";
                case DataType.DtCollection:
                    return "external";
            }
            return "undefined";
        }

        public void Reset()
        {
            if (_type == DataType.DtCollection)
                ((KagamiDataCollection)Value).Reset();
            else
                Value = _defaultValue;    // If there is no defaultValue, then variantValue should be nulled (default value if not specified).
        }

        public KagamiVariantType(FkJsonNode fieldNode)
        {
            FkJsonNode n = fieldNode.QueryMember("name");
            if (n != null)
                Name = n.GetValue();

            _type = MatchType(fieldNode.QueryMember("type").GetValue());
            if (_type == DataType.Undefined)
                throw new InvalidOperationException("Undefined type in field '" + Name + "', defined in '" 
                                                    + fieldNode.Up(2).QueryMember("name").GetValue() + "'");

            if (_type == DataType.DtCollection)
            {
                if((n = fieldNode.QueryMember("external")) != null)
                {
                    Value = new KagamiDataCollection();
                    FormalJsonDefinition formalExtern = FormalJsonDefinitionHost.GetExternalDefinition(n.GetValue());
                    if (formalExtern == null)
                        throw new InvalidOperationException("Non-existent extern ('" + n.GetValue() + "') referenced for field '"
                                                            + Name + "', defined in '" + fieldNode.Up(2).QueryMember("name").GetValue() + "'");
                    ((KagamiDataCollection)Value).LoadListStructureFromJson(formalExtern.Json);
                } else
                {
                    throw new InvalidOperationException("Attempted to create external variant without 'external' specification!\n\n" +
                                                        "Field name = "+Name+", owner message = " + fieldNode.Up(2).QueryMember("name").GetValue());
                }
            } else if (_type == DataType.DtArray)
            {
                _arrayType = MatchType(fieldNode.QueryMember("arrayType").GetValue());
                if (_arrayType == DataType.Undefined)
                    throw new InvalidOperationException("Undefined type in array field '" + Name + "', defined in '"
                                                        + fieldNode.Up(2).QueryMember("name").GetValue() + "'");

                if(_arrayType == DataType.DtCollection)
                {

                    if ((n = fieldNode.QueryMember("external")) == null)
                        throw new InvalidOperationException("Array of collection '" +Name+"', defined in '" +
                                                            fieldNode.Up(2).QueryMember("name").GetValue() + "' did not specify an 'external' field!");

                    FormalJsonDefinition formalExtern = FormalJsonDefinitionHost.GetExternalDefinition(n.GetValue());
                    if (formalExtern == null)
                        throw new InvalidOperationException("Non-existent extern ('" + n.GetValue() + "') referenced for array field '"
                                                            + Name + "', defined in '" + fieldNode.Up(2).QueryMember("name").GetValue() + "'");

                    _arrayExternDefinition = FormalJsonDefinitionHost.GetExternalDefinition(fieldNode.QueryMember("external").GetValue());
                    //LoggingProvider.EventLog.LogBasicEvent("Collection-based array !!" + name + "{" + arrayExternalName + "}");

                }
            }

            if ((n = fieldNode.QueryMember("default")) != null)
                _defaultValue = VariantToString(n.GetValue(), _type);

            if ((n = fieldNode.QueryMember("optional")) != null)
            {
                IsOptional = n.GetBoolean();
                if (_type == DataType.DtCollection)
                    ((KagamiDataCollection)Value).InstanceIsOptional = IsOptional;
            }

            if ((n = fieldNode.QueryMember("comment")) != null)
                Comment = n.GetValue();

        }

        [Obsolete("Use Value property instead, with FCast if needed.")]
        private T CastGet<T>()
        {
            return (T)Value;
        }

        [Obsolete("Use Value property instead, with FCast if needed.")]
        private void CastSet<T>(T value)
        {
            Value = value;
        }

        private Object ArrayReadType(MessageDataWrapper data, DataType readType)
        {
            switch (readType)
            {
                case DataType.DtByte:
                    return data.ReadByte();
                case DataType.DtInteger:
                    return data.ReadInt();
                case DataType.DtLong:
                    return data.ReadLong();
                case DataType.DtString:
                    return data.ReadString();
                case DataType.DtCollection:
                    KagamiDataCollection nCollection = new KagamiDataCollection();
                    nCollection.LoadListStructureFromJson(_arrayExternDefinition.Json);
                    nCollection.ReadMessageData(data);
                    return nCollection;
            }

            // Arrays of zstreams are unsupported, as that is contradictory to the nature of saving space using them ...
            // Arrays of Arrays are completely unsupported. If this is necessary, create an array of a Collection containing only the nested array.
            throw new InvalidOperationException("Attempted to read invalid type: " + TypeToString(readType) + ", for array with name = " + Name);
            // return null;
        }

        private void ReadArray(MessageDataWrapper data)
        {
            _arraySize = data.ReadInt();
            if (_arraySize < 1)  // Does this even happen o.o?
                return;

            Value = new Object[_arraySize];
            for (int x = 0; x < _arraySize; x++)
            {
                ((Object[])Value)[x] = ArrayReadType(data, _arrayType);
            }

        }

        private void WriteArrayType(MessageWriter writer, Object data, DataType writeType)
        {
            switch (writeType)
            {
                case DataType.DtByte:
                    writer.WriteByte((byte)data);
                    return;
                case DataType.DtInteger:
                    writer.WriteInt((int)data);
                    return;
                case DataType.DtLong:
                    writer.WriteLong((long)data);
                    return;
                case DataType.DtString:
                    writer.WriteString((String)data);
                    return;
                case DataType.DtCollection:
                    ((KagamiDataCollection)data).WriteMessageData(writer);
                    return;
            }

            // Arrays of zstreams are unsupported, as that is contradictory to the nature of saving space using them ...
            // Arrays of Arrays are completely unsupported. If this is necessary, create an array of a Collection containing only the nested array.
            throw new InvalidOperationException("Attempted to write invalid type: " + TypeToString(writeType) + ", for array with name = " + Name);
        }

        private void WriteArray(MessageWriter writer)
        {
            writer.WriteInt(_arraySize);

            for(int x = 0; x < _arraySize; x++)
            {
                WriteArrayType(writer, ((Object[])Value)[x], _arrayType);
            }
        }

        public void Read(MessageDataWrapper data)
        {
            if (IsOptional)
            {
                IsExpressed = data.ReadByte() != 0; // Expressed if the value is not zero, otherwise not expressed.
                if (_type == DataType.DtCollection)
                    ((KagamiDataCollection)Value).InstanceIsExpressed = IsExpressed;
                if (!IsExpressed)
                    return;
            }

            switch (_type)
            {
                case DataType.DtByte:
                    Value = data.ReadByte();
                    return;
                case DataType.DtInteger:
                    Value = data.ReadInt();
                    return;
                case DataType.DtLong:
                    Value = data.ReadLong();
                    return;
                case DataType.DtString:
                    Value = data.ReadString();
                    return;
                case DataType.DtZStream:
                    Value = data.ReadZip();
                    return;
                case DataType.DtArray:
                    ReadArray(data);
                    return;
                case DataType.DtCollection:
                    // A collection's variantValue is set to a new KagamiDataCollection instance loaded as the proper type.
                    // All that must be done is readMessageData into that collection.
                    ((KagamiDataCollection)Value).ReadMessageData(data);
                    return;
            }
        }

        public void Write(MessageWriter writer)
        {
            // Collections will write whether they are expressed or not on their own, so it need not be done here, as with
            // the general types.
            if (IsOptional && (_type != DataType.DtCollection))
            {
                if (IsExpressed)
                    writer.WriteByte(1);
                else
                {
                    writer.WriteByte(0);
                    return;
                }
            }

            switch (_type)
            {
                case DataType.DtByte:
                    writer.WriteByte((byte)Value);
                    return;
                case DataType.DtInteger:
                    writer.WriteInt((int)Value);
                    return;
                case DataType.DtLong:
                    writer.WriteLong((long)Value);
                    return;
                case DataType.DtString:
                    writer.WriteString((String)Value);
                    return;
                case DataType.DtZStream:
                    writer.WriteZStream((String)Value);
                    return;
                case DataType.DtArray:
                    WriteArray(writer);
                    return;
                case DataType.DtCollection:
                    ((KagamiDataCollection)Value).WriteMessageData(writer);
                    return;
            }
        }

        public Object[] GetArray()
        {
            return (Object[])Value;
        }

        public void InsertToTreeNodes(System.Windows.Forms.TreeNodeCollection treeNodes)
        {

            if (_type == DataType.DtCollection) {
                ((KagamiDataCollection)Value).InsertToTreeNodes(treeNodes);
            } else if (_type == DataType.DtArray) {
                // Array-to-treenode handling here...
                String nodeName = "";
                if (IsOptional)
                    nodeName += "optional ";
                if (_arrayType == DataType.DtCollection)
                    nodeName += "Collection(" + _arrayExternName + ")";
                else
                    nodeName += TypeToString(_arrayType);


                nodeName += "[" + _arraySize + "] " + Name;
                System.Windows.Forms.TreeNode n = treeNodes.Add(nodeName);

                if (Comment != null)
                {
                    n.ToolTipText = Comment;
                    n.NodeFont = new System.Drawing.Font("Consolas", 9.0f, System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Bold);
                }

                if (IsOptional && !IsExpressed)
                    n.ForeColor = System.Drawing.Color.Gray;
                else
                    n.ForeColor = System.Drawing.Color.Purple;  // Arrays are purple. Collections are blue. Sounds good :3

                if(_arraySize > 0)
                {
                    if(_arrayType == DataType.DtCollection)
                    {
                        for(int x = 0; x < _arraySize; x++)
                        {
                            System.Windows.Forms.TreeNode kdcn = ((KagamiDataCollection)((Object[])Value)[x]).InsertToTreeNodes(n.Nodes);
                            kdcn.Text = "[" + x + "] " + kdcn.Text;
                        }
                    } else {
                        for (int x = 0; x < _arraySize; x++)
                        {
                            String valueString = "NULL";
                            Object aiValue = ((Object[])Value)[x];

                            if (aiValue != null)
                                valueString = aiValue.ToString();

                            if (valueString.Length > 300)
                                valueString = valueString.Substring(0, 300);

                            n.Nodes.Add(Name + "[" + x + "] = " + valueString);
                        }
                    }
                }


            } else {
                String valueString = "NULL";

                if(Value != null)
                    valueString = Value.ToString();

                if (valueString.Length > 300)
                    valueString = valueString.Substring(0, 300);
                System.Windows.Forms.TreeNode n;
                if(IsOptional)
                    n = treeNodes.Add("optional " + TypeToString(_type) + " " + Name + " = '" + valueString + "'");
                else
                    n = treeNodes.Add(TypeToString(_type) + " " + Name + " = '" + Value + "'");
                if (Comment != null)
                {
                    n.ToolTipText = Comment;
                    n.NodeFont = new System.Drawing.Font("Consolas", 9.0f, System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Bold);
                }
                n.Tag = this;
                if (IsOptional && !IsExpressed)
                    n.ForeColor = System.Drawing.Color.Gray;
            }

        }

    }

    class KagamiDataCollection
    {

        public List<KagamiVariantType> Values;
        public String CollectionName;     // UserDataComponent, etcetc.
        public String Comment;
        private int _typeId;

        public bool InstanceIsOptional;
        public bool InstanceIsExpressed;

        public int ID => _typeId;

        public KagamiDataCollection()
        {
            Values = new List<KagamiVariantType>();
        }

        public void LoadListStructureFromJson(FkJsonNode json)
        {
            json = json.GetIndexedNode(0);  // Get the first object within the Host
            CollectionName = json.QueryMember("name").GetValue();

            FkJsonNode n = json.QueryMember("id");
            if (n != null)
                _typeId = n.GetInteger();

            JsonArray fields = json.QueryMember("fields").GetArray();
            foreach(FkJsonNode fNode in fields.NodeList)
            {
                Values.Add(new KagamiVariantType(fNode));
            }

            if ((n = json.QueryMember("comment")) != null)
                Comment = n.GetValue();

            //json.fkTrace();
        }

        public void ReadMessageData(MessageDataWrapper data)
        {
            foreach(KagamiVariantType var in Values)
            {
                var.Read(data);
            }
        }

        public void WriteMessageData(MessageWriter writer)
        {
            if (InstanceIsOptional)
            {
                if (InstanceIsExpressed)
                    writer.WriteByte(1);
                else
                {
                    writer.WriteByte(0);
                    return;
                }
            }

            foreach (KagamiVariantType var in Values)
            {
                var.Write(writer);
            }

        }


        public bool IsExpressed(String fieldName)
        {
            KagamiVariantType field = QueryField(fieldName);
            if (field == null)
                return false;

            return field.IsExpressed;
        }

        public KagamiVariantType QueryField(String fieldName)
        {
            return Values.FirstOrDefault(variant => (variant.Name != null) && (variant.Name.Equals(fieldName)));
        }

        public Object GetValue(String fieldName)
        {
            KagamiVariantType field = QueryField(fieldName);
            return field?.Value;
        }

        public void Reset()
        {
            foreach (KagamiVariantType variant in Values)
                variant.Reset();
        }

        public System.Windows.Forms.TreeNode InsertToTreeNodes(System.Windows.Forms.TreeNodeCollection treeNodes)
        {
            System.Windows.Forms.TreeNode n = treeNodes.Add("Collection " + CollectionName);
            n.ToolTipText = Comment;
            n.ForeColor = System.Drawing.Color.Blue;
            n.Tag = this;   // used to retrieve value later using copy mode
            foreach (KagamiVariantType variant in Values)
                variant.InsertToTreeNodes(n.Nodes);

            if (InstanceIsOptional)
            {
                n.Text = n.Text + " (optional)";
                if (!InstanceIsExpressed)
                {
                    n.ForeColor = System.Drawing.Color.LightBlue;
                    foreach (System.Windows.Forms.TreeNode subNode in n.Nodes)
                        subNode.ForeColor = System.Drawing.Color.Gray;
                }
            }

            return n;
        }

    }
}