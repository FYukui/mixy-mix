﻿using System.Collections.Generic;
using System.IO;
using YukiNoKagami.Logging;

namespace YukiNoKagami.MessageUtils.LFormat
{
    internal class FormalJsonDefinition
    {
        public string JsonText;
        public FkJsonNode Json { get; private set; }

        public void ReloadJson()
        {
            Json = new FkJsonNode();
            Json.LoadString(JsonText);
        }
    }

    internal static class FormalJsonDefinitionHost
    {
        private static Dictionary<string, FormalJsonDefinition> _clientDefinitions;
        private static Dictionary<string, FormalJsonDefinition> _serverDefinitions;
        private static Dictionary<string, FormalJsonDefinition> _externalDefinitions;

        public static void ReloadAll(string hostFolder)
        {
            LoadServerDefinitions(hostFolder);
            LoadClientDefinitions(hostFolder);
            LoadExternalDefinitions(hostFolder);
        }

        public static void InsertIntoKagamiDataHost()
        {
            KagamiDataHost.RemoveAll();

            foreach (KeyValuePair<string, FormalJsonDefinition> def in _clientDefinitions)
                KagamiDataHost.CreateDefinitionFromFormalJson(def.Value, def.Key);

            foreach (KeyValuePair<string, FormalJsonDefinition> def in _serverDefinitions)
                KagamiDataHost.CreateDefinitionFromFormalJson(def.Value, def.Key);

            foreach (KeyValuePair<string, FormalJsonDefinition> def in _externalDefinitions)
                KagamiDataHost.CreateDefinitionFromFormalJson(def.Value, def.Key);
        }

        public static FormalJsonDefinition GetClientDefinition(string name)
        {
            if (_clientDefinitions == null)
                return null;

            FormalJsonDefinition value = null;
            _clientDefinitions.TryGetValue(name, out value);
            return value;
        }

        public static FormalJsonDefinition GetServerDefinition(string name)
        {
            if (_serverDefinitions == null)
                return null;

            FormalJsonDefinition value = null;
            _serverDefinitions.TryGetValue(name, out value);
            return value;
        }

        public static FormalJsonDefinition GetExternalDefinition(string name)
        {
            if (_externalDefinitions == null)
                return null;

            FormalJsonDefinition value = null;
            _externalDefinitions.TryGetValue(name, out value);
            return value;
        }

        public static void LoadClientDefinitions(string hostFolder)
        {
            hostFolder += "/client/";
            if (!Directory.Exists(hostFolder))
            {
                LoggingProvider.EventLog.LogBasicEvent("No client definitions folder found. Skipping.");
                return;
            }


            string[] definitionFiles = Directory.GetFiles(hostFolder);
            _clientDefinitions = new Dictionary<string, FormalJsonDefinition>();
            if (definitionFiles.Length == 0)
            {
                LoggingProvider.EventLog.LogBasicEvent("No client definitions to load.");
                return;
            }

            LoggingProvider.EventLog.LogBasicEvent("Loading client definitions...");
            foreach (string s in definitionFiles)
            {
                StreamReader f = new StreamReader(s);
                FormalJsonDefinition nDef = new FormalJsonDefinition {JsonText = f.ReadToEnd()};
                nDef.ReloadJson();
                f.Close();
                _clientDefinitions.Add(Path.GetFileNameWithoutExtension(s), nDef);
                LoggingProvider.EventLog.LogBasicEvent("Loaded client definition from '" + s + "'");
            }

            LoggingProvider.EventLog.LogBasicEvent("");
        }

        public static void LoadServerDefinitions(string hostFolder)
        {
            hostFolder += "/server/";
            if (!Directory.Exists(hostFolder))
            {
                LoggingProvider.EventLog.LogBasicEvent("No server definitions folder found. Skipping.");
                return;
            }

            string[] definitionFiles = Directory.GetFiles(hostFolder);
            _serverDefinitions = new Dictionary<string, FormalJsonDefinition>();
            if (definitionFiles.Length == 0)
            {
                LoggingProvider.EventLog.LogBasicEvent("No server definitions to load.");
                return;
            }

            LoggingProvider.EventLog.LogBasicEvent("Loading server definitions...");
            foreach (string s in definitionFiles)
            {
                StreamReader f = new StreamReader(s);
                FormalJsonDefinition nDef = new FormalJsonDefinition {JsonText = f.ReadToEnd()};
                nDef.ReloadJson();
                f.Close();
                _serverDefinitions.Add(Path.GetFileNameWithoutExtension(s), nDef);
                LoggingProvider.EventLog.LogBasicEvent("Loaded server definition from '" + s + "'");
            }

            LoggingProvider.EventLog.LogBasicEvent("");
        }

        public static void LoadExternalDefinitions(string hostFolder)
        {
            hostFolder += "/externals/";
            if (!Directory.Exists(hostFolder))
            {
                LoggingProvider.EventLog.LogBasicEvent("No external definitions folder found. Skipping.");
                return;
            }

            string[] definitionFiles = Directory.GetFiles(hostFolder);
            _externalDefinitions = new Dictionary<string, FormalJsonDefinition>();
            if (definitionFiles.Length == 0)
            {
                LoggingProvider.EventLog.LogBasicEvent("No external definitions to load.");
                return;
            }

            LoggingProvider.EventLog.LogBasicEvent("Loading external definitions...");
            foreach (string s in definitionFiles)
            {
                StreamReader f = new StreamReader(s);
                FormalJsonDefinition nDef = new FormalJsonDefinition {JsonText = f.ReadToEnd()};
                nDef.ReloadJson();
                f.Close();
                _externalDefinitions.Add(Path.GetFileNameWithoutExtension(s), nDef);
                LoggingProvider.EventLog.LogBasicEvent("Loaded external definition from '" + s + "'");
            }

            LoggingProvider.EventLog.LogBasicEvent("");
        }
    }
}