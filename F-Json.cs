﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YukiNoKagami
{
    internal static class StringValueConverter
    {
        public static string NullConvert(string value)
        {
            if (value == null)
                return "";
            return value;
        }

        public static bool Boolean(string value)
        {
            if (value == null)
                return false;
            if (value.Equals("true"))
                return true;
            return false;
        }

        public static int Integer(string value)
        {
            if (value == null)
                return 0;
            double v = 0;
            double.TryParse(value, out v);
            return (int) v;
        }

        public static long Long(string value)
        {
            if (value == null)
                return 0;
            double v = 0;
            double.TryParse(value, out v);
            return (long) v;
        }

        public static double Float(string value)
        {
            if (value == null)
                return 0;
            double v = 0;
            double.TryParse(value, out v);
            return v;
        }
    }

    internal class JsonArray
    {

        public int ArrayLength => InnerNode.ArrayLength;
        public FkJsonNode InnerNode { get; }
        public List<FkJsonNode> NodeList { get; }

        public JsonArray(FkJsonNode arrayNode, List<FkJsonNode> childNodes)
        {
            if (arrayNode.GetNodeType() != FkJsonNode.JsonNodeType.Array)
                throw new ArgumentException("The provided FkJsonNode must be of type Array, but a node of type " +
                                            arrayNode.GetNodeType() + " was given instead.");
            InnerNode = arrayNode;
            NodeList = childNodes;
        }

        public string GetValue(int index)
        {
            if (index >= NodeList.Count)
                return null;
            return NodeList.ElementAt(index).GetValue();
        }

        public void SetValue(string value, int index)
        {
            if (index >= NodeList.Count)
                throw new ArgumentOutOfRangeException("Array index out-of-range. Size of " + NodeList.Count +
                                                      ", with index " + index + " used.");
            NodeList.ElementAt(index).SetValue(value);
        }

        public int GetInteger(int index)
        {
            if (index >= NodeList.Count)
                return 0;
            return NodeList.ElementAt(index).GetInteger();
        }

        public void SetInteger(int value, int index)
        {
            if (index >= NodeList.Count)
                throw new ArgumentOutOfRangeException("Array index out-of-range. Size of " + NodeList.Count +
                                                      ", with index " + index + " used.");
            NodeList.ElementAt(index).SetInteger(value);
        }

        public void FromInteger(int value, int count)
        {
            NodeList.Clear();
            for (int x = 0; x < count; x++)
            {
                InnerNode.CreateInteger(null, value);
            }
        }

        public FkJsonNode NodeAtIndex(int index)
        {
            if (index >= NodeList.Count)
                return null;
            return NodeList.ElementAt(index);
        }
    }

    internal class FkJsonNode
    {
        public enum JsonNodeType
        {
            Object,
            Array,
            String,
            Boolean,
            Integer,
            Host
        }

        private List<FkJsonNode> _childNodes;
        private string _name;

        //private Dictionary<string, FkJsonNode> values;
        private FkJsonNode _parent;
        private JsonNodeType _type;

        //private FkJsonNode value_object;
        private string _value;

        public FkJsonNode()
        {
            _childNodes = new List<FkJsonNode>();
            _type = JsonNodeType.Host;
        }

        private FkJsonNode(FkJsonNode parentNode)
        {
            _childNodes = new List<FkJsonNode>();
            _parent = parentNode;
        }

        public int ArrayLength
        {
            get
            {
                if (_type != JsonNodeType.Array)
                    return -1;
                return _childNodes.Count;
            }
        }

        public int MemberCount
        {
            get
            {
                if (_type != JsonNodeType.Object)
                    return -1;
                return _childNodes.Count;
            }
        }

        [Obsolete("This provided access in a fashion that will be completely unsupported in the future. Throws exception upon usage.")]
        private List<FkJsonNode> NodeList
        {
            get
            {
                throw new InvalidOperationException("Unsupported property FkJsonNode.NodeList accessed. Consider using an UnsafeCastArray, " +
                    "if a NodeList-esque property is completely necessary.");
                // return childNodes;
            }
        }

        public JsonArray UnsafeCastArray
        {
            get
            {
                if (!(_type == JsonNodeType.Object || _type == JsonNodeType.Host))
                    throw new ArgumentException("Invalid JsonNodeType provided. CastArray can be used only to cast Object or Host type nodes, " +
                        "though a node of type " + _type + " was given.");
                JsonNodeType prevType = _type;
                _type = JsonNodeType.Array; // Temporarily set to an array
                JsonArray castArray = new JsonArray(this, _childNodes);
                _type = prevType; // Restore previous type

                return castArray;
            }
        }

        protected string FkInnerTrace(int level = 0)
        {
            string res = "";
            string i = new string(' ', level*6) + "+ ";
            i += _name + "[" + _value + "] (" + _type + ")";
            res += i + "\n";
            foreach (FkJsonNode nn in _childNodes)
            {
                res += nn.FkInnerTrace(level + 1);
            }
            return res;
        }

        public void FkTrace()
        {
            string trace = FkInnerTrace(0);
            Clipboard.SetText(trace);
        }

        public string XTrace()
        {
            return FkInnerTrace(0);
        }

        private FkJsonNode AddNode(string nodeName)
        {
            FkJsonNode nNode = new FkJsonNode(this);
            nNode.SetName(nodeName);
            _childNodes.Add(nNode);
            return nNode;
        }

        public FkJsonNode QueryMember(string memberName)
        {
            if (memberName == null)
                return null;
            foreach (FkJsonNode nn in _childNodes)
                if ((nn._name != null) && (nn._name.Equals(memberName)))
                    return nn;
            return null;
        }

        public FkJsonNode Up(int count = 1)
        {
            if (_parent == null)
                return this;
            FkJsonNode res = _parent;
            for (int x = 1; x < count; x++)
            {
                if (res._parent != null)
                    res = res._parent;
                else
                    return res;
            }
            return res;
        }

        public FkJsonNode RootNode()
        {
            FkJsonNode res = this;
            while (res._parent != null)
                res = res._parent;
            return res;
        }

        public void ClearLevel()
        {
            _childNodes = new List<FkJsonNode>();
        }

        public void SetValue(string value)
        {
            _value = value;
        }

        public string GetValue()
        {
            return _value;
        }

        public void SetBoolean(bool boolVal)
        {
            _value = boolVal ? "true" : "false";
            SetNodeType(JsonNodeType.Boolean);
        }

        public bool GetBoolean()
        {
            if (_value == null)
                return false;
            if (_value.Equals("true"))
                return true;
            return false;
        }

        public void SetInteger(int intVal)
        {
            _value = intVal.ToString();
            SetNodeType(JsonNodeType.Integer);
        }

        public int GetInteger()
        {
            if (_value == null)
                return 0;
            double v = 0;
            double.TryParse(_value, out v);
            return (int) v;
        }

        public void SetLong(long longVal)
        {
            _value = longVal.ToString();
            SetNodeType(JsonNodeType.Integer);
        }

        public long GetLong()
        {
            if (_value == null)
                return 0;
            double v = 0;
            double.TryParse(_value, out v);
            return (long) v;
        }

        public void SetFloat(double floatVal)
        {
            _value = floatVal.ToString("G0");
            SetNodeType(JsonNodeType.Integer);
        }

        public double GetFloat()
        {
            if (_value == null)
                return 0;
            double v = 0;
            double.TryParse(_value, out v);
            return v;
        }

        public JsonArray GetArray()
        {
            return new JsonArray(this, _childNodes);
        }

        public FkJsonNode GetIndexedNode(int index)
        {
            if (index >= _childNodes.Count)
                return null;
            return _childNodes.ElementAt(index);
        }

        public FkJsonNode CreateString(string name, string value)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetNodeType(JsonNodeType.String);
            subNode.SetValue(value);

            return subNode;
        }

        public FkJsonNode CreateBoolean(string name, bool value)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetBoolean(value);

            return subNode;
        }

        public FkJsonNode CreateInteger(string name, int value)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetInteger(value);

            return subNode;
        }

        public FkJsonNode CreateLong(string name, long value)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetLong(value);

            return subNode;
        }

        public FkJsonNode CreateFloat(string name, double value)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetFloat(value);

            return subNode;
        }

        public FkJsonNode CreateObject(string name)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetNodeType(JsonNodeType.Object);

            return subNode;
        }

        public FkJsonNode CreateArray(string name)
        {
            FkJsonNode subNode = AddNode(name);
            subNode.SetNodeType(JsonNodeType.Array);

            return subNode;
        }

        public void SetName(string newName)
        {
            _name = newName;
        }

        public string GetName()
        {
            return _name;
        }

        protected void SetNodeType(JsonNodeType newType)
        {
            _type = newType;
        }

        public JsonNodeType GetNodeType()
        {
            return _type;
        }

        private JsonNodeType DetermineType(string str)
        {
            if (str.StartsWith("\"") && str.EndsWith("\""))
                return JsonNodeType.String;

            if (str.Equals("true") || str.Equals("false"))
                return JsonNodeType.Boolean;

            return JsonNodeType.Integer;
        }

        protected int ProcessNodeFromChars(char[] chars, int pos)
        {
            char prevChar = '-';
            int labelStart = pos;
            bool inString = false;
            bool isDefining = false;
            bool definingString = false;
                // Used so commas don't screw things up and terminate values when defining strings.

            int lCommaPos = 0;
            bool arrayPassDefinedMember = false;
                // Shows whether any member was added to the array during this comma-pass.
            // If not, then the value between the two commas is not an object or string.
            if (_type == JsonNodeType.Array)
                lCommaPos = pos;

            while (pos < chars.Length)
            {
                var c = chars[pos];
                FkJsonNode subNode = null;
                switch (c)
                {
                    case '[':
                    case '{': // begin a new node object
                        if (!isDefining)
                        {
                            subNode = AddNode(null);
                            subNode.SetNodeType(c == '[' ? JsonNodeType.Array : JsonNodeType.Object);
                            pos = subNode.ProcessNodeFromChars(chars, pos + 1);

                            if (c == '[')
                            {
                                foreach (FkJsonNode fNode in subNode._childNodes)
                                {
                                    if (fNode._type == JsonNodeType.String)
                                    {
                                        fNode._value = fNode._name;
                                        fNode._name = null;
                                    }
                                }
                            }

                            //setNodeType(JsonNodeType.Array);

                            lCommaPos = pos;
                            arrayPassDefinedMember = true;
                        }
                        else
                        {
                            subNode = AddNode(null);
                            //subNode.setNodeType(JsonNodeType.Object);
                            if (c == '[')
                            {
                                subNode.SetNodeType(JsonNodeType.Array);
                                SetNodeType(JsonNodeType.Array);
                            }

                            else
                                subNode.SetNodeType(JsonNodeType.Object);
                            pos = subNode.ProcessNodeFromChars(chars, pos + 1);
                            // Remove the sub-node, as we're not creating a new level, but inserting it into the same definition level.
                            _childNodes = subNode._childNodes;  // Essentially kills the subNode. We use it just for processing further-
                                                                // leveled nodes and set this object-node's contents to the subNode's
                                                                // contents, so it does not exist in the tree structure and will be
                                                                // released by the GC.


                            // Fix all the new child nodes; set their parent to this node, rather that its sub-node:
                            // (This should also result in GC on the sub-node)
                            if (c != '[')
                            {
                                foreach (FkJsonNode fNode in _childNodes)
                                {
                                    fNode._parent = this;
                                }
                            }
                            else
                            // Do the same, but extra (lel)   (written as two loops just to avoid tonnes of ifs and optimize a bit >.>)
                            {
                                foreach (FkJsonNode fNode in _childNodes)
                                {
                                    fNode._parent = this;
                                    if (fNode._type == JsonNodeType.String)
                                    {
                                        fNode._value = fNode._name;
                                        fNode._name = null;
                                    }
                                }
                            }

                            return pos + 1; // This is a BUG-FIX.
                            //  - Fixes named objects not ending correctly.
                            //  - See revision history for the previous loop that used to take
                            //    this place.
                            //
                            // Previous issue:
                            //  - Generally annoying and unpredictable stacking for Clash
                            //    buildings after running into other objects.
                        }
                        break;
                    case ']':
                    case '}':
                        if (isDefining)
                        {
                            _value = TrimString(new string(chars, labelStart, pos - labelStart));
                            _type = DetermineType(_value);
                            if (_type == JsonNodeType.String)
                                _value = _value.Substring(1, _value.Length - 2);
                            // +d MessageBox.Show("defined, value (BRACE/ARRAY) = '" + value + "'\n\ntype = "+type.ToString());
                        }
                        else if (_type == JsonNodeType.Array)
                        {
                            if (!arrayPassDefinedMember)
                            {
                                string s = TrimString(new string(chars, lCommaPos, pos - lCommaPos));
                                if (s.Length > 0) // Prevents the weird 0-length, null int caused by empty arrays
                                {
                                    subNode = AddNode(null);
                                    subNode._value = s;
                                    subNode.SetNodeType(DetermineType(s));
                                }
                                // String-type impossible, as string already has a handler outside of this.
                                /*if (subNode.type == JsonNodeType.String)
                                subNode.value = subNode.value.Substring(1, subNode.value.Length - 2);*/

                                // +d MessageBox.Show("~~ new ArrayPassMember-FINAL(" + s + ") ~~");
                            }
                        }
                        return pos;
                            // !!! This is a FIX. It allows proper object-stacking. Things get really screwy without this change, since
                    //     the previous "pos + 1" [note + 1] seemed to skip things and screw over groups.

                    case ':':

                        if (!inString && !isDefining)
                        {
                            isDefining = true;
                            labelStart = pos + 1;
                        }

                        break;
                    case ',':

                        if (inString || definingString)
                            break;

                        if (isDefining)
                        {
                            _value = TrimString(new string(chars, labelStart, pos - labelStart));
                            _type = DetermineType(_value);
                            if (_type == JsonNodeType.String)
                                _value = _value.Substring(1, _value.Length - 2);
                            // +d MessageBox.Show("defined, value (FROM 'c,') = '" + value + "'\n\ntype = " + type.ToString());
                            return pos + 1;
                        }
                        if (_type == JsonNodeType.Array)
                        {
                            //MessageBox.Show("NodeType.Array, -c- reached: " + lCommaPos + ", " + pos);
                            if (!arrayPassDefinedMember)
                            {
                                string s = TrimString(new string(chars, lCommaPos, pos - lCommaPos));

                                //if (s != "" && s!=null && s.Length > 0)
                                //{
                                subNode = AddNode(null);
                                subNode._value = s;
                                subNode.SetNodeType(DetermineType(s));
                                //}

                                // String-type impossible, as string already has a handler outside of this.
                                /*if (subNode.type == JsonNodeType.String)
                                subNode.value = subNode.value.Substring(1, subNode.value.Length - 2);*/

                                // +d MessageBox.Show("~~ new ArrayPassMember(" + s + ") ~~");
                            }
                            else
                                arrayPassDefinedMember = false;

                            lCommaPos = pos + 1;


                            //arrayPassDefinedMember = false;

                            //MessageBox.Show("jnt.a, re==cv");
                            //return pos;
                        }
                        //return pos + 1; // !!! This is a FIX. It allows proper object-stacking. Things get really screwy without this, since
                        //     the '}' is sometimes skipped manually.
                        // ^ This /seemed/ to fix it, but really just screwwed stuff up :x ((actual fix above))

                        break;
                    case '"':
                        if (!isDefining)
                        {
                            if (inString)
                            {
                                if (prevChar != '\\')
                                {
                                    if (_type != JsonNodeType.Array)
                                    {
                                        //MessageBox.Show("nc::quot");
                                        subNode = AddNode(new string(chars, labelStart, pos - labelStart));
                                        pos = subNode.ProcessNodeFromChars(chars, pos + 1) - 1;
                                        inString = false;
                                    }
                                    else
                                    {
                                        subNode = AddNode(new string(chars, labelStart, pos - labelStart));
                                        subNode.SetNodeType(JsonNodeType.String);
                                        arrayPassDefinedMember = true;
                                        //pos = subNode.processNodeFromChars(chars, pos + 1) - 1;
                                        inString = false;
                                        //return pos;
                                    }
                                }
                            }
                            else
                            {
                                inString = true;
                                labelStart = pos + 1;
                            }
                        }
                        else
                        {
                            definingString = !definingString;
                        }
                        break;
                }
                prevChar = c;
                pos++;
            }
            if (isDefining && pos == chars.Length) // Must check if we're at the end, since some case-statements will just 'break;'
            {
                _value = TrimString(new string(chars, labelStart, pos - labelStart));
                _type = DetermineType(_value);
                if (_type == JsonNodeType.String)
                    _value = _value.Substring(1, _value.Length - 2);
                // +d MessageBox.Show("defined, value (##AT END##) = '" + value + "'\n\ntype = " + type.ToString());
            }
            return pos;
        }

        public void LoadString(string str)
        {
            ProcessNodeFromChars(str.ToCharArray(), 0);
        }

        protected void StringBuilderAppend(StringBuilder sb)
        {
            switch (_type)
            {
                case JsonNodeType.Integer:
                    //double v = 0.0;
                    //double.TryParse(value, out v);
                    if (!string.IsNullOrEmpty(_name))
                        sb.Append('"' + _name + '"' + ":" + _value);
                    else
                        sb.Append(_value); //v.ToString("G0");
                    break;
                case JsonNodeType.String:
                    if (!string.IsNullOrEmpty(_name))
                        sb.Append('"' + _name + '"' + ":" + '"' + _value + '"');
                    else
                        sb.Append('"' + _value + '"');
                    break;
                case JsonNodeType.Boolean:
                    if (_value.Equals("true"))
                        sb.Append('"' + _name + '"' + ":true");
                    else
                        sb.Append('"' + _name + '"' + ":false");
                    break;
                case JsonNodeType.Array:
                    if (!string.IsNullOrEmpty(_name))
                        sb.Append('"' + _name + '"' + ":[");
                    else
                        sb.Append("[");

                    int x = 0;
                    int nodeCount = _childNodes.Count;

                    foreach (FkJsonNode nn in _childNodes)
                    {
                        x++;
                        if (x == nodeCount)
                            nn.StringBuilderAppend(sb);
                        else
                        {
                            nn.StringBuilderAppend(sb);
                            sb.Append(",");
                        }
                    }

                    sb.Append("]");
                    break;
                case JsonNodeType.Object:
                    if (!string.IsNullOrEmpty(_name))
                        sb.Append('"' + _name + '"' + ":{");
                    else
                        sb.Append("{");

                    x = 0;
                    nodeCount = _childNodes.Count;
                    foreach (FkJsonNode nn in _childNodes)
                    {
                        x++;
                        if (x == nodeCount)
                            nn.StringBuilderAppend(sb);
                        else
                        {
                            nn.StringBuilderAppend(sb);
                            sb.Append(",");
                        }
                    }

                    sb.Append("}");
                    break;
                case JsonNodeType.Host:

                    x = 0;
                    nodeCount = _childNodes.Count;
                    foreach (FkJsonNode nn in _childNodes)
                    {
                        x++;
                        if (x == nodeCount)
                            nn.StringBuilderAppend(sb);
                        else
                        {
                            nn.StringBuilderAppend(sb);
                            sb.Append(",");
                        }
                    }

                    break;
            }
        }

        public string GetJsonString()
        {
            StringBuilder sb = new StringBuilder("");
            StringBuilderAppend(sb);

            return sb.ToString();
        }


        private string TrimString(string str)
        {
            return str.Trim();
        }


        [Obsolete("Use stringBuilderAppend instead, as it is about 5x faster now.")]
        protected string ToStr()
        {
            switch (_type)
            {
                case JsonNodeType.Integer:
                    //double v = 0.0;
                    //double.TryParse(value, out v);
                    if (!string.IsNullOrEmpty(_name))
                        return '"' + _name + '"' + ":" + _value; //v.ToString("G0");
                    return _value; //v.ToString("G0");
                case JsonNodeType.String:
                    if (!string.IsNullOrEmpty(_name))
                        return '"' + _name + '"' + ":" + '"' + _value + '"';
                    return '"' + _value + '"';
                case JsonNodeType.Boolean:
                    if (_value.Equals("true"))
                        return '"' + _name + '"' + ":true";
                    return '"' + _name + '"' + ":false";
                case JsonNodeType.Array:
                    string res;
                    if (!string.IsNullOrEmpty(_name))
                        res = '"' + _name + '"' + ":[";
                    else
                        res = "[";

                    int x = 0;
                    int nodeCount = _childNodes.Count;
                    /*if (nodeCount == 1)
                {
                    FkJsonNode n0 = childNodes.First();
                    if ((n0.value == null || n0.value == "") && n0.type == JsonNodeType.Integer)    // Empty array support
                        return res + "]";                                                           // Created with 1 pure-null/empty int.
                }*/

                    foreach (FkJsonNode nn in _childNodes)
                    {
                        x++;
                        if (x == nodeCount)
                            res += nn.ToStr();
                        else
                            res += nn.ToStr() + ",";
                    }

                    return res + "]";
                case JsonNodeType.Object:
                    if (!string.IsNullOrEmpty(_name))
                        res = '"' + _name + '"' + ":{";
                    else
                        res = "{";

                    x = 0;
                    nodeCount = _childNodes.Count;
                    foreach (FkJsonNode nn in _childNodes)
                    {
                        x++;
                        if (x == nodeCount)
                            res += nn.ToStr();
                        else
                            res += nn.ToStr() + ",";
                    }

                    return res + "}";
                case JsonNodeType.Host:
                    res = "";

                    x = 0;
                    nodeCount = _childNodes.Count;
                    foreach (FkJsonNode nn in _childNodes)
                    {
                        x++;
                        if (x == nodeCount)
                            res += nn.ToStr();
                        else
                            res += nn.ToStr() + ",";
                    }

                    return res;
            }

            return "";
        }
    }
}