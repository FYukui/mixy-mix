﻿using System;

namespace YukiNoKagami.ServerData
{
    static class ClashNames
    {
        public static String[] Buildings = {
            "Army Camp",
            "Town Hall",
            "Elixir Pump",
            "Elixir Storage",
            "Gold Mine",
            "Gold Storage",
            "Barracks",
            "Laboratory",
            "Cannon",
            "Archer Tower",
            "Wall",
            "Wizard Tower",
            "Air Defense",
            "Mortar",
            "Clan Castle",
            "Builder's Hut",
            "Unknown",
            "Goblin Town Hall",
            "Goblin Hut",
            "Hidden Tesla",
            "Spell Factory",
            "X-Bow",
            "Barbarian King",
            "Dark Elxir Drill",
            "Dark Elixir Storage",
            "Archer Queen",
            "Dark Barracks",
            "Inferno Tower",
            "Air Sweeper",
            "Dark Spell Factory"
        };
    }
}
