﻿
namespace YukiNoKagami.ServerData
{

    static class ClashTypes
    {

        public enum Building
        {
            ArmyCamp                = 1000000,
            TownHall                = 1000001,
            ElixirPump              = 1000002,
            ElixirStorage           = 1000003,
            GoldMine                = 1000004,
            GoldStorage             = 1000005,
            Barracks                = 1000006,
            Laboratory              = 1000007,
            Cannon                  = 1000008,
            ArcherTower             = 1000009,
            Wall                    = 1000010,
            WizardTower             = 1000011,
            AirDefense              = 1000012,
            Mortar                  = 1000013,
            ClanCastle              = 1000014,
            BuilderHut              = 1000015,
            UnknownB0               = 1000016,
            GoblinTownHall          = 1000017,
            GolbinHut               = 1000018,
            HiddenTesla             = 1000019,
            SpellFactory            = 1000020,
            XBow                    = 1000021,
            BarbarianKing           = 1000022,
            DarkElixirDrill         = 1000023,
            DarkElixirStorage       = 1000024,
            ArcherQueen             = 1000025,
            DarkBarracks            = 1000026,
            InfernoTower            = 1000027,
            AirSweeper              = 1000028,
            DarkSpellFactory        = 1000029
        }

        public enum Spells
        {
            Lightning       = 26000000,
            Heal            = 26000001,
            Rage            = 26000002,
            Jump            = 26000003,
            SantaSurprise   = 26000004,
            Freeze          = 26000005,
            SantaStrike     = 26000006,
            Slow            = 26000007,
            BoostDefence    = 26000008,
            Poison          = 26000009,
            Earthquake      = 26000010,
            Haste           = 26000011
        }

        public enum Resource
        {
            Gems            = 3000000,
            Gold            = 3000001,
            Elixir          = 3000002,
            DarkElixir      = 3000003,
            WarGold         = 3000004,
            WarElixir       = 3000005,
            WarDarkElixir   = 3000006
        }

        public enum Troop
        {
            Barbarian               = 4000000,
            Archer                  = 4000001,
            Goblin                  = 4000002,
            Giant                   = 4000003,
            WallBreaker             = 4000004,
            Balloon                 = 4000005,
            Wizard                  = 4000006,
            Healer                  = 4000007,
            Dragon                  = 4000008,
            PEKKA                   = 4000009,
            Minion                  = 4000010,
            HogRider                = 4000011,
            Valkyrie                = 4000012,
            Golem                   = 4000013,
            Golemite                = 4000014,
            Witch                   = 4000015,
            Skeleton                = 4000016,
            LavaHound               = 4000017,
            LavaPup                 = 4000018,
            SkeletonTrapGround      = 4000019,
            MinionTrap              = 4000020,
            SkeletonTrapAir         = 4000021
        }


    }
}