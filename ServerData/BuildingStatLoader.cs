﻿using YukiNoKagami.Informatics;

namespace YukiNoKagami.ServerData
{
    static class BuildingStatLoader
    {

        public static void Load()
        {
            BuildingEntry e;

            // WALLS
            e = BuildingStats.CreateBuilding("Walls", ClashTypes.Building.Wall, 11, ClashTypes.Resource.Gold);
            //        LVL   GOLD        HP      TH
            e.AddLevel(0,   50,         300,    2);
            e.AddLevel(1,   1000,       500,    2);
            e.AddLevel(2,   5000,       700,    3);
            e.AddLevel(3,   10000,      900,    4);
            e.AddLevel(4,   30000,      1400,   5);
            e.AddLevel(5,   75000,      2000,   6);
            e.AddLevel(6,   200000,     2500,   7);
            e.AddLevel(7,   500000,     3000,   8);
            e.AddLevel(8,   1000000,    4000,   9);
            e.AddLevel(9,   3000000,    5500,   9);
            e.AddLevel(10,  4000000,    7000,   10);
        }

    }
}