﻿using YukiNoKagami.Informatics;

namespace YukiNoKagami.ServerData
{
    static class TroopStatLoader
    {

        public static void Load()
        {
            TroopEntry e;

            // BARBARIAN
            e = TroopStats.CreateTroop("Barbarian", ClashTypes.Troop.Barbarian, 7, 1, 20, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       8,      45,      25,    0,      0);
            e.AddLevel(1,       11,     54,      40,    1,      0);
            e.AddLevel(2,       14,     65,      60,    3,      1);
            e.AddLevel(3,       18,     78,     100,    5,      1);
            e.AddLevel(4,       23,     95,     150,    6,      2);
            e.AddLevel(5,       26,     110,    200,    7,      3);
            e.AddLevel(6,       30,     125,    250,    8,      4);


            // ARCHER
            e = TroopStats.CreateTroop("Archer", ClashTypes.Troop.Archer, 7, 1, 25, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       7,      20,     50,     0,      5);
            e.AddLevel(1,       9,      23,     80,     1,      5);
            e.AddLevel(2,       12,     28,     120,    3,      6);
            e.AddLevel(3,       16,     33,     200,    5,      6);
            e.AddLevel(4,       20,     40,     300,    6,      7);
            e.AddLevel(5,       22,     44,     400,    7,      8);
            e.AddLevel(6,       25,     48,     500,    8,      9);


            // GOBLIN       (4th unlocked troop, but last Tier 1. Giant is the 3rd unlocked, but still listed as 4th in the Y\m istandard)
            e = TroopStats.CreateTroop("Goblin", ClashTypes.Troop.Goblin, 6, 1, 30, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       11,     25,     25,     0,      10);
            e.AddLevel(1,       14,     30,     40,     1,      10);
            e.AddLevel(2,       19,     36,     60,     3,      11);
            e.AddLevel(3,       24,     43,     80,     5,      11);
            e.AddLevel(4,       32,     52,     100,    6,      12);
            e.AddLevel(5,       42,     68,     150,    8,      13);


            // GIANT
            e = TroopStats.CreateTroop("Giant", ClashTypes.Troop.Giant, 7, 5, 120, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       11,     300,    250,    0,      14);
            e.AddLevel(1,       14,     360,    750,    2,      14);
            e.AddLevel(2,       19,     430,    1250,   4,      15);
            e.AddLevel(3,       24,     520,    1750,   5,      15);
            e.AddLevel(4,       31,     670,    2250,   6,      16);
            e.AddLevel(5,       43,     940,    3000,   7,      17);
            e.AddLevel(6,       50,     1100,   3500,   8,      18);


            // WALL BREAKER
            e = TroopStats.CreateTroop("Wall Breaker", ClashTypes.Troop.WallBreaker, 6, 2, 120, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       12,     20,     1000,   0,      19);
            e.AddLevel(1,       16,     24,     1500,   2,      19);
            e.AddLevel(2,       24,     29,     2000,   4,      20);
            e.AddLevel(3,       32,     35,     2500,   5,      20);
            e.AddLevel(4,       46,     42,     3000,   6,      21);
            e.AddLevel(5,       60,     54,     3500,   8,      22);


            // BALLOON
            e = TroopStats.CreateTroop("Balloon", ClashTypes.Troop.Balloon, 6, 5, 480, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       25,     150,    2000,   0,      23);
            e.AddLevel(1,       32,     180,    2500,   2,      23);
            e.AddLevel(2,       48,     216,    3000,   4,      24);
            e.AddLevel(3,       72,     280,    3500,   5,      24);
            e.AddLevel(4,       108,    390,    4000,   6,      25);
            e.AddLevel(5,       162,    545,    4500,   7,      26);


            // WIZARD
            e = TroopStats.CreateTroop("Wizard", ClashTypes.Troop.Wizard, 6, 4, 480, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       50,     75,     1500,   0,      27);
            e.AddLevel(1,       70,     90,     2000,   3,      27);
            e.AddLevel(2,       90,     108,    2500,   4,      28);
            e.AddLevel(3,       125,    130,    3000,   5,      28);
            e.AddLevel(4,       170,    156,    3500,   6,      29);
            e.AddLevel(5,       180,    164,    4000,   8,      30);


            // HEALER
            e = TroopStats.CreateTroop("Healer", ClashTypes.Troop.Healer, 4, 14, 900, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       35,     500,    5000,   0,      31);
            e.AddLevel(1,       42,     600,    6000,   5,      31);
            e.AddLevel(2,       55,     840,    8000,   6,      32);
            e.AddLevel(3,       71,     1170,   10000,  7,      32);


            // DRAGON
            e = TroopStats.CreateTroop("Dragon", ClashTypes.Troop.Dragon, 5, 20, 1800, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       140,    1900,   25000,  0,      33);
            e.AddLevel(1,       160,    2100,   29000,  5,      34);
            e.AddLevel(2,       180,    2300,   33000,  6,      35);
            e.AddLevel(3,       200,    2500,   37000,  7,      36);
            e.AddLevel(4,       220,    2700,   42000,  8,      37);


            // PEKKA
            e = TroopStats.CreateTroop("P.E.K.K.A", ClashTypes.Troop.PEKKA, 5, 25, 2700, ClashTypes.Resource.Elixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0, 240, 2800, 28000, 0, 38);
            e.AddLevel(1, 270, 3100, 32000, 6, 39);
            e.AddLevel(2, 300, 3500, 36000, 6, 40);
            e.AddLevel(3, 340, 4000, 40000, 8, 41);
            e.AddLevel(4, 380, 4500, 45000, 8, 42);


            // MINION
            e = TroopStats.CreateTroop("Minion", ClashTypes.Troop.Minion, 6, 2, 45, ClashTypes.Resource.DarkElixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       35,     55,     6,      0,      43);
            e.AddLevel(1,       38,     60,     7,      5,      43);
            e.AddLevel(2,       42,     66,     8,      6,      44);
            e.AddLevel(3,       46,     72,     9,      6,      44);
            e.AddLevel(4,       50,     78,     10,     7,      45);
            e.AddLevel(5,       54,     84,     11,     8,      46);


            // HOG RIDER
            e = TroopStats.CreateTroop("Hog Rider", ClashTypes.Troop.HogRider, 5, 5, 120, ClashTypes.Resource.DarkElixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       60,     270,    40,     0,      47);
            e.AddLevel(1,       70,     312,    45,     5,      47);
            e.AddLevel(2,       80,     360,    52,     6,      48);
            e.AddLevel(3,       92,     415,    58,     6,      48);
            e.AddLevel(4,       105,    475,    65,     7,      49);


            // VALKYRIE
            e = TroopStats.CreateTroop("Valkyrie", ClashTypes.Troop.Valkyrie, 4, 8, 480, ClashTypes.Resource.DarkElixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       88,     900,    70,     0,      50);
            e.AddLevel(1,       99,     1000,   100,    6,      50);
            e.AddLevel(2,       111,    1100,   130,    7,      51);
            e.AddLevel(3,       124,    1200,   160,    7,      51);


            // GOLEM
            e = TroopStats.CreateTroop("Golem", ClashTypes.Troop.Golem, 5, 30, 2700, ClashTypes.Resource.DarkElixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       38,     4500,   450,    0,      52);
            e.AddLevel(1,       42,     5000,   525,    6,      52);
            e.AddLevel(2,       46,     5500,   600,    7,      53);
            e.AddLevel(3,       50,     6000,   675,    7,      53);
            e.AddLevel(4,       54,     6300,   750,    8,      54);


            // WITCH
            e = TroopStats.CreateTroop("Witch", ClashTypes.Troop.Witch, 2, 12, 1200, ClashTypes.Resource.DarkElixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       25,     75,     250,    0,      55);
            e.AddLevel(1,       30,     100,    350,    7,      55);


            // LAVA HOUND
            e = TroopStats.CreateTroop("Lava Hound", ClashTypes.Troop.LavaHound, 3, 30, 2700, ClashTypes.Resource.DarkElixir);
            //       LEVEL     DPS      HP      COST    LAB     Index
            e.AddLevel(0,       10,     5700,   390,    0,      56);
            e.AddLevel(1,       12,     6200,   450,    7,      56);
            e.AddLevel(2,       14,     6700,   510,    8,      57);



        }

    }
}