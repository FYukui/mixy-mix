﻿using System;
using System.Windows.Forms;

namespace YukiNoKagami.Logging
{
    internal class EventLogger
    {
        public delegate void DlString(string eventDescription);
        public delegate void DlStringString(string eventDescription, string eventDetails);
        public delegate void DlVoid();

        private readonly Label _lastEventLabel;
        private readonly TreeView _logTree;

        public EventLogger(TreeView eventLogTree, Label eventNotifyLabel = null)
        {
            _logTree = eventLogTree;
            _lastEventLabel = eventNotifyLabel;
        }

        public void LogBasicEvent(string eventDescription)
        {
            if (_logTree.InvokeRequired)
                _logTree.Invoke(new DlString(LogBasicEvent), eventDescription);
            else
            {
                if ((_lastEventLabel != null) && (eventDescription.Length > 0))
                    _lastEventLabel.Text = "(" + eventDescription + ")";

                eventDescription = "[" + DateTime.Now.ToLongTimeString() + "] " + eventDescription;
                TreeNode nn = new TreeNode(eventDescription);
                _logTree.Nodes.Insert(0, nn);
            }
        }

        public void LogBasicEvent(string eventDescription, string eventDetails)
        {
            if (_logTree.InvokeRequired)
                _logTree.Invoke(new DlStringString(LogBasicEvent), eventDescription, eventDetails);
            else
            {
                if ((_lastEventLabel != null) && (eventDescription.Length > 0))
                    _lastEventLabel.Text = "(" + eventDescription + ")";

                eventDescription = "[" + DateTime.Now.ToLongTimeString() + "] " + eventDescription;
                TreeNode nn = new TreeNode(eventDescription);
                nn.ToolTipText = eventDetails;
                _logTree.Nodes.Insert(0, nn);
            }
        }

        public void Clear()
        {
            if (_logTree.InvokeRequired)
                _logTree.Invoke(new DlVoid(Clear));
            else
                _logTree.Nodes.Clear();
        }
    }
}