﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using YukiNoKagami.MessageUtils.MessageHandling;
using Message = YukiNoKagami.MessageUtils.MessageHandling.Message;

namespace YukiNoKagami.Logging
{
    internal class PacketLogger
    {
        private const int MaxLogCount = 500;

        private readonly ListView _logListView;
        private readonly List<Message> _packetList;

        public PacketLogger(ListView packetLogListView)
        {
            _logListView = packetLogListView;
            _packetList = new List<Message>();
        }

        private static string GetMessageHexString(Message m)
        {
            return BitConverter.ToString(m.GetDecryptedData() ?? m.GetData(), 0, Math.Min(60, m.GetSize()));
        }

        public Message GetIndexedMessage(int index)
        {
            return _packetList.ElementAt(index);
        }

        public void LogMessage(Message m)
        {
            if (_logListView.InvokeRequired)
                _logListView.Invoke(new DlMessage(LogMessage), m);
            else
            {
                _packetList.Add(m);
                if (_packetList.Count > MaxLogCount)
                {
                    _packetList.RemoveRange(0, 1);
                    _logListView.Items.RemoveAt(MaxLogCount - 1);
                }

                ListViewItem nItem = new ListViewItem();
                if (MessageInfo.GetSource(m) == MessageInfo.MessageSource.Server)
                {
                    nItem.ForeColor = Color.FromArgb(0, 0, 255);
                }
                nItem.Text = m.GetTime().ToLongTimeString();                    // Time
                nItem.SubItems.Add(m.GetID().ToString());                       // ID
                nItem.SubItems.Add(MessageInfo.GetName(m));                     // Name
                nItem.SubItems.Add(MessageInfo.GetSource(m).ToString());        // Source (Server/Client)
                nItem.SubItems.Add(m.GetSize().ToString());                     // Size (bytes)
                nItem.SubItems.Add(GetMessageHexString(m));                     // Data (hex preview)

                _logListView.Items.Insert(0, nItem);
            }
        }


        public void ClearLog()
        {
            if (_logListView.InvokeRequired)
                _logListView.Invoke(new DlVoid(ClearLog));
            else
            {
                _packetList.Clear();
                _logListView.Items.Clear();
            }
        }

        public void SaveLog(string fileName)
        {
            StringWriter sw = new StringWriter();

            int x = 1;

            foreach (Message m in _packetList)
            {
                sw.WriteLine("[" + m.GetTime().ToLongTimeString() + "]" + " #" + x + " - ID:" + m.GetID() +
                             " - Type:" + MessageInfo.GetType(m) + " - Source:" + MessageInfo.GetSource(m) + " - Size:" +
                             m.GetSize());
                byte[] decrypted = m.GetDecryptedData();
                if (decrypted != null)
                    sw.WriteLine(BitConverter.ToString(decrypted));
                sw.WriteLine("<encrypted>");
                sw.WriteLine(BitConverter.ToString(m.GetData()));
                sw.WriteLine("</encrypted>");
                sw.WriteLine("");
                x++;
            }

            StreamWriter outFile = new StreamWriter(fileName);
            outFile.Write(sw);
            outFile.Close();

            MessageBox.Show("Packet log saved!", "Info:", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private delegate void DlMessage(Message m);

        private delegate void DlVoid();
    }
}