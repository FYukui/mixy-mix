﻿using System;
using YukiNoKagami.Ciphers;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.Listeners
{
    internal class MKeyListener : IMessageFilterHandler
    {
        private readonly ICipher _clientCipherReference;
        private readonly ICipher _serverCipherReference;

        private ClashScrambler _clashRandom;
        private int _clientSeed;
        private byte[] _key;

        public MKeyListener(ICipher clientCipher, ICipher serverCipher)
        {
            _clientCipherReference = clientCipher;
            _serverCipherReference = serverCipher;
        }

        public void OnMessage(Message m)
        {
            switch (MessageInfo.GetType(m))
            {
                case MessageInfo.MessageType.Login:
                    byte[] data = m.GetDecryptedData();

                    _clientSeed = (data[304] << 24) | (data[305] << 16) | (data[306] << 8) | (data[307]);
                    _clashRandom = new ClashScrambler(_clientSeed);

                    break;
                case MessageInfo.MessageType.Encryption:
                    data = m.GetDecryptedData();
                    int nonceLen = (data[7] << 24) | (data[8] << 16) | (data[9] << 8) | data[10];

                    byte[] nonce = new byte[nonceLen];
                    Array.Copy(data, 11, nonce, 0, nonceLen);

                    _key = _clashRandom.Scramble(nonce);

                    if ((_clientCipherReference == null) || (_serverCipherReference == null))
                        throw new InvalidOperationException("cipherReference undefined!");
                    _clientCipherReference.SetKey(_key);
                    _serverCipherReference.SetKey(_key);

                    break;
            }
        }
    }
}