﻿using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.Listeners
{
    internal class MProxyRecord : IMessageFilterHandler
    {
        public void OnMessage(Message m)
        {
            string expFile = "Proxy/packet_record/server/sequence_login/";

            switch (MessageInfo.GetType(m))
            {
                /* 
                    -> Special cases here <-
                    The default handler works fine for all inputs now, though.
                */
                default:
                    expFile += MessageInfo.GetName(m);
                    break;
            }

            MessageWriter msgWriter = new MessageWriter(m);
            msgWriter.SaveToFile(expFile);
        }

        public void RegisterMessages(MessageFilter filter)
        {
            filter.AddFilter(MessageInfo.MessageType.OwnHomeData, this);
            //filter.addFilter(MessageInfo.MessageType.VisitedHomeData, this);
            filter.AddFilter(MessageInfo.MessageType.AllianceData, this);
            filter.AddFilter(MessageInfo.MessageType.AllianceData2, this);
            filter.AddFilter(MessageInfo.MessageType.AllianceStream, this);
            filter.AddFilter(MessageInfo.MessageType.AvatarStream, this);
            filter.AddFilter(MessageInfo.MessageType.WarLog, this);
            filter.AddFilter(MessageInfo.MessageType.UCMWarDetails, this);
            filter.AddFilter(MessageInfo.MessageType.UCMWarDetails, this);
            filter.AddFilter(MessageInfo.MessageType.LoginOk, this);
            filter.AddFilter(MessageInfo.MessageType.Encryption, this);
        }
    }
}