﻿using System;
using System.Collections.Generic;
using YukiNoKagami.Informatics;
using YukiNoKagami.Logging;
using YukiNoKagami.MessageUtils.LFormat;
using YukiNoKagami.MessageUtils.MessageHandling;
using YukiNoKagami.ServerData;

namespace YukiNoKagami.Listeners
{
    //
    // DAFUQ IS THIS MESS??!?
    // ... I don't even. It's just. I. Umm.. Yeah.

    internal class MVillageListener : IMessageFilterHandler
    {
        private static bool _isFirstSelfLoad = true;
        private static readonly int[] ArmyCampCapacity = {20, 30, 35, 40, 45, 50, 55, 60};

        public void OnMessage(Message m)
        {
            switch (MessageInfo.GetType(m))
            {
                case MessageInfo.MessageType.OwnHomeData:
                    HandleOwnHomeData(m);
                    return;
                case MessageInfo.MessageType.VisitedHomeData:
                    HandleVisitedHomeData(m);
                    return;
            }
        }


        private void BaseHomeDataLoader(Message m, string normalizedName)
        {
            MessageDataWrapper dataWrapper = new MessageDataWrapper();
            dataWrapper.SetMessage(m);

            KagamiDataCollection homeData = KagamiDataHost.GetFromNormalizedName(normalizedName);
            homeData.Reset();
            homeData.ReadMessageData(dataWrapper);

            /*
            MessageWriter mwrite = new MessageWriter(m.GetID());
            ((KagamiDataCollection)homeData.getValue("user")).queryField("userName").Value = "wiiness";
            homeData.writeMessageData(mwrite);
            mwrite.setUnknown(0);
            mwrite.FinalizeMessage();
            mwrite.saveToFile("c:/users/fyuku/desktop/homedata.cmsg");
            */

            VillageStats.ClearAll();
            VillageStats.SetAge((int) homeData.GetValue("age"));
            if (homeData.QueryField("shieldTime") != null)
                VillageStats.SetShieldTime((int) homeData.GetValue("shieldTime"));
            else
                VillageStats.SetShieldTime(0);


            //
            // Handle wall and resource calculations:
            //
            string villageData = (string) homeData.GetValue("villageData");

            FkJsonNode villageDataParser = new FkJsonNode();
            villageDataParser.LoadString(villageData);
            villageDataParser = villageDataParser.GetIndexedNode(0); // Get the first object (only) in the Host

            int[] wallCounts = new int[11];
            ClashTroopBatch[] troopCounts = new ClashTroopBatch[21];
            for (int x = 0; x < troopCounts.Length; x++)
            {
                troopCounts[x] = new ClashTroopBatch {TroopID = 4000000 + x};
            }


            List<FkJsonNode> barrackNodes = new List<FkJsonNode>();
            JsonArray buildings = villageDataParser.QueryMember("buildings").GetArray();
            int troopTotalCapacity = 0;
            int builderCount = 0;
            foreach (FkJsonNode blding in buildings.NodeList)
            {
                FkJsonNode ndata = blding.QueryMember("data");

                if (ndata != null)
                {
                    ClashTypes.Building t = (ClashTypes.Building) ndata.GetInteger();
                    switch (t)
                    {
                        case ClashTypes.Building.Wall:
                            wallCounts[blding.QueryMember("lvl").GetInteger()]++;
                            break;
                        case ClashTypes.Building.ArmyCamp:

                            // units stored as: "units":[[4000006,13],[4000001,6]]
                            troopTotalCapacity += ArmyCampCapacity[blding.QueryMember("lvl").GetInteger()];
                            FkJsonNode nodeUnits = blding.QueryMember("units");
                            if (nodeUnits != null)
                            {
                                JsonArray units = nodeUnits.GetArray();
                                foreach (FkJsonNode unitInnerNode in units.NodeList)
                                {
                                    JsonArray unitCount = unitInnerNode.GetArray();
                                    troopCounts[unitCount.GetInteger(0) - 4000000].Count += unitCount.GetInteger(1);
                                }
                            }
                            break;
                        case ClashTypes.Building.Barracks:
                        case ClashTypes.Building.DarkBarracks:
                            barrackNodes.Add(blding);
                            break;
                        case ClashTypes.Building.BuilderHut:
                            builderCount++;
                            break;
                        case ClashTypes.Building.ArcherQueen:
                        case ClashTypes.Building.BarbarianKing:
                            FkJsonNode node_hero_upgrade = blding.QueryMember("hero_upg");
                            if (node_hero_upgrade != null)
                            {
                                int constructionTime = node_hero_upgrade.QueryMember("t").GetInteger();
                                int buildingLevel = -1;
                                FkJsonNode p = null;
                                if ((p = blding.QueryMember("level")) != null)
                                    // This might* be nulled, if the building is just being constructed.
                                    buildingLevel = p.GetInteger();
                                VillageStats.InsertBuilder(constructionTime, buildingLevel, t); // t set just up above
                            }

                            break;
                    }

                    // Grab some builder info while we're iterating the buildings :3
                    FkJsonNode node_construction_time = blding.QueryMember("const_t");
                    if (node_construction_time != null)
                    {
                        int constructionTime = node_construction_time.GetInteger();
                        int buildingLevel = -1;
                        FkJsonNode p = null;
                        if ((p = blding.QueryMember("lvl")) != null)
                            // This might* be nulled, if the building is just being constructed.
                            buildingLevel = p.GetInteger();
                        VillageStats.InsertBuilder(constructionTime, buildingLevel, t); // t set just up above
                    }
                }
            }

            for (int x = wallCounts.Length - 1; x >= 0; x--) // Backwards loop, so highest leveled walls are on top :3
            {
                if (wallCounts[x] > 0)
                    VillageStats.InsertWallType(x, wallCounts[x]);
            }
            VillageStats.SetMaxTroops(troopTotalCapacity);
            VillageStats.SetTroopCounts(troopCounts); // Set the troop counts as found above
            VillageStats.SetBuilderCount(builderCount);

            //
            // Get some user data (name, id, level) along with a bit of clan data (name, id, level):
            //

            KagamiDataCollection user = (KagamiDataCollection) homeData.GetValue("user");
            VillageStats.SetUserInfo((string) user.GetValue("userName"), (int) user.GetValue("level"),
                (long) user.GetValue("userId"));
            if (user.GetValue("exp") != null)
                VillageStats.SetUserExp((int) user.GetValue("exp"));

            KagamiDataCollection clan = (KagamiDataCollection) user.GetValue("clan");
            if ((clan != null) && (clan.InstanceIsExpressed))
                VillageStats.SetClanInfo((string) clan.GetValue("clanName"), (int) clan.GetValue("rank"),
                    (long) clan.GetValue("clanId"));


            //
            // Get user troop levels, so they can be applied to any batches in army camps
            //

            // No need to set all levels to 0, as VillageStats.clearAll calls [clearTroopLevels]

            KagamiDataCollection resources = (KagamiDataCollection) homeData.GetValue("resources");
            if (resources.QueryField("unitLevels") != null)
            {
                object[] unitLevels = (object[]) resources.GetValue("unitLevels");
                    // unitLevels array of ResourceComponent
                KagamiVariantType ulVariant = resources.QueryField("unitLevels");
                if (ulVariant.ArrayLength > 0)
                    // Only load levels if there are any upgraded (there won't be on a new account)
                {
                    //KagamiDataCollection[] levelsArray = (KagamiDataCollection[])unitLevels.getArray();
                    for (int x = 0; x < ulVariant.ArrayLength; x++)
                    {
                        int unitType = (int) ((KagamiDataCollection) unitLevels[x]).GetValue("type");
                        int unitLevel = (int) ((KagamiDataCollection) unitLevels[x]).GetValue("value");
                        troopCounts[unitType - 4000000].Level = unitLevel;
                        VillageStats.SetTroopLevel((ClashTypes.Troop) unitType, unitLevel);
                    }
                }
            }

            //
            // Gather alliance/clan-castle troop information, if available
            //
            List<ClashTroopBatch> castleTroops = new List<ClashTroopBatch>();
            if (resources.QueryField("allianceUnits") != null)
            {
                object[] allianceUnits = (object[]) resources.GetValue("allianceUnits");
                // No need to check for length > 0, as for-loop does this already xD. Above troop-level code is weird and does it anyones (no point)
                for (int x = allianceUnits.Length - 1; x >= 0; x--)
                {
                    // The collection in this array is of type UnitComponent : {int typeId, int count, int level)
                    int unitType = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("typeId");
                    int unitCount = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("count");
                    int unitLevel = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("level");
                    if (unitCount > 0)
                    {
                        ClashTroopBatch castleTroopBatch = new ClashTroopBatch
                        {
                            TroopID = unitType,
                            Level = unitLevel,
                            Count = unitCount
                        };
                        castleTroops.Add(castleTroopBatch);
                    }
                }
            }
            VillageStats.SetClanTroops(castleTroops);


            //
            // Handle barrack nodes stored earlier
            //
            foreach (FkJsonNode bNode in barrackNodes)
            {
                bool isDarkBarrackNode = (ClashTypes.Building) bNode.QueryMember("data").GetInteger() == ClashTypes.Building.DarkBarracks;

                FkJsonNode unit_prod_node = bNode.QueryMember("unit_prod");
                if (unit_prod_node != null)
                {
                    FkJsonNode timeRemainingNode = unit_prod_node.QueryMember("t");
                    FkJsonNode slots_node = unit_prod_node.QueryMember("slots");
                    if ((slots_node != null) && (timeRemainingNode != null))
                    {
                        int barrackTimeRemaining = timeRemainingNode.GetInteger();
                        JsonArray trainingSlots = slots_node.GetArray();
                        if (trainingSlots.ArrayLength > 0) // Only create the TroopBatch if arrayLen is > 0
                        {
                            ClashTroopBatch[] barrackTroops = new ClashTroopBatch[trainingSlots.ArrayLength];
                            int btIndex = 0;
                            foreach (FkJsonNode unitNode in trainingSlots.NodeList)
                            {
                                barrackTroops[btIndex] = new ClashTroopBatch
                                {
                                    TroopID = unitNode.QueryMember("id").GetInteger(),
                                    Count = unitNode.QueryMember("cnt").GetInteger()
                                };
                                btIndex++;
                            }

                            VillageStats.InsertBarracksInfo(barrackTroops, barrackTimeRemaining, isDarkBarrackNode);
                        }
                    }
                }
            }


            //
            // Grab cooldown/spawn info
            //
            FkJsonNode nodeRespawnVars = villageDataParser.QueryMember("respawnVars");
            if (nodeRespawnVars != null)
            {
                // Assert that these keys are defined, as the respawnVars were included with the data...
                int secondsFromLastRespawn = nodeRespawnVars.QueryMember("secondsFromLastRespawn").GetInteger();
                int obstacleClearCounter = nodeRespawnVars.QueryMember("obstacleClearCounter").GetInteger();
                int time_to_gembox_drop = nodeRespawnVars.QueryMember("time_to_gembox_drop").GetInteger();
                VillageStats.SetSpawnInfo(time_to_gembox_drop, secondsFromLastRespawn, obstacleClearCounter);
            }
            else
            {
                VillageStats.SetSpawnInfo(0, 0, 0);
            }


            VillageStats.RenderSelectedTroops();
            VillageStats.RenderInfoPlusStatic();

            // Only log if not OwnHomeData, as that handles its own logging.
            if (MessageInfo.GetType(m) != MessageInfo.MessageType.OwnHomeData)
            {
                LoggingProvider.EventLog.LogBasicEvent(DateTime.Now.ToLongTimeString() + ": Home data loaded for '" 
                    + (string) user.GetValue("userName") + "'");
            }
        }

        private void HandleOwnHomeData(Message m)
        {
            if (_isFirstSelfLoad)
            {
                LoggingProvider.EventLog.LogBasicEvent(DateTime.Now.ToLongTimeString() + ": Home data loaded for self.");
                _isFirstSelfLoad = false;
            }
            else
                LoggingProvider.EventLog.LogBasicEvent(DateTime.Now.ToLongTimeString() + ": Home data reloaded for self.");

            BaseHomeDataLoader(m, "OwnHomeData");
        }

        private void HandleVisitedHomeData(Message m)
        {
            BaseHomeDataLoader(m, "VisitedHomeData");
        }

        public void RegisterMessages(MessageFilter filter)
        {
            filter.AddFilter(MessageInfo.MessageType.OwnHomeData, this);
            filter.AddFilter(MessageInfo.MessageType.VisitedHomeData, this);
        }


        [Obsolete("Just, no... Don't even touch this. Please!")]
        private void bak_handleOwnHomeData(Message m)
        {
            MessageDataWrapper dataWrapper = new MessageDataWrapper();
            dataWrapper.SetMessage(m);

            KagamiDataCollection homeData = KagamiDataHost.GetFromNormalizedName("OwnHomeData");
            homeData.Reset();
            homeData.ReadMessageData(dataWrapper);

            if (_isFirstSelfLoad)
            {
                LoggingProvider.EventLog.LogBasicEvent(DateTime.Now.ToLongTimeString() + ": Home data loaded for self.");
                _isFirstSelfLoad = false;
            }
            else
                LoggingProvider.EventLog.LogBasicEvent(DateTime.Now.ToLongTimeString() +
                                                       ": Home data reloaded for self.");

            VillageStats.ClearAll();

            //
            // Handle wall and resource calculations:
            //
            string villageData = (string) homeData.GetValue("villageData"); // zstream villageData

            FkJsonNode villageDataParser = new FkJsonNode();
            villageDataParser.LoadString(villageData);
            villageDataParser = villageDataParser.GetIndexedNode(0); // Get the first object (only) in the Host

            int[] wallCounts = new int[11];
            ClashTroopBatch[] troopCounts = new ClashTroopBatch[21];
            for (int x = 0; x < troopCounts.Length; x++)
            {
                troopCounts[x] = new ClashTroopBatch {TroopID = 4000000 + x};
            }

            JsonArray buildings = villageDataParser.QueryMember("buildings").GetArray();
            foreach (FkJsonNode blding in buildings.NodeList)
            {
                FkJsonNode ndata = blding.QueryMember("data");

                if (ndata != null)
                {
                    ClashTypes.Building t = (ClashTypes.Building) ndata.GetInteger();
                    switch (t)
                    {
                        case ClashTypes.Building.Wall:
                            wallCounts[blding.QueryMember("lvl").GetInteger()]++;
                            break;
                        case ClashTypes.Building.ArmyCamp:

                            // units stored as: "units":[[4000006,13],[4000001,6]]

                            FkJsonNode nodeUnits = blding.QueryMember("units");
                            if (nodeUnits != null)
                            {
                                JsonArray units = nodeUnits.GetArray();
                                foreach (FkJsonNode unitInnerNode in units.NodeList)
                                {
                                    JsonArray unitCount = unitInnerNode.GetArray();
                                    troopCounts[unitCount.GetInteger(0) - 4000000].Count += unitCount.GetInteger(1);
                                }
                            }
                            break;
                    }
                }
            }

            for (int x = wallCounts.Length - 1; x >= 0; x--) // Backwards loop, so highest leveled walls are on top :3
            {
                if (wallCounts[x] > 0)
                    VillageStats.InsertWallType(x, wallCounts[x]);
            }
            VillageStats.SetTroopCounts(troopCounts); // Set the troop counts as found above

            //
            // Get some user data (name, id, level, etc):
            //

            KagamiDataCollection user = (KagamiDataCollection) homeData.GetValue("user");
            VillageStats.SetUserInfo((string) user.GetValue("userName"), (int) user.GetValue("level"),
                (long) user.GetValue("userId"));

            KagamiDataCollection clan = (KagamiDataCollection) user.GetValue("clan");
            if ((clan != null) && (clan.InstanceIsExpressed))
                VillageStats.SetClanInfo((string) clan.GetValue("clanName"), (int) clan.GetValue("rank"),
                    (long) clan.GetValue("clanId"));


            //
            // Get user troop levels, so they can be applied to any batches in army camps
            //
            KagamiDataCollection resources = (KagamiDataCollection) homeData.GetValue("resources");
            if (resources.QueryField("unitLevels") != null)
            {
                object[] unitLevels = (object[]) resources.GetValue("unitLevels");
                    // unitLevels array of ResourceComponent
                KagamiVariantType ulVariant = resources.QueryField("unitLevels");
                if (ulVariant.ArrayLength > 0)
                    // Only load levels if there are any upgraded (there won't be on a new account)
                {
                    //KagamiDataCollection[] levelsArray = (KagamiDataCollection[])unitLevels.getArray();
                    for (int x = 0; x < ulVariant.ArrayLength; x++)
                    {
                        int unitType = (int) ((KagamiDataCollection) unitLevels[x]).GetValue("type");
                        int unitLevel = (int) ((KagamiDataCollection) unitLevels[x]).GetValue("value");
                        troopCounts[unitType - 4000000].Level = unitLevel;
                    }
                }
            }

            //
            // Gather alliance/clan-castle troop information, if available
            //
            List<ClashTroopBatch> castleTroops = new List<ClashTroopBatch>();
            if (resources.QueryField("allianceUnits") != null)
            {
                object[] allianceUnits = (object[]) resources.GetValue("allianceUnits");
                // No need to check for length > 0, as for-loop does this already xD. Above troop-level code is weird and does it anyones (no point)
                for (int x = allianceUnits.Length - 1; x >= 0; x--)
                {
                    // The collection in this array is of type UnitComponent : {int typeId, int count, int level)
                    int unitType = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("typeId");
                    int unitCount = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("count");
                    int unitLevel = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("level");
                    if (unitCount > 0)
                    {
                        ClashTroopBatch castleTroopBatch = new ClashTroopBatch
                        {
                            TroopID = unitType,
                            Level = unitLevel,
                            Count = unitCount
                        };
                        castleTroops.Add(castleTroopBatch);
                    }
                }
            }
            VillageStats.SetClanTroops(castleTroops);


            VillageStats.RenderSelectedTroops();
        }

        [Obsolete("... You don't want to do this! D:")]
        private void bak_handleVisitedHomeData(Message m)
        {
            MessageDataWrapper dataWrapper = new MessageDataWrapper();
            dataWrapper.SetMessage(m);

            KagamiDataCollection homeData = KagamiDataHost.GetFromNormalizedName("VisitedHomeData");
            homeData.Reset();
            homeData.ReadMessageData(dataWrapper);

            VillageStats.ClearAll();
            VillageStats.SetAge((int) homeData.GetValue("age"));
            if (homeData.QueryField("shieldTime") != null)
                VillageStats.SetShieldTime((int) homeData.GetValue("shieldTime"));
            else
                VillageStats.SetShieldTime(0);


            /* #### DIFF - "villageData" -> "homeVillage", that's all    */

            //
            // Handle wall and resource calculations:
            //
            string villageData = (string) homeData.GetValue("homeVillage"); // zstream homeVillage

            FkJsonNode villageDataParser = new FkJsonNode();
            villageDataParser.LoadString(villageData);
            villageDataParser = villageDataParser.GetIndexedNode(0); // Get the first object (only) in the Host

            int[] wallCounts = new int[11];
            ClashTroopBatch[] troopCounts = new ClashTroopBatch[21];
            for (int x = 0; x < troopCounts.Length; x++)
            {
                troopCounts[x] = new ClashTroopBatch {TroopID = 4000000 + x};
            }


            List<FkJsonNode> barrackNodes = new List<FkJsonNode>();
            JsonArray buildings = villageDataParser.QueryMember("buildings").GetArray();
            int troopTotalCapacity = 0;
            int builderCount = 0;
            foreach (FkJsonNode blding in buildings.NodeList)
            {
                FkJsonNode ndata = blding.QueryMember("data");

                if (ndata != null)
                {
                    ClashTypes.Building t = (ClashTypes.Building) ndata.GetInteger();
                    switch (t)
                    {
                        case ClashTypes.Building.Wall:
                            wallCounts[blding.QueryMember("lvl").GetInteger()]++;
                            break;
                        case ClashTypes.Building.ArmyCamp:

                            // units stored as: "units":[[4000006,13],[4000001,6]]
                            troopTotalCapacity += ArmyCampCapacity[blding.QueryMember("lvl").GetInteger()];
                            FkJsonNode nodeUnits = blding.QueryMember("units");
                            if (nodeUnits != null)
                            {
                                JsonArray units = nodeUnits.GetArray();
                                foreach (FkJsonNode unitInnerNode in units.NodeList)
                                {
                                    JsonArray unitCount = unitInnerNode.GetArray();
                                    troopCounts[unitCount.GetInteger(0) - 4000000].Count += unitCount.GetInteger(1);
                                }
                            }
                            break;
                        case ClashTypes.Building.Barracks:
                        case ClashTypes.Building.DarkBarracks:
                            barrackNodes.Add(blding);
                            break;
                        case ClashTypes.Building.BuilderHut:
                            builderCount++;
                            break;
                    }

                    // Grab some builder info while we're iterating the buildings :3
                    FkJsonNode node_construction_time = blding.QueryMember("const_t");
                    if (node_construction_time != null)
                    {
                        int constructionTime = node_construction_time.GetInteger();
                        int buildingLevel = -1;
                        FkJsonNode p = null;
                        if ((p = blding.QueryMember("lvl")) != null)
                            // This might* be nulled, if the building is just being constructed.
                            buildingLevel = p.GetInteger();
                        VillageStats.InsertBuilder(constructionTime, buildingLevel, t); // t set just up above
                    }
                }
            }

            for (int x = wallCounts.Length - 1; x >= 0; x--) // Backwards loop, so highest leveled walls are on top :3
            {
                if (wallCounts[x] > 0)
                    VillageStats.InsertWallType(x, wallCounts[x]);
            }
            VillageStats.SetMaxTroops(troopTotalCapacity);
            VillageStats.SetTroopCounts(troopCounts); // Set the troop counts as found above
            VillageStats.SetBuilderCount(builderCount);

            //
            // Get some user data (name, id, level) along with a bit of clan data (name, id, level):
            //

            KagamiDataCollection user = (KagamiDataCollection) homeData.GetValue("user");
            VillageStats.SetUserInfo((string) user.GetValue("userName"), (int) user.GetValue("level"),
                (long) user.GetValue("userId"));
            if (user.GetValue("exp") != null)
                VillageStats.SetUserExp((int) user.GetValue("exp"));

            KagamiDataCollection clan = (KagamiDataCollection) user.GetValue("clan");
            if ((clan != null) && (clan.InstanceIsExpressed))
                VillageStats.SetClanInfo((string) clan.GetValue("clanName"), (int) clan.GetValue("rank"),
                    (long) clan.GetValue("clanId"));


            //
            // Get user troop levels, so they can be applied to any batches in army camps
            //

            // No need to set all levels to 0, as VillageStats.clearAll calls [clearTroopLevels]

            KagamiDataCollection resources = (KagamiDataCollection) homeData.GetValue("resources");
            if (resources.QueryField("unitLevels") != null)
            {
                object[] unitLevels = (object[]) resources.GetValue("unitLevels");
                    // unitLevels array of ResourceComponent
                KagamiVariantType ulVariant = resources.QueryField("unitLevels");
                if (ulVariant.ArrayLength > 0)
                    // Only load levels if there are any upgraded (there won't be on a new account)
                {
                    //KagamiDataCollection[] levelsArray = (KagamiDataCollection[])unitLevels.getArray();
                    for (int x = 0; x < ulVariant.ArrayLength; x++)
                    {
                        int unitType = (int) ((KagamiDataCollection) unitLevels[x]).GetValue("type");
                        int unitLevel = (int) ((KagamiDataCollection) unitLevels[x]).GetValue("value");
                        troopCounts[unitType - 4000000].Level = unitLevel;
                        VillageStats.SetTroopLevel((ClashTypes.Troop) unitType, unitLevel);
                    }
                }
            }

            //
            // Gather alliance/clan-castle troop information, if available
            //
            List<ClashTroopBatch> castleTroops = new List<ClashTroopBatch>();
            if (resources.QueryField("allianceUnits") != null)
            {
                object[] allianceUnits = (object[]) resources.GetValue("allianceUnits");
                // No need to check for length > 0, as for-loop does this already xD. Above troop-level code is weird and does it anyones (no point)
                for (int x = allianceUnits.Length - 1; x >= 0; x--)
                {
                    // The collection in this array is of type UnitComponent : {int typeId, int count, int level)
                    int unitType = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("typeId");
                    int unitCount = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("count");
                    int unitLevel = (int) ((KagamiDataCollection) allianceUnits[x]).GetValue("level");
                    if (unitCount > 0)
                    {
                        ClashTroopBatch castleTroopBatch = new ClashTroopBatch
                        {
                            TroopID = unitType,
                            Level = unitLevel,
                            Count = unitCount
                        };
                        castleTroops.Add(castleTroopBatch);
                    }
                }
            }
            VillageStats.SetClanTroops(castleTroops);


            //
            // Handle barrack nodes stored earlier
            //
            foreach (FkJsonNode bNode in barrackNodes)
            {
                bool isDarkBarrackNode = (ClashTypes.Building) bNode.QueryMember("data").GetInteger() ==
                                         ClashTypes.Building.DarkBarracks;

                FkJsonNode unit_prod_node = bNode.QueryMember("unit_prod");
                if (unit_prod_node != null)
                {
                    FkJsonNode timeRemainingNode = unit_prod_node.QueryMember("t");
                    FkJsonNode slots_node = unit_prod_node.QueryMember("slots");
                    if ((slots_node != null) && (timeRemainingNode != null))
                    {
                        int barrackTimeRemaining = timeRemainingNode.GetInteger();
                        JsonArray trainingSlots = slots_node.GetArray();
                        if (trainingSlots.ArrayLength > 0) // Only create the TroopBatch if arrayLen is > 0
                        {
                            ClashTroopBatch[] barrackTroops = new ClashTroopBatch[trainingSlots.ArrayLength];
                            int btIndex = 0;
                            foreach (FkJsonNode unitNode in trainingSlots.NodeList)
                            {
                                barrackTroops[btIndex] = new ClashTroopBatch
                                {
                                    TroopID = unitNode.QueryMember("id").GetInteger(),
                                    Count = unitNode.QueryMember("cnt").GetInteger()
                                };
                                btIndex++;
                            }

                            VillageStats.InsertBarracksInfo(barrackTroops, barrackTimeRemaining, isDarkBarrackNode);
                        }
                    }
                }
            }


            //
            // Grab cooldown/spawn info
            //
            FkJsonNode nodeRespawnVars = villageDataParser.QueryMember("respawnVars");
            if (nodeRespawnVars != null)
            {
                // Assert that these keys are defined, as the respawnVars were included with the data...
                int secondsFromLastRespawn = nodeRespawnVars.QueryMember("secondsFromLastRespawn").GetInteger();
                int obstacleClearCounter = nodeRespawnVars.QueryMember("obstacleClearCounter").GetInteger();
                int time_to_gembox_drop = nodeRespawnVars.QueryMember("time_to_gembox_drop").GetInteger();
                VillageStats.SetSpawnInfo(time_to_gembox_drop, secondsFromLastRespawn, obstacleClearCounter);
            }
            else
            {
                VillageStats.SetSpawnInfo(0, 0, 0);
            }


            VillageStats.RenderSelectedTroops();
            VillageStats.RenderInfoPlusStatic();

            LoggingProvider.EventLog.LogBasicEvent(DateTime.Now.ToLongTimeString() + ": Home data loaded for '"
                                                   + (string) user.GetValue("userName") + "'");
        }
    }
}