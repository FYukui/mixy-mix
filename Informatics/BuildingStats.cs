﻿using System.Collections.Generic;
using YukiNoKagami.ServerData;

namespace YukiNoKagami.Informatics
{
    /// <summary>
    ///     Represents a building's stats at a certain, provided level.
    /// </summary>
    internal class BuildingStatEntry
    {
        public float AttackSpeed;
        public int Cost;
        public int DPS;
        public int HP;
        public int Level;
        public int Range;
        public int THRequiredLevel;
    }

    /// <summary>
    ///     Contains an accessible set of levels (<see cref="LevelData"/>) for the given building, 
    ///     with each containing critical stats for that level.
    /// </summary>
    internal class BuildingEntry
    {
        public ClashTypes.Resource CostUnit;
        public ClashTypes.Building ID;
        public string Name;

        public BuildingEntry(string name, ClashTypes.Building id, int levelCount, ClashTypes.Resource costUnit)
        {
            LevelData = new BuildingStatEntry[levelCount];
            CostUnit = costUnit;
            Name = name;
            ID = id;
        }

        public int MaxLevel => LevelData.Length;
        public BuildingStatEntry[] LevelData { get; }

        public void AddLevel(int level, int cost, int hp, int thLevel, int range = 1, int dps = 0,
            float attackSpeed = 1.0f)
        {
            BuildingStatEntry statEntry = new BuildingStatEntry
            {
                Level = level,
                Cost = cost,
                HP = hp,
                THRequiredLevel = thLevel,
                Range = range,
                DPS = dps,
                AttackSpeed = attackSpeed
            };

            LevelData[level] = statEntry;
        }
    }

    /// <summary>
    ///     Provides convenient lookup and definition support for Clash building types.
    /// </summary>
    internal static class BuildingStats
    {
        public static Dictionary<ClashTypes.Building, BuildingEntry> DefinedBuildings;

        static BuildingStats()
        {
            DefinedBuildings = new Dictionary<ClashTypes.Building, BuildingEntry>();
        }

        public static BuildingEntry CreateBuilding(string buildingName, ClashTypes.Building id, int levelCount,
            ClashTypes.Resource costUnit)
        {
            BuildingEntry building = new BuildingEntry(buildingName, id, levelCount, costUnit);
            DefinedBuildings.Add(id, building);
            return building;
        }

        public static BuildingEntry GetBuilding(ClashTypes.Building id)
        {
            BuildingEntry outEntry = null;
            DefinedBuildings.TryGetValue(id, out outEntry);
            return outEntry;
        }
    }
}