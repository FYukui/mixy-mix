﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using YukiNoKagami.ServerData;

// This file is a huge mess of bad-practices.
// TODO: No more static VillageStats; group fields into serializable sub-objects !

namespace YukiNoKagami.Informatics
{
    public class ClashTroopBatch
    {
        public int Count;
        public int Level;
        public int TroopID;
    }

    public class BarracksManager
    {
        private readonly List<BarracksTroop> _trainingList;
        private readonly ClashTroopBatch[] _troopCountsReference;
        private readonly int[] _troopLevels;
        private readonly int _troopTotalCapacity;
        private int _curTick;
        private bool _exceedsTroopCapacity;
        public bool IsDarkBarracks;
        public bool ListViewReflectsExceedingCapacity;
        public bool ShouldRefreshTroopCounts;

        public BarracksManager(ClashTroopBatch[] trainingTroops, int time, int age, int[] levels,
            ClashTroopBatch[] troopCounts, int troopCapacity,
            bool isDarkBarracks = false)
        {
            _troopLevels = levels;
            _troopCountsReference = troopCounts;
            _troopTotalCapacity = troopCapacity;
            IsDarkBarracks = isDarkBarracks;

            _trainingList = new List<BarracksTroop>();
            _curTick = age;

            int tickCountTroops = 0;
            foreach (ClashTroopBatch batch in trainingTroops)
            {
                TroopEntry def = TroopStats.GetTroop((ClashTypes.Troop) batch.TroopID);
                int trainingTime = def.TrainingTime;
                // Add a new barracksTroop for each count of the troop being trained.
                for (int y = 0; y < batch.Count; y++)
                {
                    tickCountTroops += trainingTime;
                    BarracksTroop n = new BarracksTroop
                    {
                        TroopID = batch.TroopID,
                        TickRequired = tickCountTroops
                    };
                    _trainingList.Add(n);
                }
            }

            // Time represents remaining troop total training time. So look at this example:
            // Training 1 barbarian (total traintime = 20 sec). After 7 seconds, t will be (20 -7) = 13.
            // To find the passed time here (7), simply subtract the new remaining time from the total: 20 - 13 = 7
            //
            // This is added to curTick, as curTick stores the number of seconds passed training troops for this barracks.
            //curTick = curTick + (tickCountTroops - time)

            if (_trainingList.Count > 0)
            {
                _curTick = _curTick + Math.Max(_trainingList[0].TickRequired - time, 0);
            }
        }

        private void HandleFinishedTroops(List<BarracksTroop> finishedTroops, int lastTrainingListIndex)
        {
            if (finishedTroops.Count > 0)
            {
                ShouldRefreshTroopCounts = true;
                foreach (BarracksTroop ft in finishedTroops)
                {
                    TroopEntry stats = TroopStats.GetTroop((ClashTypes.Troop) ft.TroopID);
                    if (stats.HousingSpace + VillageStats.NumTroops <= _troopTotalCapacity)
                    {
                        _troopCountsReference[ft.TroopID - 4000000].Count += 1;
                        VillageStats.NumTroops += stats.HousingSpace;
                    }
                    else
                    {
                        //if(trainingList.Count > lastTrainingListIndex)  // Check if there are more troops waiting behind this stopped one.
                        //{                                               // If so, we adjust ticks, so the other ones do not advance.

                        //}
                        _trainingList.Insert(0, ft);
                        _curTick = ft.TickRequired;
                        _exceedsTroopCapacity = true;
                        ShouldRefreshTroopCounts = false;
                        return;
                    }
                }

                for (int x = 0; x < _troopCountsReference.Length; x++)
                    _troopCountsReference[x].Level = _troopLevels[x];
            }
            else
            {
                ShouldRefreshTroopCounts = false;
            }
        }

        private void AdvanceTroopTraining()
        {
            List<BarracksTroop> finishedTroops = new List<BarracksTroop>();
            int lastTrainingListIndex = 0;
            for (int x = _trainingList.Count - 1; x >= 0; x--)
            {
                if (_trainingList[x].TickRequired <= _curTick)
                {
                    finishedTroops.Add(_trainingList[x]);
                    _trainingList.RemoveAt(x);
                    lastTrainingListIndex = x;
                    break;
                    // ^ Fix/update-ish thing. Only add one troop per tick (sec), so barracks remain somewhat in-order
                    // Otherwise, one barracks may fill up the entire batch
                    // TODO: Separate all this crap, and expose barrack methods, allowing this to be processed more properly.
                }
            }
            HandleFinishedTroops(finishedTroops, lastTrainingListIndex);
        }

        public void Tick()
        {
            if (_exceedsTroopCapacity)
                return;
            _curTick += 1;
            AdvanceTroopTraining();
        }

        private void InsertTroopEntryToListItems(int groupId, int groupCount, BarracksTroop firstGroupItem, bool hasShownExceedingCapacity,
            ListView.ListViewItemCollection items)
        {
            int level = _troopLevels[groupId - 4000000];
            TroopEntry t = TroopStats.GetTroop((ClashTypes.Troop) groupId);
            TroopStatEntry stats = t.LevelData[level];

            ListViewItem ii = items.Add((level + 1).ToString()); // Level
            ii.ImageIndex = stats.CharImageIndex;
            ii.SubItems.Add(t.Name); // Name
            ii.SubItems.Add(groupCount.ToString("N0")); // Count
            ii.SubItems.Add((t.HousingSpace*groupCount).ToString("N0")); // Space
            ii.SubItems.Add(stats.HP.ToString("N0")); // HP
            ii.SubItems.Add((stats.HP*groupCount).ToString("N0")); // Total HP
            ii.SubItems.Add(stats.DPS.ToString("N0")); // DPS
            ii.SubItems.Add((stats.DPS*groupCount).ToString("N0")); // Total DPS
            ii.SubItems.Add(stats.Cost.ToString("N0")); // Cost
            ii.SubItems.Add((stats.Cost*groupCount).ToString("N0")); // Total Cost

            if (_exceedsTroopCapacity && !hasShownExceedingCapacity)
            {
                ii.SubItems.Add("!!!");
            }
            else
                ii.SubItems.Add(
                    TimeSpan.FromSeconds(Math.Max(firstGroupItem.TickRequired - _curTick, 1)).ToString(@"h\:mm\:ss"));
        }

        public void WriteToListView(ListView.ListViewItemCollection items)
        {
            if (ListViewReflectsExceedingCapacity)
                return;

            items.Clear();
            if (_trainingList.Count < 1)
                return;


            int currentGroupID = _trainingList[0].TroopID;
            int currentGroupCount = 0;
            BarracksTroop firstGroupItem = _trainingList[0];
            bool hasShownExceedingCapacity = false;
            foreach (BarracksTroop t in _trainingList)
            {
                if (t.TroopID != currentGroupID)
                {
                    InsertTroopEntryToListItems(currentGroupID, currentGroupCount, firstGroupItem,
                        hasShownExceedingCapacity, items);

                    if (_exceedsTroopCapacity && !hasShownExceedingCapacity)
                        hasShownExceedingCapacity = true;

                    currentGroupCount = 1; // Yes, 1, NOT 0
                    currentGroupID = t.TroopID;
                    firstGroupItem = t;
                }
                else
                {
                    currentGroupCount++;
                }
            }

            // Add the last item:
            InsertTroopEntryToListItems(currentGroupID, currentGroupCount, firstGroupItem, hasShownExceedingCapacity,
                items);

            if (hasShownExceedingCapacity)
                ListViewReflectsExceedingCapacity = true;
        }

        private class BarracksTroop
        {
            public int TickRequired;
            public int TroopID;
        }
    }

    internal class BuilderInfo
    {
        public int BuildingCurrentLevel;
        public string BuildingName;
        public ClashTypes.Building BuildingType;
        public int SecondsRemaining;

        public void InsertToListView(ListView.ListViewItemCollection items)
        {
            ListViewItem ii = items.Add(((int) BuildingType).ToString());
            ii.SubItems.Add(BuildingName);
            ii.SubItems.Add((BuildingCurrentLevel + 1).ToString());
            ii.SubItems.Add(TimeSpan.FromSeconds(Math.Max(SecondsRemaining, 1)).ToString(@"d\:hh\:mm\:ss"));
        }
    }

    internal static class VillageStats
    {
        public static FormMain MainForm;
        public static bool IsShowingClanTroops = false; // Home troops are shown by default
        public static bool IsShowingInfoPlus = false;

        private static ClashTroopBatch[] _troopCounts;
        private static List<ClashTroopBatch> _clanTroops;

        private static readonly List<BarracksManager> BarracksInfo = new List<BarracksManager>();
        private static readonly List<BuilderInfo> Builders = new List<BuilderInfo>();
        private static int[] _troopLevels = new int[22];

        public static int NumTroops;
        private static int _dataAge;
        private static int _maxTroopCount;
        private static int _totalBuilderCount;

        private static int _obstacleSpawnTime;
        private static int _obstacleClearIndex;
        private static int _gemboxSpawnTime;
        private static int _shieldTime;

        private static int _userEXP;

        private static int _userTicks;
            // ticks since last clearUserInfo    (pretty much ticks spent viewing/visiting user)


        private static int _listRenderBarracksIndex; // Index of the currently selected infoPlus barracks
        private static InfoPlusListViewRenderType _listRenderMode;


        private static readonly int[] ObstacleGemRewards = {6, 0, 4, 2, 0, 3, 2, 0, 0, 5, 1, 0, 3, 4, 0, 0, 5, 0, 1, 0};

        private static string GetNextGemRewardOrder()
        {
            string res = "";
            // Display first 5 (with commas following)
            int clearIndex = _obstacleClearIndex;
            for (int x = 0; x < 5; x++)
            {
                res += ObstacleGemRewards[clearIndex] + ",";
                clearIndex++;
                if (clearIndex > ObstacleGemRewards.Length - 1)
                    clearIndex = 0;
            }
            res += ObstacleGemRewards[clearIndex];
            return res;
        }

        public static void SetShieldTime(int shieldTime)
        {
            _shieldTime = shieldTime - _dataAge;
            if (_shieldTime < 0)
                _shieldTime = 0;
        }

        public static void SetUserExp(int exp)
        {
            _userEXP = exp;
        }

        public static void SetSpawnInfo(int gemboxTime, int obstacleLastRespawnSeconds, int obstacleClearCounter)
        {
            _gemboxSpawnTime = gemboxTime - _dataAge;
            _obstacleSpawnTime = 28800 - obstacleLastRespawnSeconds - _dataAge;
                // Remaining time = SpawnTime(8h = 28,800s) - Elapsed
            _obstacleClearIndex = obstacleClearCounter;
            if (_gemboxSpawnTime < 0)
                _gemboxSpawnTime = 0;
            if (_obstacleClearIndex < 0)
                _obstacleClearIndex = 28800 + (_obstacleClearIndex%28800);
                    // Wrap onto next spawn time, if a new object has already spawned
        }

        public static void SetTroopLevel(ClashTypes.Troop troopId, int level)
        {
            _troopLevels[(int) troopId - 4000000] = level;
        }

        public static void SetAge(int packetAge)
        {
            _dataAge = packetAge;
        }

        public static void SetMaxTroops(int maxTroopCount)
        {
            _maxTroopCount = maxTroopCount;
        }

        public static void SetBuilderCount(int builderCount)
        {
            _totalBuilderCount = builderCount;
        }

        // Inserts an active/working (as in a builder that's constructing/upgrading something) builder record.
        public static void InsertBuilder(int buildingTimeRemaining, int currentLevel, ClashTypes.Building buildingType)
        {
            BuilderInfo b = new BuilderInfo {SecondsRemaining = buildingTimeRemaining - _dataAge};
            if (b.SecondsRemaining > 0)
            {
                b.BuildingCurrentLevel = currentLevel;
                b.BuildingType = buildingType;
                b.BuildingName = ClashNames.Buildings[buildingType - ClashTypes.Building.ArmyCamp];
                Builders.Add(b);
            }
        }

        private static int GetBarracksIndexFromNaturalString(string naturalDescription)
        {
            int barracksNaturalNum = 1;
            int darkBarracksNaturalNum = 1;
            int i = 0;
            foreach (BarracksManager b in BarracksInfo)
            {
                if (!b.IsDarkBarracks)
                {
                    if (("Barracks #" + barracksNaturalNum).Equals(naturalDescription))
                        return i;
                    barracksNaturalNum++;
                }
                i++;
            }
            i = 0;
            foreach (BarracksManager b in BarracksInfo)
            {
                if (b.IsDarkBarracks)
                {
                    if (("Dark Barracks #" + darkBarracksNaturalNum).Equals(naturalDescription))
                        return i;
                    darkBarracksNaturalNum++;
                }
                i++;
            }

            return 0;
        }

        // Only to be used when first rendering (this sets up a bunch of stuff)
        // All others should be called using [renderInfoPlusTick]
        public static void RenderInfoPlusStatic()
        {
            if (MainForm.lblBoxSpawn.InvokeRequired)
                MainForm.lblBoxSpawn.Invoke(new DlVoid(RenderInfoPlusStatic));
            else
            {
                MainForm.lblObjectSpawn.Text = TimeSpan.FromSeconds(_obstacleSpawnTime).ToString(@"h\:mm\:ss");
                MainForm.lblLastActivity.Text = TimeSpan.FromSeconds(_dataAge + _userTicks).ToString(@"d\:hh\:mm\:ss");

                MainForm.lblBoxSpawn.Text = TimeSpan.FromSeconds(_gemboxSpawnTime).ToString(@"d\:hh\:mm\:ss");
                MainForm.lblBuilders.Text = (_totalBuilderCount - Builders.Count) + "/" + _totalBuilderCount;

                MainForm.lblShieldTime.Text = TimeSpan.FromSeconds(_shieldTime).ToString(@"d\:hh\:mm\:ss");
                MainForm.lblExp.Text = _userEXP.ToString("N0");

                MainForm.lblGemOrder.Text = GetNextGemRewardOrder();
                //XCSHL text = null;


                // Handle type selector values
                string selItemText = "";
                if (MainForm.selDisplayType.SelectedItem != null)
                    selItemText = MainForm.selDisplayType.SelectedItem.ToString();

                MainForm.selDisplayType.Items.Clear();
                MainForm.selDisplayType.Items.Add("Loot Info");
                MainForm.selDisplayType.Items.Add("Laboratory Info");
                MainForm.selDisplayType.Items.Add("Builder Info");
                int barracksNaturalNum = 1;
                int darkBarracksNaturalNum = 1;
                foreach (BarracksManager b in BarracksInfo)
                    if (!b.IsDarkBarracks)
                    {
                        MainForm.selDisplayType.Items.Add("Barracks #" + barracksNaturalNum);
                        barracksNaturalNum++;
                    }
                foreach (BarracksManager b in BarracksInfo)
                    if (b.IsDarkBarracks)
                    {
                        MainForm.selDisplayType.Items.Add("Dark Barracks #" + darkBarracksNaturalNum);
                        darkBarracksNaturalNum++;
                    }

                //MainForm.selDisplayType.Items.Add("Spell Factory");
                //MainForm.selDisplayType.Items.Add("Dark Spell Factory");


                bool itemFound = false;
                for (int xi = 0; xi < MainForm.selDisplayType.Items.Count; xi++)
                    if (MainForm.selDisplayType.Items[xi].ToString().Equals(selItemText))
                    {
                        MainForm.selDisplayType.SelectedIndex = xi;
                        itemFound = true;
                        break; // Exit the loop
                    }
                if (!itemFound)
                    MainForm.selDisplayType.SelectedIndex = 0; // Select just "Loot Info" by default.


                InfoPlusListViewConfigureType();
                //renderInfoPlusListView();         // [infoPlusListViewConfigureType] will call this already
            }
        }


        // NO NEED FOR INVOKE, AS ANY CALLER WILL HAVE ALREADY CALLED IT !
        public static void InfoPlusListViewConfigureType()
        {
            MainForm.listInfoPlus.BeginUpdate();
            MainForm.listInfoPlus.Clear();


            switch (MainForm.selDisplayType.SelectedItem.ToString())
            {
                case "Loot Info":
                    MainForm.listInfoPlus.Columns.Add("Level").Width = 70; // 1 .. 11
                    MainForm.listInfoPlus.Columns.Add("Type").Width = 120;
                        // Gold/Elix/DE Storage, Gold Mine, Elixir Pump, Dark Elixir Drill, etc.
                    MainForm.listInfoPlus.Columns.Add("Amount").Width = 120;
                    MainForm.listInfoPlus.Columns.Add("Lootable Amount").Width = 120;

                    _listRenderMode = InfoPlusListViewRenderType.LootInfo;
                    break;
                case "Builder Info":
                    MainForm.listInfoPlus.Columns.Add("Building ID").Width = 90;
                    MainForm.listInfoPlus.Columns.Add("Building Name").Width = 140;
                    MainForm.listInfoPlus.Columns.Add("Cur. Level").Width = 80;
                    MainForm.listInfoPlus.Columns.Add("Time Remaining").Width = 140;


                    _listRenderMode = InfoPlusListViewRenderType.BuilderInfo;
                    break;
                case "Laboratory Info":
                    MainForm.listInfoPlus.Columns.Add("Item ID").Width = 90;
                        // Use Item, as it can be either a spell or a troop
                    MainForm.listInfoPlus.Columns.Add("Item Name").Width = 90;
                    MainForm.listInfoPlus.Columns.Add("Time Remaining").Width = 140;

                    _listRenderMode = InfoPlusListViewRenderType.LaboratoryInfo;
                    break;
                default:
                    // The type must be of (Dark) Barracks #X, as it's not one of the above ("Loot Info" or "Builder Info")


                    MainForm.listInfoPlus.Columns.Add("Level").Width = 70;
                    MainForm.listInfoPlus.Columns.Add("Name").Width = 100;
                    MainForm.listInfoPlus.Columns.Add("Count");
                    MainForm.listInfoPlus.Columns.Add("Space");
                    MainForm.listInfoPlus.Columns.Add("HP");
                    MainForm.listInfoPlus.Columns.Add("Total HP").Width = 80;
                    MainForm.listInfoPlus.Columns.Add("DPS");
                    MainForm.listInfoPlus.Columns.Add("Total DPS").Width = 80;
                    MainForm.listInfoPlus.Columns.Add("Cost");
                    MainForm.listInfoPlus.Columns.Add("Total Cost").Width = 80;
                    MainForm.listInfoPlus.Columns.Add("Time Remaining").Width = 110;
                    MainForm.listInfoPlus.SmallImageList = MainForm.imgListTroops;

                    _listRenderBarracksIndex =
                        GetBarracksIndexFromNaturalString(MainForm.selDisplayType.SelectedItem.ToString());
                    // Set this to false, so an exceeding-capacity listView will still render the first time
                    BarracksInfo[_listRenderBarracksIndex].ListViewReflectsExceedingCapacity = false;
                    _listRenderMode = InfoPlusListViewRenderType.Barracks;
                    break;
            }


            RenderInfoPlusListView();
            MainForm.listInfoPlus.EndUpdate();
        }

        // NO NEED FOR INVOKE, AS ANY CALLER WILL HAVE ALREADY CALLED IT !
        private static void RenderInfoPlusListView()
        {
            //MainForm.listInfoPlus.BeginUpdate();

            switch (_listRenderMode)
            {
                case InfoPlusListViewRenderType.Barracks:
                    BarracksManager m = BarracksInfo[_listRenderBarracksIndex];
                    if (!m.ListViewReflectsExceedingCapacity)
                    {
                        MainForm.listInfoPlus.BeginUpdate();
                        m.WriteToListView(MainForm.listInfoPlus.Items);
                        MainForm.listInfoPlus.EndUpdate();
                    }
                    break;
                case InfoPlusListViewRenderType.BuilderInfo:
                    MainForm.listInfoPlus.BeginUpdate();
                    MainForm.listInfoPlus.Items.Clear();
                    foreach (BuilderInfo b in Builders)
                        b.InsertToListView(MainForm.listInfoPlus.Items);
                    MainForm.listInfoPlus.EndUpdate();
                    break;
            }

            //MainForm.listInfoPlus.EndUpdate();
        }

        public static void RenderInfoPlusTick()
        {
            if (IsShowingInfoPlus)
            {
                if (MainForm.lblBoxSpawn.InvokeRequired)
                    MainForm.lblBoxSpawn.Invoke(new DlVoid(RenderInfoPlusStatic));
                else
                {
                    MainForm.lblObjectSpawn.Text = TimeSpan.FromSeconds(_obstacleSpawnTime).ToString(@"h\:mm\:ss");
                    MainForm.lblLastActivity.Text =
                        TimeSpan.FromSeconds(_dataAge + _userTicks).ToString(@"d\:hh\:mm\:ss");

                    MainForm.lblBoxSpawn.Text = TimeSpan.FromSeconds(_gemboxSpawnTime).ToString(@"d\:hh\:mm\:ss");
                    MainForm.lblBuilders.Text = (_totalBuilderCount - Builders.Count) + "/" + _totalBuilderCount;

                    MainForm.lblShieldTime.Text = TimeSpan.FromSeconds(_shieldTime).ToString(@"d\:hh\:mm\:ss");
                    //MainForm.lblExp.Text = _userEXP.ToString("N0");

                    //MainForm.lblGemOrder.Text = getNextGemRewardOrder();
                    //XCSHL text = null;

                    RenderInfoPlusListView();
                }
            }
        }

        public static void InsertBarracksInfo(ClashTroopBatch[] trainingTroops, int time, bool isDarkBarracks = false)
        {
            BarracksManager m = new BarracksManager(trainingTroops, time, _dataAge, _troopLevels, _troopCounts,
                _maxTroopCount, isDarkBarracks);
            BarracksInfo.Add(m);
        }

        public static void TimerTick()
        {
            _userTicks++;

            bool troopCountsNeedUpdate = false;
            // No need to null-check barracksInfo. It will never be null.
            foreach (BarracksManager m in BarracksInfo.ToList())
            {
                m.Tick();
                if (m.ShouldRefreshTroopCounts)
                    troopCountsNeedUpdate = true;
            }

            // Display new troop counts from training, if needed (and only if clan troops aren't showing, of course)
            if (troopCountsNeedUpdate && !IsShowingClanTroops)
                RenderSelectedTroops();


            // Handle spawn timers:
            if (_gemboxSpawnTime > 0)
                _gemboxSpawnTime--;
            if (_obstacleSpawnTime > 0)
                _obstacleSpawnTime--;
            else
            {
                _obstacleSpawnTime = 28800; // 28,800 seconds in 8 hours, which is the natural spawn time.
            }
            if (_shieldTime > 0)
                _shieldTime--;

            //Handle builder timing:
            for (int x = Builders.Count - 1; x >= 0; x--)
            {
                Builders[x].SecondsRemaining--;
                if (Builders[x].SecondsRemaining <= 0)
                    Builders.RemoveAt(x);
            }

            RenderInfoPlusTick();
        }

        public static void InsertWallType(int level, int count)
        {
            if (MainForm.listViewWalls.InvokeRequired)
                MainForm.listViewWalls.Invoke(new DlIntInt(InsertWallType), level, count);
            else
            {
                ListViewItem n = MainForm.listViewWalls.Items.Add((level + 1).ToString());
                n.SubItems.Add(count.ToString());
                BuildingStatEntry wallStats = BuildingStats.GetBuilding(ClashTypes.Building.Wall).LevelData[level];
                n.SubItems.Add(wallStats.HP.ToString("N0"));
                n.SubItems.Add((wallStats.HP*count).ToString("N0"));
                n.ImageIndex = level;
            }
        }

        public static void SetUserInfo(string name, int level, long id)
        {
            if (MainForm.lblUserName.InvokeRequired)
                MainForm.lblUserName.Invoke(new DlStrIntLong(SetUserInfo), name, level, id);
            else
            {
                MainForm.lblUserName.Text = name;
                MainForm.lblUserLevel.Text = level.ToString();
                MainForm.lblUserID.Text = id.ToString();
            }
        }

        public static void SetClanInfo(string name, int level, long id)
        {
            if (MainForm.lblClanName.InvokeRequired)
                MainForm.lblClanName.Invoke(new DlStrIntLong(SetClanInfo), name, level, id);
            else
            {
                if (name == null)
                {
                    MainForm.lblClanName.Text = "(none)";
                    MainForm.lblClanLevel.Text = "N";
                    MainForm.lblClanID.Text = "0";
                }
                else
                {
                    MainForm.lblClanName.Text = name;
                    MainForm.lblClanLevel.Text = ToRomanNumeral(level);
                    MainForm.lblClanID.Text = id.ToString();
                }
            }
        }

        public static void SetTroopCounts(ClashTroopBatch[] troopCounts)
        {
            NumTroops = 0;
            _troopCounts = troopCounts;
            foreach (ClashTroopBatch batch in troopCounts)
            {
                if (batch.Count > 0)
                {
                    TroopEntry stats = TroopStats.GetTroop((ClashTypes.Troop) batch.TroopID);
                    NumTroops += stats.HousingSpace*batch.Count;
                }
            }
        }

        public static void SetClanTroops(List<ClashTroopBatch> clanTroops)
        {
            _clanTroops = clanTroops;
        }

        public static void RenderSelectedTroops()
        {
            if (MainForm.listViewTroops.InvokeRequired)
                MainForm.listViewTroops.Invoke(new DlVoid(RenderSelectedTroops));
            else
            {
                MainForm.listViewTroops.BeginUpdate();
                int totalDECost = 0;
                int totalEXCost = 0;
                int totalDPS = 0;
                int totalHP = 0;
                int totalCount = 0;
                int totalSpace = 0;
                MainForm.listViewTroops.Items.Clear();
                if (IsShowingClanTroops)
                {
                    if (_clanTroops != null)
                    {
                        foreach (ClashTroopBatch troopBatch in _clanTroops)
                        {
                            TroopEntry troopEntry = TroopStats.GetTroop((ClashTypes.Troop) troopBatch.TroopID);
                            TroopStatEntry stats = troopEntry.LevelData[troopBatch.Level];

                            ListViewItem listItem = MainForm.listViewTroops.Items.Add((stats.Level + 1).ToString());
                            listItem.ImageIndex = stats.CharImageIndex;
                            listItem.SubItems.Add(troopEntry.Name);
                            listItem.SubItems.Add(troopBatch.Count.ToString("N0"));
                            listItem.SubItems.Add((troopEntry.HousingSpace*troopBatch.Count).ToString("N0"));
                            listItem.SubItems.Add(stats.HP.ToString("N0"));
                            listItem.SubItems.Add((stats.HP*troopBatch.Count).ToString("N0"));
                            listItem.SubItems.Add(stats.DPS.ToString("N0"));
                            listItem.SubItems.Add((stats.DPS*troopBatch.Count).ToString("N0"));

                            string costUnitSuffix = null;
                            if (troopEntry.CostUnit == ClashTypes.Resource.DarkElixir)
                            {
                                totalDECost += stats.Cost*troopBatch.Count;
                                costUnitSuffix = " DE";
                            }
                            else
                            {
                                totalEXCost += stats.Cost*troopBatch.Count;
                                costUnitSuffix = " EX";
                            }

                            listItem.SubItems.Add(stats.Cost.ToString("N0") + costUnitSuffix);
                            listItem.SubItems.Add((stats.Cost*troopBatch.Count).ToString("N0") + costUnitSuffix);

                            totalDPS += stats.DPS*troopBatch.Count;
                            totalHP += stats.HP*troopBatch.Count;
                            totalCount += troopBatch.Count;
                            totalSpace += troopEntry.HousingSpace*troopBatch.Count;
                        }
                    }
                }
                else
                {
                    if (_troopCounts != null)
                    {
                        for (int x = _troopCounts.Length - 1; x >= 0; x--)
                        {
                            if (_troopCounts[x].Count > 0)
                            {
                                TroopEntry troopEntry = TroopStats.GetTroop((ClashTypes.Troop) _troopCounts[x].TroopID);
                                TroopStatEntry stats = troopEntry.LevelData[_troopCounts[x].Level];

                                ListViewItem listItem = MainForm.listViewTroops.Items.Add((stats.Level + 1).ToString());
                                listItem.ImageIndex = stats.CharImageIndex;
                                listItem.SubItems.Add(troopEntry.Name);
                                listItem.SubItems.Add(_troopCounts[x].Count.ToString("N0"));
                                listItem.SubItems.Add((troopEntry.HousingSpace*_troopCounts[x].Count).ToString("N0"));
                                listItem.SubItems.Add(stats.HP.ToString("N0"));
                                listItem.SubItems.Add((stats.HP*_troopCounts[x].Count).ToString("N0"));
                                listItem.SubItems.Add(stats.DPS.ToString("N0"));
                                listItem.SubItems.Add((stats.DPS*_troopCounts[x].Count).ToString("N0"));

                                string costUnitSuffix = null;
                                if (troopEntry.CostUnit == ClashTypes.Resource.DarkElixir)
                                {
                                    totalDECost += stats.Cost*_troopCounts[x].Count;
                                    costUnitSuffix = " DE";
                                }
                                else
                                {
                                    totalEXCost += stats.Cost*_troopCounts[x].Count;
                                    costUnitSuffix = " EX";
                                }

                                listItem.SubItems.Add(stats.Cost.ToString("N0") + costUnitSuffix);
                                listItem.SubItems.Add((stats.Cost*_troopCounts[x].Count).ToString("N0") + costUnitSuffix);

                                totalDPS += stats.DPS*_troopCounts[x].Count;
                                totalHP += stats.HP*_troopCounts[x].Count;
                                totalCount += _troopCounts[x].Count;
                                totalSpace += troopEntry.HousingSpace*_troopCounts[x].Count;
                            }
                        }
                    }
                }


                if (totalCount > 0)
                {
                    ListViewItem li = MainForm.listViewTroops.Items.Add("");
                    li.SubItems.Add("Total Stats:");
                    li.SubItems.Add(totalCount.ToString("N0"));
                    li.SubItems.Add(totalSpace.ToString("N0"));
                    li.SubItems.Add("");
                    li.SubItems.Add(totalHP.ToString("N0"));
                    li.SubItems.Add("");
                    li.SubItems.Add(totalDPS.ToString("N0"));
                    li.SubItems.Add("");
                    string costStr = "";
                    if (totalEXCost > 0)
                    {
                        costStr += totalEXCost.ToString("N0") + " EX";
                        if (totalDECost > 0)
                            costStr += ", ";
                    }
                    if (totalDECost > 0)
                        costStr += totalDECost.ToString("N0") + " DE";
                    li.SubItems.Add(costStr);
                }
                MainForm.listViewTroops.EndUpdate();
            }
        }

        public static void ClearAll()
        {
            BarracksInfo.Clear();
            _troopCounts = new ClashTroopBatch[22];
            for (int x = 0; x < _troopCounts.Length; x++)
            {
                _troopCounts[x] = new ClashTroopBatch {TroopID = 4000000 + x};
            }
            _clanTroops = new List<ClashTroopBatch>();
            ClearTroops();
            ClearWalls();
            ClearUserInfo();
            ClearTroopLevels();
            ClearBuilderInfo();
        }

        public static void ClearBuilderInfo()
        {
            Builders.Clear();
        }

        public static void ClearTroopLevels()
        {
            _troopLevels = new int[22];
        }

        public static void ClearTroops()
        {
            NumTroops = 0;
            if (MainForm.listViewTroops.InvokeRequired)
                MainForm.listViewTroops.Invoke(new DlVoid(ClearTroops));
            else
                MainForm.listViewTroops.Items.Clear();
        }

        public static void ClearWalls()
        {
            if (MainForm.listViewWalls.InvokeRequired)
                MainForm.listViewWalls.Invoke(new DlVoid(ClearWalls));
            else
                MainForm.listViewWalls.Items.Clear();
        }

        public static void ClearUserInfo()
        {
            _userTicks = 0;
            if (MainForm.lblClanID.InvokeRequired)
                MainForm.lblClanID.Invoke(new DlVoid(ClearUserInfo));
            else
            {
                MainForm.lblClanID.Text = "?";
                MainForm.lblClanLevel.Text = "?";
                MainForm.lblClanName.Text = "(None)";
                MainForm.lblUserID.Text = "?";
                MainForm.lblUserLevel.Text = "?";
                MainForm.lblUserName.Text = "?";

                // Clear info-plus stuff as well:
                MainForm.lblObjectSpawn.Text = "?";
                MainForm.lblLastActivity.Text = "?";
                MainForm.lblBoxSpawn.Text = "?";
                MainForm.lblBuilders.Text = "?";
                MainForm.lblShieldTime.Text = "?";
                MainForm.lblExp.Text = "?";
                MainForm.lblGemOrder.Text = "?";
                MainForm.lblXCSHL.Text = "null";
            }
        }


        public static string ToRomanNumeral(int number)
        {
            var romanNumerals = new[]
            {
                new[] {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
                new[] {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
                new[] {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
                new[] {"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;

            // starting with the highest place (for 3046, it would be the thousands
            // place, or 3), get the roman numeral representation for that place
            // and add it to the final roman numeral string
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][int.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }

        private enum InfoPlusListViewRenderType
        {
            LootInfo,
            BuilderInfo,
            LaboratoryInfo,
            SpellFactory,
            DarkSpellFactory,
            Barracks
        }

        //private delegate void dlVoid();
        private delegate void DlIntInt(int level, int count);

        private delegate void DlStrIntLong(string name, int level, long id);

        private delegate void DlVoid();
    }
}