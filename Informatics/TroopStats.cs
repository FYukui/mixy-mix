﻿using System.Collections.Generic;
using YukiNoKagami.ServerData;

namespace YukiNoKagami.Informatics
{
    /// <summary>
    ///     Represents a certain troop's stats at a certain, provided level.
    /// </summary>
    internal class TroopStatEntry
    {
        public int CharImageIndex;
        public int Cost;
        public int DPS;
        public int HP;
        public int LabRequiredLevel;
        public int Level;
    }

    /// <summary>
    ///     Contains an accessible set of levels (<see cref="LevelData" />) for the given building,
    ///     with each containing critical stats for that level.
    /// </summary>
    internal class TroopEntry
    {
        // Not yet used for all: AttackSpeed, Range, MovementSpeed
        public float AttackSpeed;
        public ClashTypes.Resource CostUnit;
        public int HousingSpace;
        public ClashTypes.Troop ID;
        public int MovementSpeed;
        public string Name;
        public float Range;
        public int TrainingTime;

        public TroopEntry(string name, ClashTypes.Troop id, int levelCount, int housingSpace,
            int trainingTime, ClashTypes.Resource costUnit)
        {
            LevelData = new TroopStatEntry[levelCount];
            CostUnit = costUnit;
            Name = name;
            ID = id;
            HousingSpace = housingSpace;
            TrainingTime = trainingTime;
        }

        public int MaxLevel => LevelData.Length;
        public TroopStatEntry[] LevelData { get; }

        public void AddLevel(int level, int dps, int hp, int cost, int labLevel, int charImageIndex)
        {
            TroopStatEntry statEntry = new TroopStatEntry
            {
                Level = level,
                DPS = dps,
                HP = hp,
                Cost = cost,
                LabRequiredLevel = labLevel,
                CharImageIndex = charImageIndex
            };

            LevelData[level] = statEntry;
        }
    }

    /// <summary>
    ///     Provides convenient lookup and definition support for Clash troop types.
    /// </summary>
    internal static class TroopStats
    {
        public static Dictionary<ClashTypes.Troop, TroopEntry> DefinedTroops;

        static TroopStats()
        {
            DefinedTroops = new Dictionary<ClashTypes.Troop, TroopEntry>();
        }

        public static TroopEntry CreateTroop(string troopName, ClashTypes.Troop id, int levelCount, int housingSpace,
            int trainingTime,
            ClashTypes.Resource costUnit)
        {
            TroopEntry troop = new TroopEntry(troopName, id, levelCount, housingSpace, trainingTime, costUnit);
            DefinedTroops.Add(id, troop);
            return troop;
        }

        public static TroopEntry GetTroop(ClashTypes.Troop id)
        {
            TroopEntry outEntry = null;
            DefinedTroops.TryGetValue(id, out outEntry);
            return outEntry;
        }
    }
}