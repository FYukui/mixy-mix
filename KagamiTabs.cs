﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace YukiNoKagami
{
    [Obsolete("Deprecated in favor of KagamiButton. Use that instead.\n\nConstruction yields exception.")]
    public partial class KagamiTabs : UserControl
    {
        private readonly List<KTab> _tabs;

        public KagamiTabs()
        {
            _tabs = new List<KTab>();
            InitializeComponent();
        }

        public void AddTab(string name)
        {
            KTab nTab = new KTab(name, 0, Font);
            _tabs.Add(nTab);
        }

        private void KagamiTabs_Paint(object sender, PaintEventArgs e)
        {
            int drawX = 0;
            foreach (KTab nTab in _tabs)
            {
                nTab.Draw(e.Graphics, drawX, Height);
                drawX += nTab.TabWidth;
            }
        }

        private void KagamiTabs_Load(object sender, EventArgs e)
        {
            // Nothing to do here.
        }

        protected class KTab
        {
            private const int KTabPadding = 10;
            private readonly SolidBrush _drawBrush;
            private readonly Pen _drawPen;
            private readonly string _text;
            private readonly int _textHeight;

            private readonly int _textWidth;
            private readonly Font _tFont;

            public KTab(string text, int callback, Font fnt)
            {
                _text = text;
                Size sz = TextRenderer.MeasureText(text, fnt);
                _textHeight = sz.Height;
                _textWidth = sz.Width;
                _tFont = fnt;

                _drawBrush = new SolidBrush(Color.FromArgb(80, 80, 80));
                _drawPen = new Pen(_drawBrush);

                throw new InvalidOperationException("Constructed h-deprecated KTab.");
            }


            public int TabWidth => _textWidth + KTabPadding*2;

            public void Draw(Graphics g, int x, int height)
            {
                g.DrawString(_text, _tFont, _drawBrush, x + KTabPadding, (height - _textHeight)/2);
                g.DrawRectangle(_drawPen, x, 0, KTabPadding*2 + _textWidth, height);
            }
        }
    }
}