# README #

### What is this stuff o.o? ###

Why, it's Yuki's ultimate Mixy-mix repo! ... Or honestly, I don't really know >.>

I'm pretty much in the process of building a little collection of some sample code to host here. Some of it will be hosted in a little corner my [main site](http://yuki49.net/rem) as well, while some might be exclusive to here.

Here's what's up right now:

* Yuki no Kagami – Clash of Clans multi-tool written in C#. Fairly messy in some places, considering the rush-job and my distaste for C#.
* UkiRAD excerpts – Small portions of the alpha-stage UkiRAD IDE. Hosted on main site only.

### Contact ###

* Skype ID: fyukui
* Via e-mail: [yuki@yuki49.net](mailto:yuki@yuki49.net)