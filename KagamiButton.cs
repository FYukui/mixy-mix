﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace YukiNoKagami
{
    public sealed partial class KagamiButton : UserControl
    {
        private const int KBtnPadding = 10;

        // Constant Class-Local Colors
        private static readonly Color ClOutlineNormal = Color.FromArgb(148, 148, 148);
        private static readonly Color ClOutlineHover = Color.FromArgb(65, 148, 247);
        private static readonly Color ClOutlinePushed = Color.FromArgb(0, 120, 215);
        private static readonly Color ClOutlineDown = Color.FromArgb(0, 120, 215);
        private static readonly Color ClText = Color.FromArgb(49, 49, 49);
        private readonly Pen _borderPen;

        // Private vars
        private KButtonState _btnState;
        private bool _isHovered;
        private bool _isSticky;
        private string _pText;
        private bool _releaseSticky;
        private Size _textMetrics;

        // Button states
        private enum KButtonState
        {
            Normal,
            Hover,
            Pushed, // The mouse is currently pushing the button down
            Sticky  // Button is literally stuck down. (only available with Sticky property)
        };


        public KagamiButton()
        {
            InitializeComponent();
            DoubleBuffered = true;
            XText = "KButton";
            _borderPen = new Pen(ClOutlineNormal, 2);
        }


        // Properties
        [Description("Text displayed within the KagamiButton."), Category("Appearance")]
        public string XText
        {
            get { return _pText; }
            set
            {
                _pText = value;
                _textMetrics = TextRenderer.MeasureText(_pText, Font);
                int w = _textMetrics.Width + KBtnPadding*2;
                if (Width < w)
                    Width = w;
                Refresh();
            }
        }

        [Description("Determines whether the button will retain state after a single push."), Category("Appearance")]
        public bool Sticky
        {
            get { return _isSticky; }
            set
            {
                if (_isSticky == value)
                    return;
                if (_isSticky && _btnState == KButtonState.Sticky)
                {
                    _btnState = !_isHovered ? KButtonState.Normal : KButtonState.Hover;
                    Refresh();
                }
                _isSticky = value;
            }
        }

        public bool IsDown
        {
            get { return _btnState == KButtonState.Sticky; }
            set
            {
                if (value)
                    _btnState = KButtonState.Sticky;
                else
                {
                    _btnState = _isHovered ? KButtonState.Hover : KButtonState.Normal;
                }
                Refresh();
            }
        }

        private void KagamiButton_Paint(object sender, PaintEventArgs e)
        {
            Color outlineColor = ClOutlineNormal;
            switch (_btnState)
            {
                /*case KButtonState.Normal:
                    outlineColor = CL_OUTLINE_NORMAL;
                    break;*/
                case KButtonState.Hover:
                    outlineColor = ClOutlineHover;
                    break;
                case KButtonState.Pushed:
                    outlineColor = ClOutlinePushed;
                    break;
                case KButtonState.Sticky:
                    outlineColor = ClOutlineDown;
                    break;
            }

            _borderPen.Color = outlineColor;
            e.Graphics.DrawString(_pText, Font, new SolidBrush(ClText), (Width - _textMetrics.Width)/2,
                (Height - _textMetrics.Height)/2);
            e.Graphics.DrawRectangle(_borderPen, 1, 1, Width - 2, Height - 2);
        }

        private void KagamiButton_MouseEnter(object sender, EventArgs e)
        {
            if (_btnState != KButtonState.Sticky)
                _btnState = KButtonState.Hover;
            _isHovered = true;
            Refresh();
        }

        private void KagamiButton_MouseLeave(object sender, EventArgs e)
        {
            if (_btnState != KButtonState.Sticky)
                _btnState = KButtonState.Normal;
            _isHovered = false;
            Refresh();
        }

        private void KagamiButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (_btnState == KButtonState.Sticky)
                _releaseSticky = true;
            _btnState = KButtonState.Pushed;
            Refresh();
        }

        private void KagamiButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (_isSticky)
            {
                if (_btnState == KButtonState.Sticky || _releaseSticky)
                {
                    _btnState = KButtonState.Hover;
                    _releaseSticky = false;
                }
                else
                    _btnState = KButtonState.Sticky;
            }
            else
                _btnState = KButtonState.Hover;
            Refresh();
        }
    }
}