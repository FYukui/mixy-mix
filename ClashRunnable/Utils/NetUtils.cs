﻿using System.Net;
using System.Net.Sockets;
using YukiNoKagami.Logging;

namespace YukiNoKagami.ClashRunnable.Utils
{
    internal static class NetUtils
    {
        // Pack this away since I hate seeing it shoved in the middle of code. Now for everything else... >.>"
        /// <summary>
        ///     Connects to the specified [host] on the given [port], using the TCP protocol.
        /// </summary>
        public static Socket ConnectFromHostName(string host, int port)
        {
            LoggingProvider.EventLog.LogBasicEvent($"Looking up host '{host}' ...");
            IPHostEntry ipHostInfo = Dns.Resolve(host);

            LoggingProvider.EventLog.LogBasicEvent("Resolved address successfully: " + ipHostInfo.AddressList[0]);
            IPEndPoint remoteEp = new IPEndPoint(ipHostInfo.AddressList[0], port);

            Socket nSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            nSocket.Connect(remoteEp);
            return nSocket;
        }
    }
}