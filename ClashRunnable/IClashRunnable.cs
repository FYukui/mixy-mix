﻿

namespace YukiNoKagami.ClashRunnable
{
    internal interface IClashRunnable
    {
        /// <summary>
        ///     Indicates whether this runnable instance has spawned a thread which is actively processing events.
        /// </summary>
        bool IsRunning { get; }

        /// <summary>
        ///     Signals the underlying object to spawn a thread responsible for handling its related functionality.
        /// </summary>
        void Activate();

        /// <summary>
        ///     Signals for the underlying object to stop processing, kill the thread, and save any necessary data.
        ///     The active thread will wait for this entire process to complete.
        /// </summary>
        void Kill();
    }
}