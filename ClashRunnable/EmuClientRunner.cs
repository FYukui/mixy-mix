﻿using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using YukiNoKagami.Ciphers;
using YukiNoKagami.ClashRunnable.Utils;
using YukiNoKagami.Emulation;
using YukiNoKagami.Logging;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.ClashRunnable
{
    internal class EmuClientRunner : IClashRunnable
    {
        private const int ServerPort = 9339;
        private const int KeepAliveInterval = 5000; // Send a keep-alive packet every 5000ms / 5 sec.
        private Thread _runThread;

        private Socket _serverSocket;

        public bool IsRunning => _runThread.IsAlive;

        public void Activate()
        {
            _runThread = new Thread(Run);
            _runThread.IsBackground = true;
            _runThread.Start();
        }

        public void Kill()
        {
            _runThread.Abort();
            while (_runThread.IsAlive)
            {
                Thread.Sleep(1);
            }
            if ((_serverSocket != null) && _serverSocket.IsBound)
                _serverSocket.Close();
        }

        private void Run()
        {
            _serverSocket = null;

            MessageReader msgReaderServer = new MessageReader(null); // Again, null-out socket since the reader does nothing but receive.

            ClashCipher clientCipher = new ClashCipher();
            ClashCipher serverCipher = new ClashCipher();
            msgReaderServer.SetCipherMethod(serverCipher);

            EmuClientFilter emuFilter = new EmuClientFilter();
            emuFilter.SetServerSipherReference(serverCipher);
            emuFilter.SetCipher(clientCipher);
            msgReaderServer.SetMessageFilter(emuFilter);

            // +
            // + Grab Clash server IP from hostname and attempt to connect to the server.
            _serverSocket = NetUtils.ConnectFromHostName("game.clashofclans.com", ServerPort); // This should be customizable
            emuFilter.SetServerSocket(_serverSocket);

            // +
            // + Send Login packet recorded from a previous EmuServer session. This /absolutely requires/ that the file exists.
            // # TODO: This hurts. Please... Clean this hackery (and the below while you're at it!)
            MessageWriter pkWriter = new MessageWriter(0);
            pkWriter.LoadFromFile("Purity/packet_record/client/sequence_login/Login");
            byte[] byMsg = pkWriter.FinalizeMessage();

            int clientSeed = (byMsg[304] << 24) | (byMsg[305] << 16) | (byMsg[306] << 8) | (byMsg[307]);
            emuFilter.SetClientSeed(clientSeed);

            Message oMsg = emuFilter.CreateCipheredClientMessage(byMsg);
            _serverSocket.Send(oMsg.GetData());
            LoggingProvider.PacketLog.LogMessage(oMsg);


            // + 
            // + Start keep-alive timer (lol) and generate a keepAliveMessage
            Stopwatch timer = Stopwatch.StartNew();
            MessageWriter keepAliveWriter = new MessageWriter((int) MessageInfo.MessageType.KeepAlive);
            Message msgKeepAlive = new Message(keepAliveWriter.FinalizeMessage());

            bool shouldSendKeepAlivePackets = false; // Prevent sending KeepAlives too early.

            while (true)
            {
                if (_serverSocket.Available > 0)
                {
                    msgReaderServer.StreamMessage(_serverSocket);
                    if (emuFilter.JustHandledEncryption)
                    {
                        emuFilter.JustHandledEncryption = false;
                        shouldSendKeepAlivePackets = true;  // After this point, we should start sending KeepAlive packets every 5000ms
                                                            // Doing so prior to this can cause a fuss, as the server expects the client
                                                            // to wait until encryption processing has finished.
                        timer.Restart();
                    }
                }

                if (timer.ElapsedMilliseconds > KeepAliveInterval)
                {
                    if (shouldSendKeepAlivePackets)
                    {
                        _serverSocket.Send(msgKeepAlive.GetData());
                        LoggingProvider.PacketLog.LogMessage(msgKeepAlive);
                        timer.Restart();
                    }
                }

                Thread.Sleep(3);
            }
        }


    }
}