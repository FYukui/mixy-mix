﻿using System.Net.Sockets;
using System.Threading;
using YukiNoKagami.Ciphers;
using YukiNoKagami.ClashRunnable.Utils;
using YukiNoKagami.Listeners;
using YukiNoKagami.Logging;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.ClashRunnable
{
    internal class ProxyRunner : IClashRunnable
    {
        private const int ServerPort = 9339;
        private Socket _mirrorSocket;
        private Socket _playerSocket;

        private Thread _runThread;
        private TcpListener _server;
        private volatile int _shouldStop;

        public bool IsRunning => _runThread.IsAlive;

        public void Activate()
        {
            _runThread = new Thread(Run) {IsBackground = true};
            _runThread.Start();
        }

        public void Kill()
        {
            _runThread.Abort();
            while (_runThread.IsAlive)
            {
                Thread.Sleep(1);
            }
            if ((_playerSocket != null) && _playerSocket.IsBound)
                _playerSocket.Close();
            if ((_mirrorSocket != null) && _mirrorSocket.IsBound)
                _mirrorSocket.Close();
            _server.Stop();
        }

        private void RegisterServerFilters(MessageFilter filter)
        {
            new MVillageListener().RegisterMessages(filter);
            new MProxyRecord().RegisterMessages(filter);
        }

        private void RegisterClientFilters(MessageFilter filter)
        {
            // Oops O.O ... >.>
        }

        private void Run()
        {
            _server = new TcpListener(ServerPort);
            _playerSocket = null;
            _mirrorSocket = null;

            MessageReader mReaderPlayer = new MessageReader(null); // Create with null-out sockets. Set these later !!!
            MessageReader mReaderMirror = new MessageReader(null);
            MessageFilter filter = new MessageFilter();
            ClashCipher clientCipher = new ClashCipher();
            ClashCipher serverCipher = new ClashCipher();
            MKeyListener keyListener = new MKeyListener(clientCipher, serverCipher);

            filter.AddFilter(MessageInfo.MessageType.Login, keyListener);
            filter.AddFilter(MessageInfo.MessageType.Encryption, keyListener);

            RegisterServerFilters(filter);
            RegisterClientFilters(filter);

            mReaderMirror.SetMessageFilter(filter);
            mReaderPlayer.SetMessageFilter(filter);

            mReaderMirror.SetCipherMethod(clientCipher);
            mReaderPlayer.SetCipherMethod(serverCipher);

            _server.Start();

            while (true)
            {
                if (_server.Pending()) // Connection attempt !
                {
                    LoggingProvider.EventLog.LogBasicEvent("Client requesting server connection to 127.0.0.1:" + ServerPort + " !");
                    _playerSocket = _server.AcceptSocket();
                    LoggingProvider.EventLog.LogBasicEvent("Accepted new client: " + _playerSocket.RemoteEndPoint);

                    LoggingProvider.EventLog.LogBasicEvent("~");
                    LoggingProvider.EventLog.LogBasicEvent("Establishing proxy...");

                    _mirrorSocket = NetUtils.ConnectFromHostName("game.clashofclans.com", ServerPort);
                    mReaderPlayer.SetOutSocket(_mirrorSocket); // Set messageReader out-sockets
                    mReaderMirror.SetOutSocket(_playerSocket);
                }

                if (_playerSocket?.Available > 0)
                {
                    //LoggingProvider.EventLog.LogBasicEvent("player Available = " + playerSocket.Available);
                    mReaderPlayer.StreamMessage(_playerSocket);
                }
                if (_mirrorSocket?.Available > 0)
                {
                    //LoggingProvider.EventLog.LogBasicEvent("mirror Available = " + mirrorSocket.Available);
                    mReaderMirror.StreamMessage(_mirrorSocket);
                }

                Thread.Sleep(3); // (Is this provably different than using 16? I know Windoze is finicky with clock ticks sometimes >.>)
            }
        }

        public void Pause()
        {
        }

        public void Resume()
        {
        }
    }
}