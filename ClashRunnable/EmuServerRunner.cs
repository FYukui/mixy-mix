﻿using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using YukiNoKagami.Ciphers;
using YukiNoKagami.Emulation;
using YukiNoKagami.Logging;
using YukiNoKagami.MessageUtils.MessageHandling;

namespace YukiNoKagami.ClashRunnable
{
    internal class EmuServerRunner : IClashRunnable
    {
        private const int ServerPort = 9339;
        private const int KeepAliveInterval = 5000; // Send a keep-alive packet every 5000ms / 5 sec.

        private Socket _playerSocket;
        private Thread _runThread;
        private TcpListener _server;

        public bool IsRunning => _runThread.IsAlive;

        public void Activate()
        {
            _runThread = new Thread(Run) {IsBackground = true};
            _runThread.Start();
        }

        public void Kill()
        {
            _runThread.Abort();
            while (_runThread.IsAlive)
            {
                Thread.Sleep(1);
            }
            if ((_playerSocket != null) && _playerSocket.IsBound)
                _playerSocket.Close();
            _server.Stop();
        }

        private void Run()
        {
            _server = new TcpListener(ServerPort);
            _playerSocket = null;


            MessageReader msgReaderPlayer = new MessageReader(null);    // Create with null-out socket. It needs not be set, as there is no
                                                                        // redirection or proxying, simply pure receiving.

            ClashCipher clientCipher = new ClashCipher(); // Used to decrypt received client packets
            ClashCipher serverCipher = new ClashCipher(); // Encrypts server messages before sending them
            msgReaderPlayer.SetCipherMethod(clientCipher);

            EmuServerFilter emuFilter = new EmuServerFilter();
            msgReaderPlayer.SetMessageFilter(emuFilter);
            emuFilter.SetCipher(serverCipher);
            emuFilter.SetClientCipherReference(clientCipher);

            _server.Start();

            Stopwatch timer = null;
            MessageWriter keepAliveWriter = new MessageWriter((int) MessageInfo.MessageType.ServerKeepAlive);
            Message msgKeepAlive = new Message(keepAliveWriter.FinalizeMessage()); // TODO: Fix message sending model, srsly (T_T).


            LoggingProvider.EventLog.LogBasicEvent("Started server on port " + ServerPort);
            while (true)
            {
                if (_server.Pending()) // Connection attempt !
                {
                    LoggingProvider.EventLog.LogBasicEvent("Client requesting server connection to 127.0.0.1:" + ServerPort + " !");

                    _playerSocket = _server.AcceptSocket();
                    LoggingProvider.EventLog.LogBasicEvent("Accepted new client: " + _playerSocket.RemoteEndPoint);
                    LoggingProvider.EventLog.LogBasicEvent("~");
                    emuFilter.SetClientSocket(_playerSocket);

                    timer = Stopwatch.StartNew();
                }

                if (_playerSocket != null)
                {
                    if (_playerSocket.Available > 0)
                    {
                        //evtLog.LogBasicEvent("player Available = "+playerSocket.Available);
                        msgReaderPlayer.StreamMessage(_playerSocket);
                    }

                    // TODO: Refrain from stopwatch usage, implement TickBasedQueue.
                    if (timer.ElapsedMilliseconds > KeepAliveInterval)
                    {
                        _playerSocket.Send(msgKeepAlive.GetData());
                        LoggingProvider.PacketLog.LogMessage(msgKeepAlive);
                        timer.Restart();
                    }
                }

                Thread.Sleep(3);
            }
        }

    }
}