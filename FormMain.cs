﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using YukiNoKagami.ClashRunnable;
using YukiNoKagami.Informatics;
using YukiNoKagami.Logging;
using YukiNoKagami.MessageUtils.LFormat;
using YukiNoKagami.MessageUtils.MessageHandling;
using YukiNoKagami.ServerData;
using Message = YukiNoKagami.MessageUtils.MessageHandling.Message;

//using YukiNoKagami.MessageUtils.MessageHandling.Message;

namespace YukiNoKagami
{
    public partial class FormMain : Form
    {
        private readonly List<KagamiButton> _btnTabs;

        private readonly Color _clStatusOffline = Color.FromArgb(192, 0, 0);
        private readonly Color _clStatusOnline = Color.FromArgb(0, 128, 0);
        private readonly Color _clStatusStarting = Color.FromArgb(255, 210, 0);

        private readonly EventLogger _evtLog;

        private readonly FormPacketDetails _packetDetailsForm;
        private readonly PacketLogger _pktLog;
        private IClashRunnable _clashInstance;

        public FormMain()
        {
            InitializeComponent();
            _btnTabs = new List<KagamiButton> {kbServerStats, kbVillageStats, kbPacketLog, kbEventLog};

            kbEventLog.Tag = plEventLog;
            kbPacketLog.Tag = plPacketLog;
            kbVillageStats.Tag = plVillageStats;
            kbServerStats.IsDown = true;


            _evtLog = new EventLogger(eventTree, lblLastEvent);
            _pktLog = new PacketLogger(packetListView);


            LoggingProvider.EventLog = _evtLog;
            LoggingProvider.PacketLog = _pktLog;

            _packetDetailsForm = new FormPacketDetails();

            listViewWalls.Columns[3].Width = -2;
            VillageStats.MainForm = this;
            VillageStats.ClearAll();

            BuildingStatLoader.Load();
            TroopStatLoader.Load();
        }

        private void ClearBtnTabState(KagamiButton self)
        {
            foreach (KagamiButton nBtn in _btnTabs)
                if (nBtn.IsDown && nBtn != self)
                {
                    nBtn.IsDown = false;
                    if (nBtn.Tag != null)
                        ((Control) nBtn.Tag).Visible = false;
                }
        }

        private void btnStartServer_Click(object sender, EventArgs e)
        {
            if (btnStartServer.Text.Equals("Start"))
            {
                statusIndicator.BackColor = _clStatusStarting;
                lblServerStatus.Text = "Server Status: Starting server...";

                // Shameless button-text-based switch xD
                switch (serverModeSwitch.XText)
                {
                    case "Proxy":
                        _clashInstance = new ProxyRunner();
                        break;
                    case "Emulate":
                        _clashInstance = new EmuServerRunner();
                        break;
                    case "ClientX":
                        _clashInstance = new EmuClientRunner();
                        break;
                }
                _clashInstance.Activate();
                if (_clashInstance is ProxyRunner)
                    updateTimer.Stop();

                statusIndicator.BackColor = _clStatusOnline;
                lblServerStatus.Text = "Server Status: Online";
                btnStartServer.Text = "Stop";
            }
            else
            {
                statusIndicator.BackColor = _clStatusStarting;
                lblServerStatus.Text = "Server Status: Stopping server...";
                btnStartServer.Text = "Start";

                if (_clashInstance is ProxyRunner)
                    updateTimer.Stop();
                _clashInstance.Kill();

                statusIndicator.BackColor = _clStatusOffline;
                lblServerStatus.Text = "Server Status: Offline";
            }
        }

        private void serverModeSwitch_MouseUp(object sender, MouseEventArgs e)
        {
            // We can't switch the server mode if we've got an instance actively running; suggest the user cancel
            // the current one.
            if ((_clashInstance != null) && (_clashInstance.IsRunning))
            {
                MessageBox.Show(this, "The instance mode cannot be changed while it is running!\n\n" +
                                      "Please stop the server and try again.", "Unable to Switch Instance Mode:",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            switch (serverModeSwitch.XText)
            {
                case "ClientX":
                    serverModeSwitch.Text = "Proxy";
                    break;
                case "Proxy":
                    serverModeSwitch.Text = "Emulate";
                    break;
                case "Emulate":
                    serverModeSwitch.Text = "ClientX";
                    break;
            }

        }

        private void FormMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                if ((_clashInstance != null) && _clashInstance.IsRunning)
                {
                    if (MessageBox.Show("The server is currently running, are you sure you want to close?",
                        "Confirmation:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        Close();
                }
        }

        private void kagamiBtnTab_MouseUp(object sender, MouseEventArgs e)
        {
            ClearBtnTabState((KagamiButton) sender);
            ((KagamiButton) sender).IsDown = true;

            if (((KagamiButton) sender).Tag != null)
            {
                ((Control) ((KagamiButton) sender).Tag).Visible = true;
            }
        }

        private void eventLog_Clear_Click(object sender, EventArgs e)
        {
            _evtLog.Clear();
            //eventTree.Nodes.Add("xxxx").ForeColor = Color.FromArgb(255, 0, 0);
            //eventTree.Nodes.Add("yyyy").ForeColor = Color.FromArgb(0, 0, 0);
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Hide();
            _clashInstance?.Kill();
            Application.Exit();
        }

        private void packetListView_SizeChanged(object sender, EventArgs e)
        {
            int w = packetListView.ClientSize.Width;
            for (int x = 0; x < 5; x++)
            {
                w -= packetListView.Columns[x].Width;
            }
            packetListView.Columns[5].Width = w - 1;
        }

        private void packetListView_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            if (e.ColumnIndex == 5)
                return;
            int w = packetListView.ClientSize.Width - 1;
            for (int x = 0; x < 5; x++)
            {
                if (x == e.ColumnIndex)
                    w -= e.NewWidth;
                else
                    w -= packetListView.Columns[x].Width;
            }
            packetListView.Columns[5].Width = w;
        }

        private void btnClearPacketLog_Click(object sender, EventArgs e)
        {
            _pktLog.ClearLog();
        }

        private void btnSavePacketLog_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog
            {
                Filter = "Log Files (.log)|*.log",
                Title = "Save Packet Log:"
            };
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                _pktLog.SaveLog(saveDialog.FileName);
            }
        }

        private void btnLoadDefinitions_Click(object sender, EventArgs e)
        {
            //FkJsonNode json = new FkJsonNode();


            FormalJsonDefinitionHost.ReloadAll("MessageDefinitions");
            FormalJsonDefinitionHost.InsertIntoKagamiDataHost();

            //json.loadString(Clipboard.GetText());
            //Clipboard.SetText(json.xTrace());

            /*json.rootNode();
            json.clearLevel();

            json = json.createObject(null);
            json.createString("objProperty#1", "some value or whatever :3");
            json.createString("property2", "another string :x");
            json.createFloat("property3", 3);

            json = json.up().createObject(null);
            json.createString("property", "blah blah blah ");
            json.queryMember("property").setInteger(333);

            json.createArray("buildings").getArray().fromInteger(1, 10);

            json = json.rootNode();


            json.fkTrace();

            Clipboard.SetText(json.getJsonString());*/

            //MessageBox.Show("Executed getJsonString in ~" + ((double)jsonTime /1000.0).ToString() + " ms :O");

            //Clipboard.SetText(json.getJsonString());
        }

        private void DisplayPacketDetails()
        {
            if (packetListView.SelectedItems.Count < 1)
                return;
            Message m = _pktLog.GetIndexedMessage(packetListView.Items.Count - packetListView.SelectedItems[0].Index - 1);


            _packetDetailsForm.SetMessage(m);
            _packetDetailsForm.ShowDialog(this);
        }

        private void packetListView_DoubleClick(object sender, EventArgs e)
        {
            DisplayPacketDetails();
        }

        private void btnPacketDetails_Click(object sender, EventArgs e)
        {
            DisplayPacketDetails();
        }

        private void listViewWalls_SizeChanged(object sender, EventArgs e)
        {
            int w = listViewWalls.ClientSize.Width;
            for (int x = 0; x < 3; x++)
            {
                w -= listViewWalls.Columns[x].Width;
            }
            listViewWalls.Columns[3].Width = w - 1;
        }

        private void listViewWalls_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            if (e.ColumnIndex == 3)
                return;
            int w = listViewWalls.ClientSize.Width - 1;
            for (int x = 0; x < 3; x++)
            {
                if (x == e.ColumnIndex)
                    w -= e.NewWidth;
                else
                    w -= listViewWalls.Columns[x].Width;
            }
            listViewWalls.Columns[3].Width = w;
        }

        private void listViewTroops_SizeChanged(object sender, EventArgs e)
        {
            int w = listViewTroops.ClientSize.Width;
            for (int x = 0; x < 9; x++)
            {
                w -= listViewTroops.Columns[x].Width;
            }
            listViewTroops.Columns[9].Width = w - 1;
        }

        private void listViewTroops_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            if (e.ColumnIndex == 9)
                return;
            int w = listViewTroops.ClientSize.Width - 1;
            for (int x = 0; x < 9; x++)
            {
                if (x == e.ColumnIndex)
                    w -= e.NewWidth;
                else
                    w -= listViewTroops.Columns[x].Width;
            }
            listViewTroops.Columns[9].Width = w;
        }

        private void lblUserName_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Clipboard.SetText(lblUserName.Text);
                MessageBox.Show("User Name copied to clipboard!");
            }
        }

        private void lblClanName_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Clipboard.SetText(lblClanName.Text);
                MessageBox.Show("Clan Name copied to clipboard!");
            }
        }

        private void lblUserID_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Clipboard.SetText(lblUserID.Text);
                MessageBox.Show("User ID copied to clipboard!");
            }
        }

        private void lblClanID_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Clipboard.SetText(lblClanID.Text);
                MessageBox.Show("Clan ID copied to clipboard!");
            }
        }

        private void btnToggleCCView_Click(object sender, EventArgs e)
        {
            VillageStats.IsShowingClanTroops = !VillageStats.IsShowingClanTroops;
            btnToggleCCView.Text = VillageStats.IsShowingClanTroops ? "Hide CC" : "Show CC";
            VillageStats.RenderSelectedTroops();
        }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            VillageStats.TimerTick();
        }

        private void btnLootInfo_Click(object sender, EventArgs e)
        {
            if (plLootInfoPlus.Visible)
            {
                lblWallsCaption.Visible = true;
                listViewWalls.Visible = true;
                lblTroopsCaption.Visible = true;
                listViewTroops.Visible = true;
                btnToggleCCView.Visible = true;
                plLootInfoPlus.Visible = false;
                btnLootInfo.Text = "Loot Info";
                VillageStats.IsShowingInfoPlus = false;
            }
            else
            {
                lblWallsCaption.Visible = false;
                listViewWalls.Visible = false;
                lblTroopsCaption.Visible = false;
                listViewTroops.Visible = false;
                btnToggleCCView.Visible = false;

                selDisplayType.SelectedIndex = 0;
                VillageStats.IsShowingInfoPlus = true;
                VillageStats.RenderInfoPlusStatic();
                plLootInfoPlus.Visible = true;
                btnLootInfo.Text = "Troop Info";
            }
        }

        private void selDisplayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            VillageStats.InfoPlusListViewConfigureType();
        }

        private void btnExperimental_Click(object sender, EventArgs e)
        {
            MessageWriter msgWrite = new MessageWriter(10101);

            msgWrite.SetUnknown(7);
            msgWrite.FinalizeMessage();
            msgWrite.SaveToFile("c:/users/fyuku/desktop/login.cmsg");
        }
    }
}