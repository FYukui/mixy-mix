﻿namespace YukiNoKagami
{
    sealed partial class KagamiButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // KagamiButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "KagamiButton";
            this.Size = new System.Drawing.Size(64, 30);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.KagamiButton_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KagamiButton_MouseDown);
            this.MouseEnter += new System.EventHandler(this.KagamiButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.KagamiButton_MouseLeave);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KagamiButton_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
