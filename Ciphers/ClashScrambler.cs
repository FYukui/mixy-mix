﻿using System;

namespace YukiNoKagami.Ciphers
{
    public class ClashScrambler
    {
        private const int MatrixA = unchecked((int) 0x9908B0df);
        private const int UpperMask = unchecked((int) 0x80000000);
        private const int LowerMask = 0x7fffffff;
        private const int TemperingMaskB = unchecked((int) 0x9d2c5680);
        private const int TemperingMaskC = unchecked((int) 0xefc60000);

        private readonly int[] _s = new int[624];
        private int _ix;

        /// <summary>
        ///     Seeds the random number generator.
        /// </summary>
        /// <param name="seed">The client seed, as sent to the server in the Login message</param>
        public ClashScrambler(int seed)
        {
            for (int i = 0; i < _s.Length; i++)
            {
                _s[i] = seed;
                seed = 1812433253*((seed ^ (seed >> 30)) + 1);
            }
        }

        /// <summary>
        ///     The Clash v7 key generator. Generates the nonce used to form the RC4 key.
        /// </summary>
        /// <param name="serverRandom">The random seed returned from the server.</param>
        public byte[] Scramble(byte[] serverRandom)
        {
            byte[] result = new byte[serverRandom.Length];
            // Mask is the 100th byte from the stream.
            byte mask = NextByte(100);
            // Xor the value provided by the server with prng stream and mask it with the above value
            // The mask is no doubt intended as an obfuscation step. Using & instead of ^ is an odd choice.
            for (int i = 0; i < serverRandom.Length; i++)
            {
                result[i] = (byte) (serverRandom[i] ^ (NextByte() & mask));
            }
            return result;
        }


        /// <summary>
        ///     Get the low byte from the next int in the stream.
        /// </summary>
        public byte NextByte()
        {
            return (byte) NextInt();
        }

        /// <summary>
        ///     Get the low order byte from the nth int in the stream.
        /// </summary>
        /// <param name="offset">The distance from the byte to fetch, relative to current pos.</param>
        public byte NextByte(int offset)
        {
            Skip(offset - 1);
            return NextByte();
        }

        /// <summary>
        ///     Skip forward <paramref name="n" /> iterations.
        /// </summary>
        public void Skip(int n)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            for (int i = 0; i < n; i++)
            {
                NextInt();
            }
        }

        public int NextInt()
        {
            if (_ix == 0)
            {
                for (int i = 1, j = 0; i <= _s.Length; i++, j++)
                {
                    int v4 = (_s[i%_s.Length] & LowerMask) + (_s[j] & UpperMask);
                    int v6 = (v4 >> 1) ^ _s[(i + 396)%_s.Length];
                    if ((v4 & 1) == 1)
                    {
                        v6 ^= MatrixA;
                    }
                    _s[j] = v6;
                }
            }
            int val = _s[_ix];
            _ix = (_ix + 1)%_s.Length;
            val ^= (val >> 11) ^ ((val ^ (val >> 11)) << 7) & TemperingMaskB;
            val = (((val ^ (val << 15) & TemperingMaskC) >> 18) ^ val ^ (val << 15) & TemperingMaskC);
            return val;
        }
    }
}