﻿using System.Collections.Generic;

namespace YukiNoKagami.Ciphers
{
    public class RC4
    {
        private readonly byte[] _state = new byte[256];
        private readonly Stack<RC4State> _storedStates;
        private int _x;
        private int _y;


        public RC4(byte[] key)
        {
            SetKey(key);
            _storedStates = new Stack<RC4State>();
        }

        public void Push()
        {
            _storedStates.Push(new RC4State(_state, _x, _y));
        }

        public void Pop()
        {
            RC4State lastState = _storedStates.Pop();
            lastState.State.CopyTo(_state, 0);
            _x = lastState.X;
            _y = lastState.Y;
        }

        public void SetKey(byte[] key)
        {
            _x = _y = 0;

            for (int i = 0; i < 256; ++i)
            {
                _state[i] = (byte) i;
            }

            for (int i = 0, j = 0, k = 0; i < 256; ++i)
            {
                k = (key[j] + _state[i] + k) & 0xff;
                byte temp = _state[i];
                _state[i] = _state[k];
                _state[k] = temp;
                j = (j + 1)%key.Length;
            }
        }

        public byte[] Encrypt(byte[] clearText)
        {
            byte[] cipherText = new byte[clearText.Length];
            for (int i = 0; i < clearText.Length; ++i)
            {
                cipherText[i] = (byte) (clearText[i] ^ _state[Next()]);
            }
            return cipherText;
        }

        private int Next()
        {
            _x = (_x + 1) & 0xff;
            _y = (_y + _state[_x]) & 0xff;
            var temp = _state[_x];
            _state[_x] = _state[_y];
            _state[_y] = temp;
            return (_state[_x] + _state[_y]) & 0xff;
        }

        public byte[] Generate(int length)
        {
            byte[] key = new byte[length];
            for (int i = 0; i < key.Length; i++)
            {
                key[i] = (byte) Next();
            }
            return key;
        }

        public void Skip(int length)
        {
            for (int i = 0; i < length; i++)
            {
                Next();
            }
        }

        protected internal class RC4State
        {
            public readonly byte[] State;
            public readonly int X;
            public readonly int Y;

            public RC4State(byte[] stateBytes, int stateX, int stateY)
            {
                State = new byte[stateBytes.Length];
                stateBytes.CopyTo(State, 0);
                X = stateX;
                Y = stateY;
            }
        }
    }
}