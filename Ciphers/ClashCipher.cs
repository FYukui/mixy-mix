﻿using System.Text;

namespace YukiNoKagami.Ciphers
{
    internal interface ICipher
    {
        byte[] Encrypt(byte[] b);
        void SetKey(byte[] nonce);
    }

    public class ClashCipher : ICipher
    {
        // RC4 key prefix. Clearly a keyboard mash.
        private static readonly byte[] BaseKey = Encoding.UTF8.GetBytes("fhsd6f86f67rt8fw78fw789we78r9789wer6re");
        // The initial nonce is literally "nonce"
        private static readonly byte[] InitialNonce = Encoding.UTF8.GetBytes("nonce");

        private RC4 _rc4;

        public ClashCipher()
        {
            SetKey(InitialNonce);
        }

        public byte[] Encrypt(byte[] b)
        {
            return _rc4.Encrypt(b);
        }

        public void SetKey(byte[] nonce)
        {
            byte[] key = Concat(BaseKey, nonce);
            _rc4 = new RC4(key);
            _rc4.Skip(key.Length);
        }

        public void PushState()
        {
            _rc4.Push();
        }

        public void PopState()
        {
            _rc4.Pop();
        }

        private static byte[] Concat(byte[] a, byte[] b)
        {
            byte[] key = new byte[a.Length + b.Length];

            a.CopyTo(key, 0);
            b.CopyTo(key, a.Length);

            return key;
        }
    }
}